<?php
if (strpos($_SERVER['REQUEST_URI'], 'localhost') >= 0 || strpos($_SERVER['REQUEST_URI'], 'homologacao') >= 0) {
//URL de testes
    $pdo = new PDO('mysql:host=dbmy0044.whservidor.com;dbname=olafafa', 'olafafa', 'Brunoa12');
    //token de testes
    define('TOKEN', '4E81907FAF5746FE9AF1994D605FCF65');
} else {
    $pdo = new PDO('mysql:host=dbmy0038.whservidor.com;dbname=olafafa_1', 'olafafa_1', 'Brunoa12');
    define('TOKEN', 'A735233311754A138DB551C679161C29');
}

$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

header('Content-Type: text/html; charset=ISO-8859-1');

class PagSeguroNpi {

    private $timeout = 20; // Timeout em segundos

    public function notificationPost() {
        $postdata = 'Comando=validar&Token=' . TOKEN;
        foreach ($_POST as $key => $value) {
            $valued = $this->clearStr($value);
            $postdata .= "&$key=$valued";
        }
        return $this->verify($postdata);
    }

    private function clearStr($str) {
        if (!get_magic_quotes_gpc()) {
            $str = addslashes($str);
        }
        return $str;
    }

    private function verify($data) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://pagseguro.uol.com.br/pagseguro-ws/checkout/NPI.jhtml");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = trim(curl_exec($curl));
        curl_close($curl);
        return $result;
    }

    public function atualiza_estoque($idcompra) {
        if (strpos($_SERVER['REQUEST_URI'], 'localhost') >= 0 || strpos($_SERVER['REQUEST_URI'], 'homologacao') >= 0) {
            $pdo = new PDO('mysql:host=dbmy0044.whservidor.com;dbname=olafafa', 'olafafa', 'Brunoa12');
        } else {
            $pdo = new PDO('mysql:host=dbmy0038.whservidor.com;dbname=olafafa_1', 'olafafa_1', 'Brunoa12');
        }
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $tr = "select quantidade,compras_tb_id,produtos_tb_id,tamanhos_id from itens where compras_tb_id='{$idcompra}';";
        // if (count($itens) > 0) {
        foreach ($pdo->query($tr) as $item) {

            $prod = $item['produtos_tb_id'];
            $tamanho = $item['tamanhos_id'];
            foreach ($pdo->query("select quantidade,id from estoque_tb where produtos_tb_id='{$prod}' and tamanhos_id='{$tamanho}';") as $dados) {

                $qtd = $dados['quantidade'] - $item['quantidade'];
                //   echo $qtd;
                $update = $pdo->prepare("update estoque_tb set quantidade=':quantidade' where id=':tabela_id';");
                $update->bindParam(':quantidade', $qtd);
                $update->bindParam(':tamanho', $item['id']);
                echo $update->execute();
            }
        }
        //}
    }

}

if (count($_POST) > 0) {
    // POST recebido, indica que é a requisição do NPI.
    $npi = new PagSeguroNpi();
    $result = $npi->notificationPost();
    if (strpos($_SERVER['REQUEST_URI'], 'localhost') >= 0 || strpos($_SERVER['REQUEST_URI'], 'homologacao') >= 0) {
        $pdo = new PDO('mysql:host=dbmy0044.whservidor.com;dbname=olafafa', 'olafafa', 'Brunoa12');
    } else {
        $pdo = new PDO('mysql:host=dbmy0038.whservidor.com;dbname=olafafa_1', 'olafafa_1', 'Brunoa12');
    }
    $stmt = $pdo->prepare("insert pagseguro (dados)values(':dados');");
    $stmt->bindParam(':dados', var_dump($_POST));
    $stmt->execute();
    $transacaoID = isset($_POST['TransacaoID']) ? $_POST['TransacaoID'] : '';

    if ($result == "VERIFICADO") {

        $StatusTransacao = isset($_POST['StatusTransacao']) ? $_POST['StatusTransacao'] : '';

        switch ($StatusTransacao) {
            case 'Completo':
                $status = 'o';
                //  $npi->atualiza_estoque($_POST['Referencia']);
                break;
            case 'Aguardando Pagto':
                $status = 'a';
                break;
            case 'Aprovado':
                $status = 'p';
                //atuallizar estoque
                $npi->atualiza_estoque($_POST['Referencia']);
                break;
            case 'Em Análise':
                $status = 'e';
                break;
            case 'Cancelado':
                $status = 'c';
                break;
        }

        $Referencia = isset($_POST['Referencia']) ? $_POST['Referencia'] : '';
        $stmt = $pdo->prepare("UPDATE compras_tb SET status=:status WHERE id=:idtabela;");
        $stmt->bindParam(':idtabela', $Referencia);
        $stmt->bindParam(':status', $status);
        $stmt->execute();
    } else if ($result == "FALSO") {
        //O post não foi validado pelo PagSeguro.
        echo 'post falso';
    } else {
        //Erro na integração com o PagSeguro.
        echo ($result);
        echo 'erro na integração';
    }
} else {
    // POST não recebido, indica que a requisição é o retorno do Checkout PagSeguro.
    // No término do checkout o usuário é redirecionado para este bloco.
    ?>
    <h3>Obrigado por efetuar a compra.</h3>
    <?php
}
?>