<!DOCTYPE html>

<head>
	<meta charset="UTF-8">
	<title>Termos de uso do olafafá</title>
	<link rel="stylesheet" type="text/css" href="css/bebe.css">
</head>
<body>	

<!--INICIO DO CABEÇALHO-->

	<header id="cabecalho">
		<div class="site">
			<span id="nuvem-esquerda">
				<img src="imagens/nuvem.png" id="esquerda">
			</span>
			<div id="box-login">
					<p>Login</p>
					<input id="login" placeholder=" e-mail">
					<input id="senha" placeholder=" senha">
				<div id="btn-login">
					<img src="imagens/btn-login.png">
				</div> 
			</div>
			<span id="nuvem-direita">
				<img src="imagens/nuvem.png" id="esquerda">
			</span>
		</div>
	</header>

	<div id="faixa-cabecalho">
		<div class="site">
			<!--Links do cabeçalho para animação-->
			<div id="login-cadastro">
				<a href="">Login</a>
				<a href="">Cadastre-se</a>	
			</div>
			<div id="marca">
				<a href="index.html"><img src="imagens/marca.png"></a>
			</div>

			<div id="boas-vindas">
				<p id="ola">olá</p>
				<p>Fabíola! Seja bem vinda.</p>
				<a href="">Sair</a>
			</div>
			<div id="container-sacola">
				<div id="sacola">
					<img src="imagens/sacola.png">
					<hr>
					<p >0 ITENS</p>
					<hr>
					<P>R$ 0,00</P>
				</div>
			</div>	
		</div>		
	</div>

	<div id="box-menu-two">
	<div class="site">
		<nav id="menu-bebe">
			<ul>
				<div id="borda-menu-bebe">
					<a class="menu-efeito" id="bebe" href="bebe.html">Bebê</a>
					<a class="menu-efeito" href="default">Menino</a>
					<a class="menu-efeito" href="default">Menina</a>
					<a class="menu-efeito" href="default">Sapato</a>
					<a class="menu-efeito" href="default">Acessórios</a>
				</div>
			</ul>
		</nav>
		<h1 id="body-nome">Termos e Condições</h1>
	</div>
</div>
		<div class="site termos-page">
			<p>
			<strong>Pagamentos</strong><br>
			A nossa forma de pagamento pode ser por:<br><br><br>
			<strong>-Depósito bancário</strong><br>
			O pagamento poderá ser realizado por meio do Internet banking, nas agências bancárias ou caixas eletrônicos para a conta da Olá fafá!no Banco Itaú, nesse caso entre em contato conosco através do e-mail pagamento@olafafa.com
			Após a compensação e confirmação do depósito, sua encomenda será entregue no endereço do seu cadastro.<br><br>
			<strong>-Pagseguro</strong><br>
			Através do Pagseguro aceitamos todos os cartões de débito e crédito e dividimos em até 3x (parcela mínima R$ 40,00)
			Poder ser também por boleto bancário e nesse caso a data de vencimentoé de 2 (dois) dias úteis após a realização de sua compra, após esta data, o documento perde a validade e a compra será automaticamente cancelada. A confirmação de pagamento é feita pelo Pagseguro em até 3 (três) dias úteis, sendo assim, não é preciso enviar-nos qualquer notificação. Caso necessite de um novo boleto envie um e-mail para pagamento@olafafa.com. <br><br>
			<strong>Entrega</strong><br>
			Os pedidos serão processados em até 2 (dois) dias úteis após a confirmação do pagamento junto as instituições financeiras.<br>
			Os pedidos feitos aos sábados, domingos ou feriados serão despachados no primeiro dia útil subsequente à autorização do pagamento.<br>
			Utilizamos o sistema dos Correios para todas as nossas entregas e o prazo pode variar de acordo com a cidade e o tipo de frete escolhido na hora da compra.<br>
			O cliente pode escolher a forma de entrega no momento de fechamento de sua compra.  As modalidades de entrega que disponibilizamos são PAC e SEDEX.<br>

			As compras são embaladas e enviadas em caixas de papelão, mas você pode optar por embalar para presente qualquer produto do site, basta marcar a opção PRESENTE ao lado de cada produto a ser embalado.<br>

			A entrega podera ser feita no seu endereço ou no endereço do presenteado, mas cada pedido terá um único endereço de entrega.<br>
			Recuse o recebimento caso a embalagem esteja aberta, o produto avariado ou o produto esteja em desacordo com o pedido.<br><br>
			<strong>Trocas de Devoluções</strong><br>
			Nosso compromisso é com a sua satisfação, então se precisar trocar algum produto, por qualquer um dos motivos abaixo,o primeiro passo é entrar em contato conosco informando o motivo da troca através do e-mail trocas@olafafa.comno prazo de até 7 (sete) dias úteis após a data do recebimento do produto.<br><br>
			<strong>Troca de produtos</strong><br>
			Caso você receba o produto e queria trocar o tamanho ou escolher outra mercadoria, você poderá escolher um produto de mesmo valor ou faremos o abatimento proporcional ao valor pago na compra de outra mercadoria.<br><br>

			<strong>Insatisfação, arrependimento ou desistência</strong><br>
			Caso você receba o produto e desista de sua compra, você tem a opção de devolvê-lo e receber a restituição do valor da compra. O código do consumidor assegura o direito de troca do produto no caso de defeito ou desistência desde que ocorra em até 7 (sete) dias úteis após a entrega do mesmo.<br><br>


			<strong>Defeito de fabricação</strong><br>
			Caso você receba um produto que apresente defeito de fabricação, você deverá enviá-lo para análise. Se for constatado o problema, será feita a substituição do produto por outro igual ou você poderá optar pela restituição do valor pago ou pelo abatimento proporcional ao valor na compra de outro produto. O prazo para análise é de 30 dias corridos, mas pode ser definido antes deste período.<br><br>

			<strong>Integridade dos dados</strong><br>
			Os dados armazenados em nossa base de dados não será utilizado para outros fins, além das solicitações feitas pelo sistema.
			O dados são alocados em um banco de dados relacional hospedado na UOL Host em ambiente compartilhado de hospedagem.





			</p>
		</div>
	</div>
</div>

	<footer id="rodape">
		<div id="rodape-topo"></div>
		<div id="rodape-baixo">
			<div class="site">	
				<div class="creditos">
					<ul>
						<h2>Sobre nós</h2>
						<li><a href='<?php echo base_url('quem-somos.php');?>'>Quem Somos</a></li>
						<li><a href="">Política de Privacidade</a></li>
					</ul>
				</div>
				<div class="creditos">
					<ul>
						<h2>Política</h2>
						<li><a href="">Termos e Condições</a></li>
						<li><a href="">Pagamento</a></li>
					</ul>
				</div>
				<div class="creditos">
					<ul>
						<h2>Contato</h2>
						<li><a href="">contato@olafafa.com.br</a></li>
					</ul>
				</div>
			
				<div class="creditos">
					<div id="redes-sociais">
						<a target="_blank" href=" http://www.facebook.com.br"><img src="imagens/btn-face.png"></a>
						<a target="_blank" href="https://twitter.com/" ><img src="imagens/btn-twitter.png"></a>
					</div>				
				</div>
				<div id="cadastro">
					<p>Cadastre-se e receba nossas novidades!</p>
					<input id="email" placeholder=" e-mail">
					<input id="botao" type="button" value="Ok">			
				</div>
				<div class="creditos">
					<img src="imagens/cartoes.png">
				</div>
				<div id="linha"></div>
				<div id="direito-de-copia"><p>Copyright &copy 2013 Olá Fafá. Todos os direitos reservados.</p></div>
				<div id="logo-k">
					<img src="imagens/logo-k.png">
				</div>
			</div>		
		</div>
	</footer>

</body>				