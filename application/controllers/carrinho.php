<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class carrinho extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_site();
    }

    public function index() {
        if (!isset($_SESSION)) {
            session_start();
        }
   //     echo var_dump($_SESSION);
        if (isset($_SESSION['carrinho']) && !is_null($_SESSION['carrinho'])) {
            $dados = array();
            $dados['carrinho'] = $_SESSION['carrinho'];
            $dados['total'] = $_SESSION['total'];
            $dados['itens'] = $_SESSION['itens'];
           // echo var_dump($dados);
            $conteudo = $this->load->view("cart_view", $dados, true);
        } else {
            $conteudo = "Não temos itens no carrinho";
        }
        echo $conteudo;
       /* set_tema('conteudo', $conteudo);
        load_template();*/
    }
    public function pagseguro(){
    
        $this->load->model('compras_model','compras');
        
        $dados['idcompra']=$this->compras->do_cadastro();

       //echo $this->db->last_query(); 
        $dados['carrinho']=$_SESSION['carrinho'];
        $this->load->model('itens');
        
        $this->itens->do_cadastro($dados['idcompra']);
        $this->db->select("rua,bairro,cidade,estado,cep,complemento");
        $this->db->where(array('compras_tb.id'=>$dados['idcompra']));
        $this->db->join('compras_tb','compras_tb.cliente_tb_id=cliente_tb.id');
        $dados['cliente']=$this->db->get('cliente_tb')->result();
        
        echo $this->load->view('pagseguro',$dados,true);
        
       // unset($_SESSION['carrinho']);
        //unset($_SESSION['total']);
        //unset($_SESSION['itens']);
        
    }

}

/*
 * End of file carrinho.php
 * Location: application/controllers/carrinho.php
 */
?>

