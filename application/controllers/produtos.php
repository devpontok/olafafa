<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class produtos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_site();
    }

    public function index() {
        set_tema('footerinc', (load_js('http://gs-libs.googlecode.com/files/jquery.gs-innerzoom.min.js', 'js', TRUE)), false);
        set_tema('footerinc', load_js('funcao'), false);
        set_tema('footerinc', load_js('zoom'), false);
        $conteudo = "";
        $segs = $this->uri->segment_array();


        if (!is_numeric($segs[count($segs)])) {
            set_tema('conteudo', '<h1>O produto não foi localizado</h1>');
        } else {

            $id = $segs[count($segs)];
            $conteudo = '';
            $dados = array();
            unset($segs[1]);
            $dados['vigentes']=array();
            $this->load->model('promocoes_model','promocao');

            $dados['vigentes']=$this->promocao->promocao_vigente_produtos($id);
       //     echo var_dump($dados['vigentes']);
            //Montar dados de produto
            $this->load->model('produtos_model', 'produto');
            $this->db->join('estoque_tb', 'estoque_tb.produtos_tb_id=produtos_tb.id');
            $this->db->where(array('quantidade >' => '0'));
            $produto = $this->produto->get_productsById($id);
            if ($produto != false) {
                $dados['imagens'] = array();


                $this->load->model('categorias_model', 'categoria');
                $dados['produto'] = $produto[0];
                if (count($segs) == 1) {
    //pegar categorias do produto
                    $cat = $this->categoria->get_byId($dados['produto']->categorias_tb_id);
                    $cat2 = '';
                    $url = $cat[0]->alias;
                    if (!is_null($cat[0]->pai_id)) {
                        $cat2 = $this->categoria->get_byId($cat[0]->pai_id);
                        $url.='/' . $cat2[0]->alias;
                    }
                    $url = 'roupas-infantis/' . $url . "/{$id}";
                    redirect(base_url($url));
                } else {

                    $cat = $this->categoria->get_byAlias($segs[2]);
                }
                $dados['categoria'] = $cat[0];
                $dados['filhos'] = $this->categoria->get_categoriasFilhas($dados['categoria']->id);
                if ($dados['filhos']) {
                    $dados['sem_titulo'] = true;
                    $conteudo.= $this->load->view('submenu_view', $dados, TRUE);
                }
                set_tema('headerinc', load_css(array('bebe', 'zoom')), FALSE);
                $this->load->model('galeria_model', 'galeria');
                $this->db->where('produtos_tb_id', $id);
                $this->db->order_by('principal', 'desc');
                $dados['imagens'] = $this->galeria->get_all();

                /* obter tamanhos disponíveis */
                $this->db->select("produtos_tb.id, 
                    estoque_tb.tamanhos_id as tamanhos_id,
                    estoque_tb.id as estoque_id,
                    cor_id as cor,
                    modelo_id as modelo, 
                    estoque_tb.quantidade as qtd");
              //  $this->db->join('produto_tamanho', 'produto_tamanho.produtos_tb_id=produtos_tb.id');
            //    $this->db->join('tamanhos', 'produto_tamanho.tamanhos_id=tamanhos.id');
                $this->db->join('estoque_tb', 'estoque_tb.produtos_tb_id=produtos_tb.id', 'inner');
                $this->db->group_by('estoque_tb.id');
                $this->db->where(array('produtos_tb.id' => $id,'estoque_tb.quantidade >'=>0));
             //   $this->db->order_by('tamanhos.id asc');
                $dados['estoque'] = $this->db->get('produtos_tb')->result_array();
                $this->db->where(array('tipo_id'=>2));
                $tamanhos=$this->db->get('caracteristicas_tb')->result_array();
                $dados['tamanhos']=array();
                $dados['tamanhos'][0]='Tamanhos';
                foreach($tamanhos as $tamanho){
                    $dados['tamanhos'][$tamanho['id']]=$tamanho['nome'];
                }

                $this->db->where(array('tipo_id'=>1));
                $cores=$this->db->get('caracteristicas_tb')->result_array();
                $dados['cores']=array();
                $dados['cores'][0]='Cores';
                foreach($cores as $cor){
                    $dados['cores'][$cor['id']]=$cor['nome'];
                }

                $this->db->where(array('tipo_id'=>3));
                $modelos=$this->db->get('caracteristicas_tb')->result_array();
                $dados['modelos']=array();
                $dados['modelos'][0]='Modelos';
                foreach($modelos as $modelo){
                    $dados['modelos'][$modelo['id']]=$modelo['nome'];
                }
               /* fim dos tamanhos disponiveis */
                $conteudo.=$this->load->view('produtos_view', $dados, TRUE);
            } else {
                $conteudo = '<p id="error-perfil" >sem dados para este produto</p>';
            }
        }

        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function adiciona_carrinho() {

        if (!isset($_SESSION))
            session_start();
//tratamento de dados enviados
        //tipo_id=1 cor, tipo_id=2 tamanho, tipo_id=3 modelo
        $tamanhos2=$this->input->post('checks',true);
        $tamanhos2=explode("_",$tamanhos2);
        unset($tamanhos2[0]);
        $tamanhos=array();
        foreach($tamanhos2 as $tam){
            $tamanhos[$tam]=1;
        }
        
        //tamanho[0]=tamanho_id
        //tamanho[1]=qtd de itens deste tamanho
        $id = $this->input->post('id', TRUE);
        
        $data = array(
            'id' => $id,
            'qty' => $this->input->post('qtd', TRUE),
            'price' =>  $this->input->post('valor', TRUE),
            'name' => $this->input->post('nome', TRUE),
            'peso' => $this->input->post('peso', TRUE),
            'tamanhos' => $tamanhos
        );
//recuperar dados da sessão$tamanhos
        if (isset($_SESSION['carrinho'])) {
            $carrinho = $_SESSION['carrinho'];
//tratamento de adição de valores
            $adicionou = false;
            $cont = 0;
            foreach ($carrinho as $index=>$item) {
                if ($item['id'] == $id) {
                    //$carrinho[$cont] = $data;
                    foreach($tamanhos as $key=>$valor){
                        if(isset($carrinho[$index]['tamanhos'][$key])){
                            $carrinho[$index]['tamanhos'][$key]++;
                        }else{
                            $carrinho[$index]['tamanhos'][$key]=1;
                        }
                    }
                   // $carrinho[$index]=$data;
                    $adicionou = true;
                }
                $cont++;
            }
            if ($adicionou == false) {
                array_push($carrinho, $data);
            }
            $_SESSION['carrinho'] = $carrinho;
        } else {
            $_SESSION['carrinho'][0] = $data;
            $carrinho = $_SESSION['carrinho'];
            $_SESSION['itens']=0;
            $_SESSION['total'] =0;
        }
        
        /*  foreach ($carrinho as $item) {
          $dados_itens = count($item['tamanhos']) > 0 ? count($item['tamanhos']) : 1;
          $_SESSION['itens']+=$dados_itens;
          $_SESSION['total']+=($dados_itens * $item['price']);
          } */
        $this->determina_itens_total_carrinho();
        echo $_SESSION['total'] . '-' . $_SESSION['itens'];
    }

    public function remove_item_carrinho() {

        //remover tamanho de item do carrinho
        //não existe carrinho
        if (!isset($S_SESSION) || !isset($_SESSION['carrinho'])) {
            echo false;
        } else {
            //existe carrinho - obter carrinho
            $carrinho = $_SESSION['carrinho'];
            //busca elemento no carrinho
            $id = $this->input->post('id');
            $tamanho = $this->input->post('tamanho');
            foreach ($carrinho as $item) {
                if ($item['id'] == $id) {
                    //tamanho[0]=tamanho_id
                    //tamanho[1]=qtd de itens deste tamanho
                    $tamanhos = $item['tamanhos'];
                    $cont = 0;
                    foreach ($tamanhos as $tamanho) {
                        $cont++;
                    }
                }
            }
        }

        //item existe no carrinho?
        //buscar remover item do carrinho
    }

    //manipula via ajax a quantidade de itens do carrinho
    public function aumenta_item_carrinho() {
        //obter os itens do carrinho
        $produto = $this->input->post('produto_id');
        $qtd = $this->input->post('quantidade');
    //    echo $qtd;
        $tamanho_recebido = $this->input->post('tamanho_id');
    //    echo ' '.$tamanho_recebido;
        if (!is_null($tamanho_recebido) && !is_null($produto) && !is_null($qtd)) {
            if (!isset($_SESSION)) {
                session_start();
                //     echo "Carrinho vazio";
            } else {
                if (empty($_SESSION['carrinho']) || !isset($_SESSION['carrinho'])){
                    //       echo "Carrinho vazio";
                } else {
                    $carrinho = $_SESSION['carrinho'];
                    $cont = array_keys($carrinho);
                    foreach ($carrinho as $item) {
                        if ($item['id'] == $produto) {
                            foreach ($cont as $count) {
                               // $contador = 0;
                                //adição de itens com múltiplos tamanhos
                                if (count($carrinho[$count]['tamanhos']) > 0) {
                                    foreach ($item['tamanhos'] as $key=>$tamanho) {
                                        //buscar tamanho a atualizar
                                        //tamanho[0]=tamanho_id
                                        //tamanho[1]=qtd de itens deste tamanho
                                        if ($key == $tamanho_recebido) {
                                            //achou tamanho, atualiza quantidade do item no carrinho
                                            if ($qtd != 0) {
                                                if (count($carrinho[$count]['tamanhos']) > 0) {
                                                  //  $carrinho[$count]['tamanhos'][$contador]['1'] = $qtd;
                                                    $carrinho[$count]['tamanhos'][$tamanho_recebido]=$qtd;
                                                } else {
                                                    $carrinho[$count]['qty'] = $qtd;
                                                }
                                            } else {
                                                if (count($carrinho[$count]['tamanhos']) > 0) {
                                                    if (count($carrinho[$count]['tamanhos']) == 1) {
                                                        unset($carrinho[$count]);
                                                    } else {
                                                        unset($carrinho[$count]['tamanhos'][$tamanho_recebido]);
                                                    }
                                                } else {
                                                    unset($carrinho[$count]);
                                                }
                                            }
                                            $_SESSION['carrinho'] = $carrinho;
                                            $this->determina_itens_total_carrinho();
                                            echo $_SESSION['total'] . '-' . $_SESSION['itens'];
                                        }
                                       // $contador++;
                                    }
                                    //fim da adição de itens com múltiplos tamanhos
                                } else {
                                    //erro
                                    if ($carrinho[$count]['id'] == $produto) {
                                        if ($qtd == 0) {
                                            unset($carrinho[$count]);
                                        } else {
                                            $carrinho[$count]['qty'] = $qtd;
                                        }
                                        $_SESSION['carrinho'] = $carrinho;
                                        $this->determina_itens_total_carrinho();
                                        echo $_SESSION['total'] . '-' . $_SESSION['itens'];
                                    }
                                }
                            }
                        }
                    }
                }
                //            echo 'carrinho atualizado com sucesso';
            }
        } else {
            echo "Faltam dados para modificar o carrinho";
        }
    }

    private function determina_itens_total_carrinho() {
        $_SESSION['itens'] = 0;
        $_SESSION['total'] = 0;
        $carrinho = $_SESSION['carrinho'];
        foreach ($carrinho as $item) {
            //   echo count($item['tamanhos']);
          /*  if (count($item['tamanhos']) == 0) {
                $_SESSION['itens']+=(int) $item['qty'];
                $_SESSION['total']+=((int) $item['qty'] * (float) $item['price']);
            } else {*/
                if (is_array($item['tamanhos']) && count($item['tamanhos']) > 0) {
                    foreach ($item['tamanhos'] as $tamanhos) {
                        $_SESSION['itens']+=(int) $tamanhos;
                        $_SESSION['total']+=((int) $tamanhos * (float) $item['price']);
                    }
                }
           // }
        }
    }

    public function tem_tamanhos() {
        $id = $this->input->post('id');
        $ids= $this->input->post('ids');
        if (!is_null($id)) {
            $this->db->where(array('produtos_tb_id' => $id,'estoque_tb.quantidade >'=>0));
            $dados = $this->db->get('estoque_tb')->result();
            echo count($dados);
        } else {
            echo 'Não pode verificar produto';
        }
    }

    public function destroi_carrinho() {
        unset($_SESSION['carrinho']);
        unset($_SESSION['total']);
        unset($_SESSION['itens']);
    }

}

/*
 * End of file produtos.php
 * Location: application/controllers/produtos.php
 */
?>

