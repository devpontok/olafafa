<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class site extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_site();
    }

    public function index() {

        //Buscar produtos com destaques
        $this->load->model('produtos_model', 'produto');
        $dados['pasta'] = get_pathServidor();
        //buscar dados de destaques
        $this->db->limit(6);
        $this->db->join('estoque_tb','estoque_tb.produtos_tb_id=produtos_tb.id');
        $this->db->where(array('estoque_tb.quantidade >'=>0));
        $this->db->group_by('produtos_tb.id');
        $dados['destaques'] = $this->produto->get_productsByDestaques();


        //buscar dados de carrossel

        $dados['carrossel'] = scandir($dados['pasta'] . 'galeria/destaques');
        $conteudo = $this->load->view('home/destaques_view', $dados, true);
        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function deslogar() {
        $this->session->sess_destroy();
    }

    public function logado() {
        echo esta_logado(FALSE);
    }

    public function mostrar_carrinho() {
        $conteudo = '';
        if (esta_logado(FALSE)) {
            $this->load->model('compras_model', 'compras');
            $user = $this->session->userdata('user_id');
            $this->db->where('compras_tb.cliente_tb_id', $user);
            $compras = $this->compras->get_all();
            $compras_itens = array();
            $this->load->model('itens');
            foreach ($compras as $compra) {
                //array id_compra, valor_total,data_compra,status

                $compras_itens['id_compra'] = $compra->compra;
                $compras_itens['valor_total'] = $compra->valor;
                $compras_itens['data_compra'] = $compra->data_compra;
                $compras_itens['status'] = $compra->status;
                $compras_itens['itens'] = array();
                //busca itens para adicionar no array['itens']=>array de itens da compra
                $itens_dados = $this->itens->get_all($compra->compra);
                echo var_dump($itens_dado);
                // $dados_compra=$itens->get_all();
            }
            echo var_dump($compras);
        } else {
            $conteudo = "<h1>Desculpe, mas você não pode acessar esta área sem realizar login</h1>";
        }
        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function cadastro() {
        //      init_site();
        $this->load->library('session');
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nome', 'Nome de Usuário', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('nome', 'Telefone', "trim|required|strtolower");
        $this->form_validation->set_rules('senha', 'Senha', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('email', 'Email', "trim|required|min_length[4]|strtolower|is_unique[login_tb.email]|valid_email");
        $this->form_validation->set_rules('login', 'Login', "trim|required|min_length[4]|strtolower|is_unique[login_tb.login]");
        $this->form_validation->set_rules('data_nascimento', 'Data de nascimento', "trim|min_length[8]|strtolower");
        $this->form_validation->set_rules('cpf', 'CPF', "trim|required|min_length[11]|strtolower|is_unique[cliente_tb.cpf]");
        $this->form_validation->set_rules('rua', 'Rua', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('bairro', 'Bairro', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('cidade', 'Cidade', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('estado', 'Estado', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('cep', 'CEP', "trim|required|min_length[4]|strtolower");

        $this->form_validation->set_rules('complemento', 'Complemento', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('aceite', 'Aceite', "required");

        if ($this->form_validation->run() == TRUE):
            $nome = $this->input->post('nome', TRUE);
            $tel = $this->input->post('tel', TRUE);
            $senha = md5($this->input->post('senha', TRUE));
            $email = $this->input->post('email', TRUE);
            $login = $this->input->post('login', TRUE);
            $data_nascimento = $this->input->post('data_nascimento', TRUE);
            $cpf = $this->input->post('cpf', TRUE);
            $rua = $this->input->post('rua', TRUE);
            $bairro = $this->input->post('bairro', TRUE);
            $cidade = $this->input->post('cidade', TRUE);
            $estado = $this->input->post('estado', TRUE);
            $cep = $this->input->post('cep', TRUE);
            $complemento = $this->input->post('complemento', TRUE);
            $aceite = $this->input->post('aceite', TRUE);

            $this->load->model('usuarios_model', 'usuarios');

            $query = $this->usuarios->do_cadastro($nome, $senha, $email, $login, $data_nascimento, $cpf, $rua, $bairro, $cidade, $estado, $cep, $complemento, $aceite, $tel);
            if ($query != FALSE) {
                $dados = array(
                    'user_id' => $query,
                    'user_nome' => $nome,
                    'user_logado' => true
                );

                $this->session->set_userdata($dados);

                redirect('');
            } else {
                echo 'Cadastro falhou';
            }
        endif;
        $dados['tela'] = 'usuario';
        $conteudo = $this->load->view('cadastro', $dados, true);
        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function perfil() {
        //   init_site();
        $this->load->library('session');
        $logado = esta_logado(false);
        $conteudo = '';
        if ($logado == true) {
            $this->load->helper(array('form'));
            $this->load->library('form_validation');
            $dados = array();
            //usuário  logado na sessão
            $user_id = $this->session->userdata('user_id');

            //Dados de compra
            $this->load->model('compras_model', 'compras');
            $this->db->where(array('compras_tb.cliente_tb_id' => $user_id));
            $this->db->limit(1000);
            $compras = $this->compras->get_all();
            //Dados de usuários
            $dados['compras'] = array();
            $cont = 0;
            foreach ($compras as $compra) {
                $dados['compras'][$cont]['compra'] = $compra->compra;
                $dados['compras'][$cont]['data_compra'] = $compra->data_compra;
                $dados['compras'][$cont]['valor'] = $compra->valor;
                $this->load->model('itens');
                $dados['compras'][$cont]['itens'] = $this->itens->get_byCompra($compra);
                // echo var_dump($dados['compras'][$cont]['itens']);
                $cont++;
            }
            $this->load->model('usuarios_model', 'usuarios');

            $this->db->where(array('cliente_tb.id' => $user_id));
            $usuario = $this->usuarios->get_all();
            $dados['rua'] = isset($usuario[0]->rua) ? $usuario[0]->rua : '';
            $dados['bairro'] = isset($usuario[0]->bairro) ? $usuario[0]->bairro : '';
            $dados['cidade'] = isset($usuario[0]->cidade) ? $usuario[0]->cidade : '';
            $dados['estado'] = isset($usuario[0]->estado) ? $usuario[0]->estado : '';

            $dados['cep'] = isset($usuario[0]->cep) ? $usuario[0]->cep : '';
            $dados['nome'] = isset($usuario[0]->nome) ? $usuario[0]->nome : '';
            $dados['login'] = isset($usuario[0]->login) ? $usuario[0]->login : '';
            $dados['email'] = isset($usuario[0]->email) ? $usuario[0]->email : '';
            $dados['cpf'] = isset($usuario[0]->cpf) ? $usuario[0]->cpf : '';
            $dados['senha'] = "";
            $dados['nascimento'] = isset($usuario[0]->data_nascimento) ? $usuario[0]->data_nascimento : '';
            $dados['complemento'] = isset($usuario[0]->complemento) ? $usuario[0]->complemento : '';
            $dados['telefone'] = isset($usuario[0]->telefone) ? $usuario[0]->telefone : '';

            $this->form_validation->set_rules('nome', 'Nome de Usuário', "trim|required|min_length[4]|strtolower");
            $this->form_validation->set_rules('telefone', 'Telefone', "trim|required|strtolower");
            $this->form_validation->set_rules('senha', 'Senha', "trim|min_length[4]|strtolower");
            $this->form_validation->set_rules('email', 'Email', "trim|required|min_length[4]|strtolower");
            $this->form_validation->set_rules('login', 'Login', "trim|required|min_length[4]|strtolower");
            $this->form_validation->set_rules('data_nascimento', 'Data de nascimento', "trim|min_length[8]|strtolower");
            $this->form_validation->set_rules('cpf', 'CPF', "trim|required|min_length[11]|strtolower");
            $this->form_validation->set_rules('rua', 'Rua', "trim|required|min_length[4]|strtolower");
            $this->form_validation->set_rules('bairro', 'Bairro', "trim|required|min_length[4]|strtolower");
            $this->form_validation->set_rules('cidade', 'Cidade', "trim|required|min_length[4]|strtolower");
            $this->form_validation->set_rules('estado', 'Estado', "trim|required|min_length[4]|strtolower");
            $this->form_validation->set_rules('cep', 'CEP', "trim|required|min_length[4]|strtolower");

            $this->form_validation->set_rules('complemento', 'Complemento', "trim|required|min_length[4]|strtolower");

            if ($this->form_validation->run() == TRUE):
                $nome = $this->input->post('nome', TRUE);
                $tel = $this->input->post('tel', TRUE);
                $senha = ($this->input->post('senha', TRUE) != '') ? md5($this->input->post('senha', TRUE)) : NULL;
                $email = $this->input->post('email', TRUE);
                $login = $this->input->post('login', TRUE);
                $data_nascimento = $this->input->post('data_nascimento', TRUE);
                $tel = $this->input->post('telefone', TRUE);
                $cpf = $this->input->post('cpf', TRUE);
                $rua = $this->input->post('rua', TRUE);
                $bairro = $this->input->post('bairro', TRUE);
                $cidade = $this->input->post('cidade', TRUE);
                $estado = $this->input->post('estado', TRUE);
                $cep = $this->input->post('cep', TRUE);
                $complemento = $this->input->post('complemento', TRUE);
                $aceite = $this->input->post('aceite', TRUE);

                $this->load->model('usuarios_model', 'usuarios');

                $query = $this->usuarios->do_update($user_id, $nome, $senha, $email, $login, $data_nascimento, $cpf, $rua, $bairro, $cidade, $estado, $cep, $complemento, $tel);
                if ($query == FALSE) {

                    echo 'Cadastro falhou';
                } else {
                    redirect(base_url("site/perfil"));
                }
            endif;

            $conteudo = $this->load->view('home/perfil_view', $dados, true);
        } else {
            $conteudo.="<p id='error-perfil'>Para acessar esta página é preciso fazer login</p>";
        }

        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function checa_email($email = NULL) {
        if (is_null($email)) {
            $email = $this->input->post('email');
        }
        if (!is_null($email)) {
            $segunda_parte = explode("@", $email);
            if (count($email)!=2 || strpos('.', $segunda_parte[1]) < 0) {
                echo "Digite um email válido";
            } else {
                $usuario = $this->db->query("select cliente_tb_id from login_tb where email like '{$email}'")->result();
                if ($usuario != FALSE && !is_null($usuario[0]->id)) {
                    $user = $this->session->userdata('user_id');
                    if (!empty($user)) {
                        if ($user != $usuario[0]->id) {
                            echo 'Email já cadastrado';
                        } else {
                            echo 0;
                        }
                    } else {
                        echo 'Email já cadastrado';
                    }
                } else {
                    echo 0;
                }
            }
        } else {
            echo 'Digite um e-mail válido, sem ele não poderá cadastrar';
        }
    }

    public function checa_login($login = NULL) {
        if (is_null($login)) {
            $login = $this->input->post('login');
        }

        $usuario = $this->db->query("select cliente_tb_id as id from login_tb where login = '{$login}'")->result();

        if ($usuario != FALSE && !is_null($usuario[0]->id)) {
            $user = $this->session->userdata('user_id');

            if (!empty($user)) {
                if ($user != $usuario[0]->id) {
                    echo 'Login já cadastrado';
                } else {
                    echo 0;
                }
            } else {
                echo 'Login já cadastrado';
            }
        } else {
            echo 0;
        }
    }

    public function checa_cpf($cpf = NULL) {
        if (is_null($cpf)) {
            $cpf = $this->input->post('cpf');
        }
        if (!is_null($cpf)) {
            $valido = verifica_cpf($cpf);
            if ($valido) {
                echo "Digite um cpf válido";
            } else {
                $user = $this->session->user_id;
                $usuario = $this->db->query("select cliente_tb_id from cliente_tb where cpf like '{$cpf}'")->result();
                if (isset($user)) {
                    if ($user != $usuario['id']) {
                        echo 'CPF já cadastrado';
                    } else {
                        echo 0;
                    }
                } else {
                    echo 'CPF já cadastrado';
                }
            }
        } else {
            echo 'Digite um CPF válido, sem ele não poderá cadastrar';
        }
    }

    public function verifica_cpf($cpf = null) {

        // Verifica se um número foi informado
        if (empty($cpf)) {
            return false;
        }

        // Elimina possivel mascara
        $cpf = ereg_replace('[^0-9]', '', $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        // Verifica se o numero de digitos informados é igual a 11 
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se nenhuma das sequências invalidas abaixo 
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cpf == '00000000000' ||
                $cpf == '11111111111' ||
                $cpf == '22222222222' ||
                $cpf == '33333333333' ||
                $cpf == '44444444444' ||
                $cpf == '55555555555' ||
                $cpf == '66666666666' ||
                $cpf == '77777777777' ||
                $cpf == '88888888888' ||
                $cpf == '99999999999') {
            return false;
            // Calcula os digitos verificadores para verificar se o
            // CPF é válido
        } else {

            for ($t = 9; $t < 11; $t++) {

                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }

            return true;
        }
    }
   

}

/*
 * End of file site.php
 * Location: application/controllers/site.php
 */
?>

