<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class categorias extends CI_Controller {

    private $conteudo;

    public function __construct() {
        parent::__construct();
        init_painel();
        $this->load->model('categorias_model', 'categoria');
        $this->conteudo = $this->load->view('adm/menu_view', '', TRUE);
    }

    public function index() {
        $this->load->view("");
    }

    public function cadastro($id=null) {
        $this->form_validation->set_rules('nome','Nome da categoria',"trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('categoria','Categoria pai',"trim|required|strtolower");
        if($this->form_validation->run()==true):
            $nome=$this->input->post('nome',true);
            $categoria=$this->input->post('categoria',true);
            $alias=cria_url($nome);
            $this->categoria->do_cadastro(ucfirst(strtoupper(trim($nome))),$categoria,$alias);
        endif;
        if(!is_null($id)){
            $categoria=$this->categoria->get_byId($id);
            if($categoria!=FALSE){
                $dados['nome']=$categoria[0]->nome;
                
                 $dados['select'] = array($categoria[0]->categoria_pai);
                 $dados['action']="adm/categorias/atualiza";
            }
        }else{
             $dados['select'] = array(0);
             $dados['action']="adm/categorias/cadastro";
        }
        $this->db->where('pai_id',NULL);
        $categorias = $this->categoria->get_all();
        $dados['options'] = array();
        $dados['options'][''] = '';
        foreach ($categorias as $cat) {
            $dados['options'][$cat->id] = $cat->nome;
        }
        
       
        $this->conteudo.=$this->load->view('adm/categorias_view',$dados,TRUE);
        set_tema('conteudo',$this->conteudo);
        load_template();
        
    }

}

/*
 * End of file categorias.php
 * Location: application/controllers/categorias.php
 */
?>

