<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class compras extends CI_Controller {

    private $conteudo;

    public function __construct() {
        parent::__construct();
        init_painel();
        $this->conteudo = $this->load->view('adm/menu_view', '', TRUE);
    }

    public function index() {
        $this->load->view("");
    }

    public function lista() {
        $this->load->model('compras_model', 'compras');
        $compras=$this->compras->get_all();
     //   echo var_dump($compras);
        $cont=0;
        $dados['total']=0;
        $dados['compras']=array();
        foreach($compras as $compra){

            $dados['compras'][$cont]['compra']=$compra->compra;
            $dados['compras'][$cont]['email']=$compra->email;
            $dados['compras'][$cont]['nome']=$compra->nome;
            $dados['compras'][$cont]['data_compra']=$compra->data_compra;
            $dados['compras'][$cont]['status']=$compra->status;
            $dados['compras'][$cont]['valor']=$compra->valor;
            if($compra->status=='p' || $compra->status=='l' ){ /*$compra->status=='c' || */ 
                $dados['total']+=$compra->valor;
            }
            $dados['compras'][$cont]['forma_pagamento']=$compra->forma_pagamento;
            $dados['compras'][$cont]['taxas']=$compra->taxas;
            $dados['compras'][$cont]['itens']=array();
            $contador_itens=0;
            $itens=$this->compras->get_itensCompra($compra->compra);
            foreach($itens as $item){
                $dados['compras'][$cont]['itens'][$contador_itens]="{$item->id}-{$item->nome}: {$item->quantidade}";
                $contador_itens++;
            }
            $cont++;
        }
        //Sessão para evitar listar novamente as compras
        if(!isset($_SESSION)){
            session_start();
        }
        $_SESSION['compras']= $dados['compras'];
        $_SESSION['periodo']='';
 //       set_tema('footerinc', load_js(array('html5','prettify','jquery-1.9','bootstrap-datepicker','function'), 'js/adm', FALSE));
        set_tema('headerinc', load_css(array('bootstrap', 'flat-ui', 'demo','ui-lightness/jquery-ui-1.10.4.custom','ui-lightness/jquery-ui-1.10.4.custom.min','estilo'), 'css/adm'), FALSE);
        set_tema('footerinc', load_js(array('jquery-1.10.2.min', 'jquery-ui-1.10.4.custom',
                                        'jquery.ui.touch-punch.min', 'bootstrap.min',
                                        'bootstrap-select','bootstrap-switch',
                                        'flatui-checkbox', 'flatui-radio',
                                        'jquery.tagsinput', 'jquery.placeholder',
                                        'jquery.stacktable', 'application',
                                        'mask',
                                        'bootstrap-modal','function'), 'js/adm', FALSE));
        $this->conteudo.= $this->load->view('adm/compras_view', $dados, true);
        set_tema('conteudo', $this->conteudo);
        load_template();
    }
    public function liberar_frete(){
        $id=$this->input->post('id',true);
        $this->load->model('compras_model','compras');
        $this->compras->atualiza_frete($id);
    }
    public function imprimir_excel(){
        if(!isset($_SESSION)){
            session_start();
        }

        $data=date('Y_m_d');
        $compras['compras']=$_SESSION['compras'];
        $data_composta=explode("_", $data);
        $compras['periodo']= (isset($_SESSION['periodo']) && !empty($_SESSION['periodo']))?mb_convert_encoding($_SESSION['periodo'],'utf-8'):"";
        $compras['data']=$data_composta[2]."/".$data_composta[1]."/".$data_composta[0];
        $str=$this->load->view('adm/tabela_compras_view',$compras,true);
     
        $this->load->helper('file');
        $arquivo=get_pathServidor();
        $arquivo.="relatorios/relatorio_vendas_{$data}.xls";
   //     echo $str;
   //     echo $arquivo;
        if(!write_file($arquivo,$str)){
            echo '0';
        }else{
            echo "relatorios/relatorio_vendas_{$data}.xls";
        }

    }
    public function imprimir_pdf(){
        if(!isset($_SESSION)){
            session_start();
        }

        $data=date('Y_m_d');
        $compras['compras']=$_SESSION['compras'];
        $data_composta=explode("_", $data);
        $compras['periodo']= (isset($_SESSION['periodo']) && !empty($_SESSION['periodo']))?mb_convert_encoding($_SESSION['periodo'],'utf-8'):"";
        $compras['data']=$data_composta[2]."/".$data_composta[1]."/".$data_composta[0];
        $str=$this->load->view('adm/tabela_compras_view',$compras,true);
     //   echo $str;
        $arquivo=get_pathServidor();
        $arquivo.="relatorios/relatorio_vendas_{$data}.pdf";
  //      echo $arquivo;
        $this->load->helper(array('dompdf', 'file'));
         
      //   $data=pdf_create($str,$arquivo);
        $dado = pdf_create($str, '', false);
        if(!write_file($arquivo,$dado)){
            echo '0';
        }else{
            echo "relatorios/relatorio_vendas_{$data}.pdf";
        }

    }
    /*public function monta_tabela(){
        $conteudo=$this->load->view('tabela_compras_view',$dados,true);
    }*/
    public function pesquisa(){
        $data_inicial=$this->input->post('data_inicial',true);
        $data_final=$this->input->post('data_final',true);
        $dados['compras']=array();
        if(!empty($data_inicial) || !empty($data_final)){
            $this->load->model('compras_model', 'compras');
            if(!isset($_SESSION)){
             session_start();
            }
            $_SESSION['periodo']="Período de pesquisa ";
             if(!empty($data_inicial)){
                $_SESSION['periodo'].="de {$data_inicial} ";
                $data=explode("/",$data_inicial);
                $data_inicial="{$data[2]}-{$data[1]}-{$data[0]}";
             }
             if(!empty($data_final)){
                $_SESSION['periodo'].="até {$data_final}";
                $data=explode("/",$data_final);
                $data_final="{$data[2]}-{$data[1]}-{$data[0]}";
             }
            $compras=$this->compras->get_dados($data_inicial,$data_final);
          //  echo var_dump($compras);
            $cont=0;
            $dados['total']=0;
            
            foreach($compras as $compra){
                $dados['compras'][$cont]['compra']=$compra->compra;
                $dados['compras'][$cont]['email']=$compra->email;
                $dados['compras'][$cont]['nome']=$compra->nome;
                $dados['compras'][$cont]['data_compra']=$compra->data_compra;
                $dados['compras'][$cont]['status']=$compra->status;
                $dados['compras'][$cont]['valor']=$compra->valor;
                if($compra->status=='l' || $compra->status=='p'){
                    $dados['total']+=$compra->valor;
                }
                $dados['compras'][$cont]['forma_pagamento']=$compra->forma_pagamento;
                $dados['compras'][$cont]['taxas']=$compra->taxas;
                $dados['compras'][$cont]['itens']=array();
                $contador_itens=0;
                $itens=$this->compras->get_itensCompra($compra->compra);
                foreach($itens as $item){
                    $dados['compras'][$cont]['itens'][$contador_itens]="{$item->id}-{$item->nome}: {$item->quantidade}";
                    $contador_itens++;
                }
                $cont++;
            }
            $_SESSION['compras']=$dados['compras'];
            if(empty($dados['compras'])){
                $dados['mensagem']="N&atilde;o existem para este per&iacute;odo";
            }
        echo $this->load->view('adm/compras_view', $dados, true);
       }else{
            $dados['mensagem']='Escolha datas viáveis para a pesquisa';
            echo $this->load->view('adm/compras_view', $dados, true);
       }
    }
     public function itens($id) {
        $this->load->model('itens', 'compras');
        $this->db->where(array('compras_tb_id'=>$id));
        $dados['itens']=$this->compras->get_all();
        $this->conteudo.= $this->load->view('adm/itens_view', $dados, true);
        set_tema('conteudo', $this->conteudo);
        load_template();
    }
}

/*
 * End of file compras.php
 * Location: application/controllers/compras.php
 */
?>

