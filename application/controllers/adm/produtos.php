<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class produtos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_painel();
    }

    public function index() {
        $this->load->view("");
        init_painel();
        $this->load->model('produtos_model', 'produto');
    }

    public function lista() {
        //Categorias para pesquisa
        $this->load->model('categorias_model', 'categoria');
        $categorias = $this->categoria->get_all();
        $dados['options'] = array();
        $dados['options'][0] = '';
        foreach ($categorias as $cat) {
            $dados['options'][$cat->id] = $cat->nome;
        }
        $dados['select'] = array(0);

        $conteudo = $this->load->view('adm/menu_view', $dados, true);
        //Pegar produtos para montar grid de listagem
        $this->load->model('produtos_model', 'produto');
        $this->db->select('categorias_tb.alias as categoria_nome, produtos_tb.id,produtos_tb.nome,produtos_tb.frete_gratis as frete_gratis');
        $this->db->join('categorias_tb', 'categorias_tb.id=produtos_tb.categorias_tb_id');
        $this->db->group_by('produtos_tb.id');

        //CHECAR SE O FRETE É GRATUITO EM TODOS OS PRODUTOS DO SITE
        $query=$this->db->query('select count(id) as produtos from produtos_tb where frete_gratis=1');
        $row=$query->row();

        $query=$this->db->query('select count(id) as produtos from produtos_tb');
        $row2=$query->row();
        $dados['frete_gratis']=($row->produtos==$row2->produtos)?true:false;

        $dados['tela'] = 'lista';
        $dados['produtos'] = $this->produto->get_all(NULL, 20, 0);

        $conteudo.=$this->load->view('adm/produto', $dados, true);
        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function atualiza($id = NULL) {
        $conteudo = "";
        set_tema('headerinc',load_css(array('adm/bootstrap.min','adm/style','adm/blueimp-gallery.min','adm/jquery.fileupload',
            'adm/jquery.fileupload-ui')),FALSE);
//,'adm/tmpl.min'
      /*  set_tema('footerinc',load_js(array('adm/jquery-1.10.2','adm/jquery.ui.widget','adm/load-image.min','adm/canvas-to-blob.min',
            'adm/jquery-ui-1.10.4.custom','adm/bootstrap.min','adm/jquery.blueimp-gallery.min',
            'adm/jquery.iframe-transport','adm/jquery.fileupload',
            'adm/jquery.fileupload-process','adm/jquery.fileupload-image',
            'adm/jquery.fileupload-audio','adm/jquery.fileupload-video',
            'adm/jquery.fileupload-validate','adm/jquery.fileupload-ui','mask','adm/function')),FALSE);*/
        $dados['produto']=null;
        $dados['id']=$id;
        $this->db->where('produtos_tb_id', $id);
        $this->load->model('galeria_model', 'galeria');
        $dados['galeria'] = $this->galeria->get_all();
        $this->load->model('produtos_model', 'produto');
        $product = $this->produto->get_productsById($id);
        $this->load->model('categorias_model', 'categoria');
        $categorias = $this->categoria->get_all();
        $dados['options'] = array();
        foreach ($categorias as $cat) {
            $dados['options'][$cat->id] = $cat->nome;
        }
        $dados['tela'] = 'cadastro';
        $dados['select'] = array();
        $dados['action'] = base_url('adm/produtos/atualiza/' . $id);



        if (!is_null($id) && is_array($product)) {
            $this->load->library('upload');

            $this->load->model('categorias_model', 'categoria');
            $dados['destaques'] = '';
            if (!is_null($product)) {
                $dados['nome'] = $product[0]->nome;
                $dados['valor'] = $product[0]->valor;
                $dados['peso'] = $product[0]->peso;
                $dados['detalhes'] = $product[0]->detalhes;
                $dados['id'] = $product[0]->id;
                $dados['destaques'] = ($product[0]->destaques == 1) ? 'checked' : '';
                $dados['frete_gratis']=$product[0]->frete_gratis;
                $dados['select'] = array($product[0]->categorias_tb_id);
                $cat_nome = $this->categoria->get_byId($product[0]->categorias_tb_id);
                $imagem = get_imagemProduto($cat_nome[0]->alias, $id);
                $dados['imagem'] = $imagem['url'];
            }

            /* montar os componentes de tamanhos */
            //$dados['select'] = array();
            $this->db->order_by('nome','desc');
            $tamanhos = $this->db->get('tamanhos');


            $result = $tamanhos->result_array();

            $dados['tamanho'] = $result;

            $dados['selected_tam'] = array();
            $this->db->where(array('produtos_tb_id' => $id));
            $result = $this->db->get('produto_tamanho')->result();

            foreach ($result as $linha) {
                array_push($dados['selected_tam'], $linha->tamanhos_id);
            }

            /* fim do carregamento dos tamanhos possíveis */
            $this->form_validation->set_rules('nome', 'Nome', "trim|required|min_length[4]|strtolower|xss_clean");
            $this->form_validation->set_rules('valor', 'Preço', "trim|required|decimal|xss_clean");
            $this->form_validation->set_rules('peso', 'Peso', "trim|decimal|xss_clean");
            $this->form_validation->set_rules('detalhes', 'Detalhes', "trim|max_length[600]|strtolower|xss_clean");
            $this->form_validation->set_rules('destaque', 'Destaques', "xss_clean");
            $this->form_validation->set_rules('categoria', 'Categoria', "trim|required|numeric|xss_clean");
            $dados['tela'] = 'cadastro';
            $dados['action'] = "adm/produtos/atualiza/{$id}";
            if ($this->form_validation->run() == TRUE):
                $nome = $this->input->post('nome', TRUE);
            $valor = $this->input->post('valor', TRUE);
            $peso = $this->input->post('peso', TRUE);
            $detalhes = $this->input->post('detalhes', TRUE);
            $categoria = $this->input->post('categoria', true);
            $destaque = $this->input->post('destaque') == 'destaque';
            $tamanhos = array();
            if ($this->input->post('1', TRUE)) {
                array_push($tamanhos, '1');
            }
            if ($this->input->post('2', TRUE)) {
                array_push($tamanhos, '2');
            }
            if ($this->input->post('3', TRUE)) {
                array_push($tamanhos, '3');
            }
            if ($this->input->post('4', TRUE)) {
                array_push($tamanhos, '4');
            }
            if ($this->input->post('5', TRUE)) {
                array_push($tamanhos, '5');
            }
            if ($this->input->post('6', TRUE)) {
                array_push($tamanhos, '6');
            }

            if ($this->input->post('7', TRUE)) {
                array_push($tamanhos, '7');
            }
            if ($this->input->post('8', TRUE)) {
                array_push($tamanhos, '8');
            }
            if ($this->input->post('9', TRUE)) {
                array_push($tamanhos, '9');
            }
            if ($this->input->post('10', TRUE)) {
                array_push($tamanhos, '10');
            }
            if ($this->input->post('11', TRUE)) {
                array_push($tamanhos, '11');
            }
            if ($this->input->post('12', TRUE)) {
                array_push($tamanhos, '12');
            }
            if ($this->input->post('13', TRUE)) {
                array_push($tamanhos, '13');
            }
            if ($this->input->post('14', TRUE)) {
                array_push($tamanhos, '14');
            }
            if ($this->input->post('15', TRUE)) {
                array_push($tamanhos, '15');
            }
            if ($this->input->post('16', TRUE)) {
                array_push($tamanhos, '16');
            }
            if ($this->input->post('17', TRUE)) {
                array_push($tamanhos, '17');
            }
            if ($this->input->post('18', TRUE)) {
                array_push($tamanhos, '18');
            }

            if ($this->input->post('19', TRUE)) {
                array_push($tamanhos, '19');
            }
            if ($this->input->post('20', TRUE)) {
                array_push($tamanhos, '20');
            }
            if ($this->input->post('21', TRUE)) {
                array_push($tamanhos, '21');
            }
            if ($this->input->post('22', TRUE)) {
                array_push($tamanhos, '22');
            }
            if ($this->input->post('23', TRUE)) {
                array_push($tamanhos, '23');
            }
            if ($this->input->post('24', TRUE)) {
                array_push($tamanhos, '24');
            }

            if ($this->input->post('25', TRUE)) {
                array_push($tamanhos, '25');
            }
            if ($this->input->post('26', TRUE)) {
                array_push($tamanhos, '26');
            }
            if ($this->input->post('27', TRUE)) {
                array_push($tamanhos, '27');
            }
            if ($this->input->post('28', TRUE)) {
                array_push($tamanhos, '28');
            }
            if ($this->input->post('29', TRUE)) {
                array_push($tamanhos, '29');
            }
            if ($this->input->post('30', TRUE)) {
                array_push($tamanhos, '30');
            }
            if ($this->input->post('31', TRUE)) {
                array_push($tamanhos, '31');
            }
            if ($this->input->post('32', TRUE)) {
                array_push($tamanhos, '32');
            }
            if ($this->input->post('33', TRUE)) {
                array_push($tamanhos, '33');
            }
            if ($this->input->post('34', TRUE)) {
                array_push($tamanhos, '34');
            }
                //echo var_dump($tamanhos);
            if ($this->produto->do_update($nome, $valor, $peso, $detalhes, $destaque, $categoria, $id)) {

                    //Trocar imagem produto
                $conteudo.="<div class='col-md-12  form-group has-success'><input type='text' class='form-control' value='O produto foi atualizado com sucesso!'/></div>";
               /* if ($_FILES['files']['name'] != '') {
                    if (isset($imagem['path'])) {
                        unlink($imagem['path']);
                    }
                    $cat = $this->categoria->get_byId($categoria);
                    $pasta = get_pathServidor() . "galeria/roupas/";
                    $img = "{$pasta}{$cat[0]->alias}{$id}";
                    $conteudo.=upload($pasta, $img, $_FILES);
                    $this->load->model('galeria_model', 'galeria');
                    $ext = '.' . substr($_FILES['files']['type'], strpos($_FILES['files']['type'], '/') + 1);
                    $img = "{$cat[0]->alias}{$id}{$ext}";
                    $this->galeria->do_update($img, $id, 1);
                }*/
                $this->db->query("delete from produto_tamanho where produtos_tb_id=$id");
                foreach ($tamanhos as $tamanho) {
                    $data['produtos_tb_id'] = $id;
                    $data['tamanhos_id'] = $tamanho;
                    $str = $this->db->insert_string('produto_tamanho', $data);
                    $this->db->query($str);
                }
            } else {
                $conteudo.="<div class='col-md-12  form-group has-success'><input type='text' class='form-control' value='O produto não foi atualizado com sucesso!'/></div>";
            }
            endif;
            $dados['update']=true;
           // echo var_dump($dados);
            $conteudo = $this->load->view('adm/menu_view', $dados, TRUE);
            $conteudo.=$this->load->view('adm/produto', '', TRUE);
            set_tema('conteudo', $conteudo);
        } else {
            $conteudo = $this->load->view('adm/menu_view', $dados, TRUE);
            $conteudo.= "<h1>Produto não localizado, por favor verifique se ainda está cadastrado na loja e tente novamente</h1>";
        }
        set_tema('conteudo', $conteudo);
        load_template();
    }
    public function frete_gratis(){
         //Categorias para pesquisa
        $this->load->model('categorias_model', 'categoria');
        $categorias = $this->categoria->get_all();
        $dados['options'] = array();
        $dados['options'][0] = '';
        foreach ($categorias as $cat) {
            $dados['options'][$cat->id] = $cat->nome;
        }
        $dados['select'] = array(0);

        $conteudo = $this->load->view('adm/menu_view', $dados, true);
        //Pegar produtos para montar grid de listagem
        $this->load->model('produtos_model', 'produto');
        $this->db->select('categorias_tb.alias as categoria_nome, produtos_tb.id,produtos_tb.nome, produtos_tb.id,produtos_tb.frete_gratis');
        $this->db->join('categorias_tb', 'categorias_tb.id=produtos_tb.categorias_tb_id');
        $this->db->group_by('produtos_tb.id');
        $this->db->where('frete_gratis','1');
        //CHECAR SE O FRETE É GRATUITO EM TODOS OS PRODUTOS DO SITE
        $query=$this->db->query('select count(id) as produtos from produtos_tb where frete_gratis=1');
        $row=$query->row();

        $query=$this->db->query('select count(id) as produtos from produtos_tb');
        $row2=$query->row();
        $dados['frete_gratis']=($row->produtos==$row2->produtos)?true:false;

        $dados['tela'] = 'lista';
        $dados['produtos'] = $this->produto->get_all(NULL, 20, 0);

        $conteudo.=$this->load->view('adm/produto', $dados, true);
        set_tema('conteudo', $conteudo);
        load_template();
    }
    public function cadastro() {
        set_tema('headerinc',load_css(array('adm/bootstrap.min','adm/style','adm/blueimp-gallery.min','adm/jquery.fileupload',
            'adm/jquery.fileupload-ui')),FALSE);
//,'adm/tmpl.min'
        set_tema('footerinc',load_js(array('adm/jquery.ui.widget','adm/load-image.min','adm/canvas-to-blob.min',
           'adm/bootstrap.min','adm/jquery.blueimp-gallery.min',
            'adm/jquery.iframe-transport','adm/jquery.fileupload',
            'adm/jquery.fileupload-process','adm/jquery.fileupload-image',
            'adm/jquery.fileupload-audio','adm/jquery.fileupload-video',
            'adm/jquery.fileupload-validate','adm/jquery.fileupload-ui','mask','adm/function')),FALSE);
        //MONTAR O MENU SUPERIOR
        $conteudo = '';
        //Seleção de tela
        $dados['tela'] = 'cadastro';
        //Action do formulário
        $dados['action'] = "adm/produtos/cadastro";
        $this->load->model('categorias_model', 'categoria');
        $categorias = $this->categoria->get_all();
        //montar select de categorias
        $dados['options'] = array();
        foreach ($categorias as $categoria_item) {
            $dados['options'][$categoria_item->id] = $categoria_item->alias;
        }

        /* montar os componentes de tamanhos */
        $dados['select'] = array();
        $dados['destaques'] = '';
        $tamanhos = $this->db->get('tamanhos');


        $result = $tamanhos->result_array();

        $dados['tamanho'] = $result;

        $dados['selected_tam'] = array();
        /* fim do carregamento dos tamanhos possíveis */

        //carregar menu
        $conteudo.=$this->load->view('adm/menu_view', $dados, TRUE);
        //regras de validação de formulário
        $this->form_validation->set_rules('nome', 'Nome', "trim|required|min_length[4]|strtolower|xss_clean");
        $this->form_validation->set_rules('valor', 'Preço', "trim|required||xss_clean");
        $this->form_validation->set_rules('peso', 'Peso', "trim|xss_clean");
        $this->form_validation->set_rules('detalhes', 'Detalhes', "trim|max_length[600]|strtolower|xss_clean");
        $this->form_validation->set_rules('destaque', 'Destaque', "xss_clean");
        $this->form_validation->set_rules('categoria', 'Categoria', "trim|required|numeric|xss_clean");

        

        //Disparar cadastro  
        if ($this->form_validation->run() == TRUE):
            $nome = $this->input->post('nome', TRUE);
        $valor = $this->input->post('valor', TRUE);
        $peso = $this->input->post('peso', TRUE);
        $detalhes = $this->input->post('detalhes', TRUE);
        $destaques = $this->input->post('destaque', TRUE) == 'destaque';

        $categoria = $this->input->post('categoria', true);

        $tamanhos = array();
        if ($this->input->post('1', TRUE)) {
            array_push($tamanhos, '1');
        }
        if ($this->input->post('2', TRUE)) {
            array_push($tamanhos, '2');
        }
        if ($this->input->post('3', TRUE)) {
            array_push($tamanhos, '3');
        }
        if ($this->input->post('4', TRUE)) {
            array_push($tamanhos, '4');
        }
        if ($this->input->post('5', TRUE)) {
            array_push($tamanhos, '5');
        }
        if ($this->input->post('6', TRUE)) {
            array_push($tamanhos, '6');
        }

        if ($this->input->post('7', TRUE)) {
            array_push($tamanhos, '7');
        }
        if ($this->input->post('8', TRUE)) {
            array_push($tamanhos, '8');
        }
        if ($this->input->post('9', TRUE)) {
            array_push($tamanhos, '9');
        }
        if ($this->input->post('10', TRUE)) {
            array_push($tamanhos, '10');
        }
        if ($this->input->post('11', TRUE)) {
            array_push($tamanhos, '11');
        }
        if ($this->input->post('12', TRUE)) {
            array_push($tamanhos, '12');
        }
        if ($this->input->post('13', TRUE)) {
            array_push($tamanhos, '13');
        }
        if ($this->input->post('14', TRUE)) {
            array_push($tamanhos, '14');
        }
        if ($this->input->post('15', TRUE)) {
            array_push($tamanhos, '15');
        }
        if ($this->input->post('16', TRUE)) {
            array_push($tamanhos, '16');
        }
        if ($this->input->post('17', TRUE)) {
            array_push($tamanhos, '17');
        }
        if ($this->input->post('18', TRUE)) {
            array_push($tamanhos, '18');
        }

        if ($this->input->post('19', TRUE)) {
            array_push($tamanhos, '19');
        }
        if ($this->input->post('20', TRUE)) {
            array_push($tamanhos, '20');
        }
        if ($this->input->post('21', TRUE)) {
            array_push($tamanhos, '21');
        }
        if ($this->input->post('22', TRUE)) {
            array_push($tamanhos, '22');
        }
        if ($this->input->post('23', TRUE)) {
            array_push($tamanhos, '23');
        }
        if ($this->input->post('24', TRUE)) {
            array_push($tamanhos, '24');
        }

        if ($this->input->post('25', TRUE)) {
            array_push($tamanhos, '25');
        }
        if ($this->input->post('26', TRUE)) {
            array_push($tamanhos, '26');
        }
        if ($this->input->post('27', TRUE)) {
            array_push($tamanhos, '27');
        }
        if ($this->input->post('28', TRUE)) {
            array_push($tamanhos, '28');
        }
        if ($this->input->post('29', TRUE)) {
            array_push($tamanhos, '29');
        }
        if ($this->input->post('30', TRUE)) {
            array_push($tamanhos, '30');
        }
        if ($this->input->post('31', TRUE)) {
            array_push($tamanhos, '31');
        }
        if ($this->input->post('32', TRUE)) {
            array_push($tamanhos, '32');
        }
        $this->load->model('produtos_model', 'produto');

        $id_produto = $this->produto->do_cadastro($nome, $valor, $peso, $detalhes, $destaques, $categoria);
        if (is_numeric($id_produto)) {
            $this->add_Imagem($id_produto,$_FILES,"adm/produtos/atualiza/{$id_produto}");

              /*  $cat = $this->categoria->get_byId($categoria);
                $pasta = get_pathServidor() . "galeria/roupas/";
                $img = "{$pasta}{$cat[0]->alias}{$id_produto}";
                $conteudo.="<div class='col-md-12  form-group has-success'><input type='text' class='form-control' value='O cadastro de produto foi bem suscedido!'/></div>";

                $conteudo.=upload($pasta, $img, $_FILES);

                $this->load->model('galeria_model', 'galeria');
                $ext = '.' . substr($_FILES['arquivo']['type'], strpos($_FILES['arquivo']['type'], '/') + 1);
                $img = "{$cat[0]->alias}{$id_produto}{$ext}";
                $this->galeria->do_cadastro($img, $id_produto, 1);
                foreach ($tamanhos as $tamanho) {
                    $data['produtos_tb_id'] = $id_produto;
                    $data['tamanhos_id'] = $tamanho;
                    $str = $this->db->insert_string('produto_tamanho', $data);
                    $this->db->query($str);
                }*/
        } else {
            $conteudo.="<div class='col-md-12  form-group has-error'><input type='text' class='form-control' value='O cadastro de produtos não foi bem suscedido!'/></div>";
        }
        endif;
        $dados['produto']=null;
        $dados['id']=null;
        $dados['galeria']=null;
        
        $conteudo.=$this->load->view('adm/produto', $dados, true);
     //   $conteudo.=$this->load->view('galeria_view', $dados, true);
        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function destaques() {
        $this->load->model('categorias_model', 'categoria');
        $categorias = $this->categoria->get_all();
        $dados['options'] = array();
        foreach ($categorias as $cat) {
            $dados['options'][$cat->id] = $cat->nome;
        }
        $dados['select'] = array();

        $conteudo = $this->load->view('adm/menu_view', $dados, true);
        //Pegar produtos para montar grid de listagem
        $this->load->model('produtos_model', 'produto');
        $this->db->select('categorias_tb.alias as categoria_nome, produtos_tb.id,produtos_tb.nome,produtos_tb.frete_gratis');
        $this->db->join('categorias_tb', 'categorias_tb.id=produtos_tb.categorias_tb_id');
        $this->db->group_by('produtos_tb.id');
        $this->db->where(array('destaques' => 1));
        $dados['tela'] = 'lista';
        $dados['produtos'] = $this->produto->get_all(NULL, 20, 0);
        $frete=$this->produto->get_freteGratisTodoSite();
        $dados['frete_gratis']=$frete[0]['itens']>0?0:1;
        $conteudo.=$this->load->view('adm/produto', $dados, true);
        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function slider() {
        set_tema('headerinc',load_css(array('adm/bootstrap.min','adm/style','adm/blueimp-gallery.min','adm/jquery.fileupload',
            'adm/jquery.fileupload-ui')),FALSE);
//,'adm/tmpl.min'
        set_tema('footerinc',load_js(array('adm/jquery-1.10.2','adm/jquery.ui.widget','adm/load-image.min','adm/canvas-to-blob.min',
            'adm/jquery-ui-1.10.4.custom','adm/bootstrap.min','adm/jquery.blueimp-gallery.min',
            'adm/jquery.iframe-transport','adm/jquery.fileupload',
            'adm/jquery.fileupload-process','adm/jquery.fileupload-image',
            'adm/jquery.fileupload-audio','adm/jquery.fileupload-video',
            'adm/jquery.fileupload-validate','adm/jquery.fileupload-ui','mask','adm/function')),FALSE);

        //Montar tela de menu
        $this->load->model('categorias_model', 'categoria');
        $categorias = $this->categoria->get_all();
        $dados['options'] = array();
        foreach ($categorias as $cat) {
            $dados['options'][$cat->id] = $cat->nome;
        }
        $dados['select'] = array();
        
        $conteudo = $this->load->view('adm/menu_view', $dados, true);
        //Obter as imagens de destaque do site
        $dados['destaques'] = scandir(get_pathServidor() . "galeria/destaques");
        //Permitir excluir imagem solicitada

        unset($dados['destaques'][0]);
        unset($dados['destaques'][1]);
        $this->form_validation->set_rules('nome', 'arquivo', "trim|xss_clean");
    // echo var_dump($_POST).var_dump($_FILES);
        if ($this->form_validation->run() == true):
            //checar se pode enviar nova imagem
        $fotos = count($dados['destaques'])+count($_FILES['arquivo']['name']);

        if ($fotos <=3){
          //  $fotos_name = rand(0, 100);

                //enviar nova imagem
            $pasta = get_pathServidor() . "galeria/destaques/";
            
            $cont=0;
            $total=count($_FILES['arquivo']['name']);
            while($cont<$total){
                $fotos_name = rand(0, 100);
                $img = $pasta . "destaque{$fotos_name}";
                $file=array('name'=>$_FILES['arquivo']['name'][$cont],
                            'type'=>$_FILES['arquivo']['type'][$cont],
                            'tmp_name'=>$_FILES['arquivo']['tmp_name'][$cont],
                            'error'=>$_FILES['arquivo']['error'][$cont],
                            'size'=>$_FILES['arquivo']['size'][$cont]
                            );
            //    ECHO var_dump($file);
                $conteudo.=upload($pasta, $img, $file);

               /* $ext = '.' . substr($file['type'], strpos($file['type'], '/') + 1);
                $subiu = move_uploaded_file($file['tmp_name'], $img . $ext);
                echo var_dump($file);
                echo $cont;*/
                $cont++;
            }
            $_FILES = array();
            redirect('adm/produtos/slider');
        } else {
            $conteudo.="<div class='col-md-12 form-group has-error'><input type='text' class='form-control' value='Não é permitido ter mais que três imagens como destaque para a home do site'/></div>";
        }
        $conteudo.='<br/>';
        
        endif;

        $conteudo.=$this->load->view('adm/destaques', $dados, true);
        //Permitir adcionar nova imagem, se ainda não houverem 3 selecionadas
        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function apagarSlider() {
        $imagem = $this->input->post('id');
        // echo get_pathServidor() . "galeria/destaques/{$imagem}";
        unlink(get_pathServidor() . "galeria/destaques/{$imagem}");
        //redirect('adm/produtos/apagarSlider');
    }

    public function pesquisar() {
        $this->load->model('categorias_model', 'categoria');
        $categorias = $this->categoria->get_all();
        $dados['options'] = array();
        $dados['options'][0] = '';
        $conteudo = $this->load->view('adm/menu_view', $dados, TRUE);
        foreach ($categorias as $cat) {
            $dados['options'][$cat->id] = $cat->nome;
        }
        $dados['select'] = array(0);

        $nome = $this->input->post('nome', true);
        $categoria = $this->input->post('categoria', true);
        if ($nome != '') {
            $this->db->like('produtos_tb.nome', $nome);
        }if ($categoria != 0) {
            $this->db->where('categorias_tb.id', $categoria);
        }
        $this->load->model('produtos_model', 'produto');
        $this->db->select('categorias_tb.alias as categoria_nome, produtos_tb.id,produtos_tb.nome,produtos_tb.frete_gratis');
        $this->db->join('categorias_tb', 'categorias_tb.id=produtos_tb.categorias_tb_id');
        $this->db->group_by('produtos_tb.id');
        $dados['tela'] = 'lista';
        $dados['produtos'] = $this->produto->get_all();
        //CHECAR SE O FRETE É GRATUITO EM TODOS OS PRODUTOS DO SITE
        $query=$this->db->query('select count(id) as produtos from produtos_tb where frete_gratis=1');
        $row=$query->row();

        $query=$this->db->query('select count(id) as produtos from produtos_tb');
        $row2=$query->row();
        $dados['frete_gratis']=($row->produtos==$row2->produtos)?true:false;
        $conteudo = $this->load->view('adm/produto', $dados, true);
        echo $conteudo;
        //set_tema('conteudo', $conteudo);
        //load_template();
    }

    public function galeria($id = NULL) {
        $dados = array();
        set_tema('headerinc',load_css(array('adm/bootstrap.min','adm/style','adm/blueimp-gallery.min','adm/jquery.fileupload',
            'adm/jquery.fileupload-ui')),FALSE);
//,'adm/tmpl.min'
        set_tema('footerinc',load_js(array('adm/jquery-1.10.2','adm/jquery.ui.widget','adm/load-image.min','adm/canvas-to-blob.min',
            'adm/jquery-ui-1.10.4.custom','adm/bootstrap.min','adm/jquery.blueimp-gallery.min',
            'adm/jquery.iframe-transport','adm/jquery.fileupload',
            'adm/jquery.fileupload-process','adm/jquery.fileupload-image',
            'adm/jquery.fileupload-audio','adm/jquery.fileupload-video',
            'adm/jquery.fileupload-validate','adm/jquery.fileupload-ui','mask','adm/function')),FALSE);

        $conteudo = $this->load->view('adm/menu_view', $dados, TRUE);

        if(!is_null($id)){
            $this->load->model('produtos_model', 'produto');

            $produto = $this->produto->get_productsById($id);

            $dados['id'] = $id;

            if (($produto)!= false){
                $this->db->where('produtos_tb_id', $id);
                $this->load->model('galeria_model', 'galeria');
                $dados['galeria'] = $this->galeria->get_all();
                $dados['produto']=$id;
                $conteudo.=$this->load->view('galeria_view', $dados, true);
            }else{
                $conteudo.="<div class='col-md-12 form-group has-error'>
                <input type='text' class='form-control' value='O produto não existe'/>
                </div>";
            }
        }else{
            $conteudo.="<div class='col-md-12 form-group has-error'>
            <input type='text' class='form-control' value='O produto não existe'/>
            </div>";
        }
        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function delete_imagem() {
        $imagem = $this->input->post('id', true);
        if (!is_null($imagem)) {
            unlink(get_pathServidor() . "galeria/roupas/{$imagem}");
            $this->load->model('galeria_model', 'galeria');
            echo $this->galeria->do_delete($imagem);
        }
    }

    public function add_Imagem($id = null,$files=null,$redirect_to=null) {
        echo var_dump($_FILES);
        if(!is_null($files)){
            $_FILES= $files;
        }
        if (!is_null($id)) {
            $array = $_FILES['files'];            
            $principal=0;
            $this->load->model('produtos_model', 'produto');
            $produtos = $this->produto->get_productsById($id);
            $this->load->model('categorias_model', 'categoria');
            $this->load->model('galeria_model', 'galeria');
            $cat = $this->categoria->get_byId($produtos[0]->categorias_tb_id);
            $categoria_produto = $cat[0]->alias;
            $pasta = get_pathServidor() . "galeria/roupas/";
            $total=count($array['name']);
            $contador=0;
            $conteudo='';
            $redirect=false;
            while($contador<$total){
                if($array['error'][$contador]!=1){
                    $this->db->select("count(id)+1 as max");
                    $this->db->where('produtos_tb_id', $id);
                    $imagem = $this->galeria->get_all();
                    //fazerupload de imagem
                    $img = "{$cat[0]->alias}{$id}_{$imagem[0]->max}";
                    $dado=array('name'=>$array['name'][$contador],
                        'type'=>$array['type'][$contador],
                        'tmp_name'=>$array['tmp_name'][$contador],
                        'error'=>$array['error'][$contador],
                        'size'=>$array['size'][$contador]);
                    $ext = '.' . substr($dado['type'], strpos($dado['type'], '/') + 1);
                    $this->load->model('galeria_model', 'galeria');
                    $conteudo = upload($pasta, $pasta . $img, $dado);
                    $this->galeria->do_cadastro($img . $ext, $id, $principal);
                    $conteudo.= upload($pasta, $pasta . $img, $array);
                    $contador++;
                }else{
                    $contador++;
                }
                echo $conteudo;
                
               
            }
             $redirect=true;
        } else {
            //IMPOSSÍVEL ADD IMAGEM NÃO FOI ID PRODUTO
            echo "<div class='col-md-12 form-group has-error'><input type='text' class='form-control' value='O produto não existe'/></div>";
        }
        if($redirect){
            if(!is_null($redirect_to)){
                redirect($redirect_to);
            }
            redirect("adm/produtos/galeria/{$id}");
        }
    }

    public function troca_principal() {
        $id = $this->input->post('imagem', true);
        echo $id;
        if (!is_null($id)) {
            $imagem = explode("-", $id);
            $str = "update galeria set principal='0' where principal='1' and produtos_tb_id='{$imagem[1]}';";
            $this->db->query($str);
            $str = "update galeria set principal='1' where arquivo like '{$imagem[0]}' and produtos_tb_id='{$imagem[1]}';";
            $this->db->query($str);
        }
    }

    public function troca_alt() {
        $id = $this->input->post('imagem', true);
        $conteudo = $this->input->post('conteudo', true);
        echo $id;
        if (!is_null($id)) {
            $str = "update galeria set alt='{$conteudo}' where arquivo like '{$id}';";
            $this->db->query($str);
        }
    }

    public function troca_title() {
        $id = $this->input->post('imagem', true);
        $conteudo = $this->input->post('conteudo', true);
        echo $id;
        if (!is_null($id)) {
            $str = "update galeria set title='{$conteudo}' where arquivo like '{$id}';";
            $this->db->query($str);
        }
    }

    public function apaga_produto() {
        $id = $this->input->post('id');
        if (!is_null($id)) {
            $this->load->model('itens');
            $this->db->where(array('produtos_tb_id' => $id));
            $itens = $this->itens->get_all();
            if (count($itens) > 0) {
                echo 'O produto já foi comprado e não pode ser removido do banco de dados';
            } else {
                $this->db->query("delete from produto_tamanho where produtos_tb_id='{$id}';");
                $this->db->query("delete from galeria where produtos_tb_id='{$id}';");
                $resposta=$this->db->query("delete from produtos_tb where id='{$id}';");
                if($resposta==true || $resposta==1){
                    echo 'Produto removido da base de dados';
                }

            }
        } else {
            echo 'O produto não foi identificado';
        }
    }
    public function frete_gratis_todos(){
        $this->db->query('update produtos_tb set frete_gratis="1";');
    }
    public function frete_pago_todos(){
        $this->db->query('update produtos_tb set frete_gratis="0";');
    }
    public function remove_frete_gratis(){
        $produto= $this->input->post('id', true);
        //echo $produto;
        echo $this->db->query('update produtos_tb set frete_gratis="0" where id='.$produto);
    }
    public function adiciona_frete_gratis(){
        $produto= $this->input->post('id', true);
        echo $this->db->query('update produtos_tb set frete_gratis="1" where id='.$produto);
    }
    public function promocao($id=null,$acao=null){
        $dados=array();
        
        $conteudo = $this->load->view('adm/menu_view', $dados, true);
        if(is_null($id)){
            $dados['cadastro']=true;
            $dados['promocao'][0]->id='';
            $dados['promocao'][0]->nome='';
            $dados['promocao'][0]->percentual=false;
            $dados['promocao'][0]->site_todo=false;
            $dados['promocao'][0]->data_inicial='';
            $dados['promocao'][0]->data_final='';
            $dados['promocao'][0]->valor='';
            //TROCAR ESTES DADOS
            $this->form_validation->set_rules('nome', 'Nome', "trim|required|min_length[4]|strtolower|xss_clean");
            $this->form_validation->set_rules('valor', 'Valor', "number|trim|required||xss_clean");
            $this->form_validation->set_rules('data_inicial2', 'Data Inicial', "trim|xss_clean");
            $this->form_validation->set_rules('data_final2', 'Data Final', "trim|xss_clean");
            $this->form_validation->set_rules('percentual', 'Valor Percentual?', "xss_clean");  
            $this->form_validation->set_rules('site_todo', 'Promoção válida para todo o site?', "xss_clean");
            //Disparar cadastro  
            if ($this->form_validation->run() == TRUE):
                $nome = $this->input->post('nome', TRUE); 
            $valor = $this->input->post('valor', TRUE);
            $data_final = $this->input->post('data_final2', TRUE);
            echo $data_final;
            $data_inicial = $this->input->post('data_inicial2', TRUE); 
            $percentual = $this->input->post('percentual',true);
            $todo_site = $this->input->post('site_todo',true);
            $this->load->model('promocoes_model', 'promocao');
            $id=$this->promocao->add($nome,$valor,$data_inicial,$data_final,$percentual,$todo_site);
            redirect(base_url('adm/produtos/promocao/'.$id));
            endif;

        }else{ 

            $this->load->model('promocoes_model','promocao');
            $dados['promocao']=$this->promocao->get_by_id($id);
            $this->form_validation->set_rules('nome', 'Nome', "trim|required|min_length[4]|strtolower|xss_clean");
            $this->form_validation->set_rules('valor', 'Valor', "number|trim|required||xss_clean");
            $this->form_validation->set_rules('data_inicial2', 'Data Inicial', "trim|xss_clean");
            $this->form_validation->set_rules('data_final2', 'Data Final', "trim|xss_clean");
            $this->form_validation->set_rules('percentual', 'Valor Percentual?', "xss_clean");  
            $this->form_validation->set_rules('site_todo', 'Promoção válida para todo o site?', "xss_clean");
            if(isset($acao)){
                //Disparar cadastro  
                if ($this->form_validation->run() == TRUE):
                //    echo var_dump($this->input->post());
                    $nome = $this->input->post('nome', TRUE); 
                $valor = $this->input->post('valor', TRUE);
                $data_final = $this->input->post('data_final2', TRUE);
                $data_inicial = $this->input->post('data_inicial2', TRUE); 
                $percentual = $this->input->post('percentual',true);
                $todo_site = $this->input->post('site_todo',true);
                $this->load->model('promocoes_model', 'promocao');
                $this->promocao->atualiza($id,$nome,$valor,$data_inicial,$data_final,$percentual,$todo_site);
                redirect(base_url('adm/produtos/promocao/'.$id));
                endif;
            }
            
        }
        $conteudo.=$this->load->view('adm/promocao',$dados,true);
        set_tema('conteudo', $conteudo);
        load_template();
    }
    public function lista_promocao(){
        $dados=array();
        $this->load->model('promocoes_model','promocao');
        $dados['promocoes']=$this->promocao->get_all();
        $dados['lista']=true;
        $conteudo = $this->load->view('adm/menu_view', $dados, true);
        $conteudo.=$this->load->view('adm/promocao',$dados,true);
        set_tema('conteudo', $conteudo);
        load_template();
    }
    public function listagem_promocao(){
        $retorno='';
        if(!empty($_POST['nome']) || !empty($_POST['data_inicial']) || !empty($_POST['data_final']) ){
            $this->load->model('promocoes_model','promocao');
            $promocoes=$this->promocao->get_promocao($_POST);
            if(!empty($promocoes) && is_array($promocoes)){
                foreach ($promocoes as $promocao) { 
                    $retorno= "<div class='col-sm-3'><a href='".base_url('adm/produtos/promocao/'.$promocao->id)."'>{$promocao->id}</a></div>
                    <div class='col-sm-3'><a href='".base_url('adm/produtos/promocao/'.$promocao->id)."'>".mb_convert_encoding($promocao->nome,'utf8')."</a></div>
                    <div class='col-sm-3'><a href='".base_url('adm/produtos/promocao/'.$promocao->id)."'>{$promocao->data_inicial}</a></div>
                    <div class='col-sm-3'><a href='".base_url('adm/produtos/promocao/'.$promocao->id)."'>{$promocao->data_final}</a></div>";
                }
            } 
        }else{
            $retorno='Desculpe, mas não localizamos a promoção';
        }
        echo $retorno;
    }
    public function finaliza_promocao(){
        if(isset($_POST)){
            if($_POST['id']){
                $this->load->model('promocoes_model','promocao');
                $promocoes=$this->promocao->delete_promocao($_POST['id']);
            }
        }else{
            echo "A promoção não foi encerrada";
        }
    }
    public function add_produto_promocao($id_promocao){

        $dados['promocao_id']=$id_promocao;
        $this->load->model('produtos_model','produto');
        //Produtos da promoção para listagem exclusão caso deseje
        $dados['todos_produtos']=array();
        $todos_produtos=$this->produto->produtos_foraPromocao($id_promocao);
        $this->load->model('promocoes_model','promocao');

        //Se a promoção tiver vigência sobre todo site este não precisa obter itens da promoção
        $site_todo=$this->promocao->promocao_site_todo($id_promocao);
        $dados['site_todo']=$this->promocao->promocao_site_todo($id_promocao);
        $dados['promocao_vazia']=false;
        if(!$site_todo){
            $dados['produtos_destaPromocao']=$this->produto->produtos_byPromocao($id_promocao);
        }else{
            //determinar array vazio para caso de promoção do site inteiro
           $dados['produtos_destaPromocao']=$todos_produtos;
           $dados['promocao_vazia']=true;
       }
       $dados['selecionados']=array();
       if(!$dados['promocao_vazia']){
        $produtos_promo=$dados['produtos_destaPromocao'];
        foreach($produtos_promo as $prod){
            array_push($dados['selecionados'],$prod->produto_id);
        }
    }

    $dados['todos_produtos']= array('0' => '-- TODOS --');
    $categoria=$todos_produtos[0]->categoria_nome;
    $categorias=array();
    foreach($todos_produtos as $produtos){
        if($produtos->categoria_nome!=$categoria){
            if(!empty($categorias)){
                $dados['todos_produtos'][$categoria]=$categorias;
                $categorias=array();
                $categoria=mb_convert_encoding($produtos->categoria_nome,'utf-8');
            }                    
        }
        $categorias[$produtos->produto_id]=$produtos->produto_id.'-'.mb_convert_encoding($produtos->produto_nome,'utf-8');
    }
    $this->load->model('promocoes_model','promocao');
    $conteudo=$this->load->view('adm/menu_view', $dados, true);
    $conteudo.=$this->load->view('adm/promocao_produto',$dados,true);
    set_tema('conteudo', $conteudo);
    load_template();
}
public function insere_na_promocao(){
    $produtos=explode(',',$this->input->post('produtos',true));
    $promocao=$this->input->post('promocao',true);
    $this->load->model('promocao_produto_model','relacionamento');
        //remover todos os itens da promoção
    $this->relacionamento->deleta_tudo($promocao);
        //array  dos produtos que não serão inseridos por estarem em promoções vigentes no mesmo período
    $nao_inseridos=array();
    $this->load->model('promocoes_model','promocao');
        //obter dados da promoção
    $esta_promocao=$this->promocao->get_by_id($promocao);
        //obter promoções vigentes neste período
    $promocoes_concorrentes=$this->promocao->promocao_por_periodo($esta_promocao[0]->data_inicial,$esta_promocao[0]->data_final,$promocao);
//echo var_dump($promocoes_concorrentes);
        //caso a promoção deva ser cadastrada para todo o site
    $produtos_bloqueados=array();
    if(!empty($promocoes_concorrentes)){
        $produtos_bloqueados=$this->relacionamento->get_produtos_promocoes_array($promocoes_concorrentes);
    }
    if(!in_array('0', $produtos)){
        foreach($produtos as $produto){
            if(!in_array($produto, $produtos_bloqueados)){
                $this->relacionamento->add($promocao,$produto);
            }else{
                array_push($nao_inseridos,$produto);
            }
        } 
        $this->promocao->remover_site_inteiro($promocao);
    }
    else{
        $this->promocao->colocar_site_inteiro($promocao);
    }
    if(!empty($nao_inseridos)){
        $this->load->model('produtos_model','produto');
        $this->db->select('id,nome');
        $this->db->where_in('id',($nao_inseridos));
        $produtos=$this->produto->get_all();
        if(!empty($produtos)){
            $produto_nao='O(s) produtos(s) não foram adicionados a esta promoção por já estarem cadastrados em outra promoção vigente no mesmo período: ';
            foreach($produtos as $prod){
                $produto_nao.=$prod->id.'-'.$prod->nome;
            }
            echo $produto_nao;
        }
    }
}
public function remove_produto_promocao(){
    $produto_id=$this->input->post('produto',true);
    $promocao_id=$this->input->post('promocao',true);
    $this->load->model('promocao_produto_model','relacionamento');
    $this->relacionamento->remove($promocao_id,$produto_id);

}
}

/*
 * End of file produtos.php
 * Location: application/controllers/produtos.php
 */
?>

