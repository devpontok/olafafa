<?php
if ( !defined('BASEPATH')) exit('Acesso ao script não é permitido');
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Painel extends CI_Controller{
    public function __construct(){
           parent::__construct();
           init_painel();
    }
    public function index()
    {
        if(esta_logado(TRUE)){
                set_tema('titulo','painel');
                $conteudo='';
                $conteudo=$this->load->view('adm/menu_view','',TRUE);
                $this->load->model('compras_model', 'compras');
                $this->db->where(array('status'=>'p'));
                $compras=$this->compras->get_all();
             //   echo var_dump($compras);
                $cont=0;
                $dados['total']=0;
                $dados['compras']=array();
                foreach($compras as $compra){
                    $dados['compras'][$cont]['compra']=$compra->compra;
                    $dados['compras'][$cont]['email']=$compra->email;
                    $dados['compras'][$cont]['nome']=$compra->nome;
                    $dados['compras'][$cont]['data_compra']=$compra->data_compra;
                    $dados['compras'][$cont]['status']=$compra->status;
                    $dados['compras'][$cont]['valor']=$compra->valor;
                    $dados['total']+=$compra->valor;
                    $dados['compras'][$cont]['forma_pagamento']=$compra->forma_pagamento;
                    $dados['compras'][$cont]['taxas']=$compra->taxas;
                    $dados['compras'][$cont]['itens']=array();
                    $contador_itens=0;
                    $itens=$this->compras->get_itensCompra($compra->compra);
                    foreach($itens as $item){
                        $dados['compras'][$cont]['itens'][$contador_itens]="{$item->id}-{$item->nome}: {$item->quantidade}";
                        $contador_itens++;
                    }
                    $cont++;
                }
                //Sessão para evitar listar novamente as compras
                if(!isset($_SESSION)){
                    session_start();
                }
                $_SESSION['compras']= $dados['compras'];
                $_SESSION['periodo']='';
         //       set_tema('footerinc', load_js(array('html5','prettify','jquery-1.9','bootstrap-datepicker','function'), 'js/adm', FALSE));
                set_tema('headerinc', load_css(array('bootstrap', 'flat-ui', 'demo','ui-lightness/jquery-ui-1.10.4.custom','ui-lightness/jquery-ui-1.10.4.custom.min','estilo'), 'css/adm'), FALSE);
                set_tema('footerinc', load_js(array('jquery-1.10.2.min', 'jquery-ui-1.10.4.custom',
                                                'jquery.ui.touch-punch.min', 'bootstrap.min',
                                                'bootstrap-select','bootstrap-switch',
                                                'flatui-checkbox', 'flatui-radio',
                                                'jquery.tagsinput', 'jquery.placeholder',
                                                'jquery.stacktable', 'application',
                                                'mask',
                                                'bootstrap-modal','function'), 'js/adm', FALSE));
                $conteudo.= $this->load->view('adm/compras_view', $dados, true);
                set_tema('conteudo',$conteudo);
                load_template();
        }else{
              $this->inicio();
        }
        //$this->load->view("painel_view");
    }
    public function inicio(){
        //carregar o módulo de usuários e mostrar tela de login
      redirect('usuarios/login');
    }
}
/*
 * End of file sistema.php 
 * Location: Application/controllers/adm/painel.php
  */

