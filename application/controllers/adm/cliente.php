<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class cliente extends CI_Controller {

    private $conteudo;

    public function __construct() {
        parent::__construct();
        init_painel();
        $this->load->model('usuarios_model', 'usuario');
        $this->conteudo = $this->load->view('adm/menu_view', '', TRUE);
    }

    public function index() {
        $this->load->view("");
    }

    public function lista() {
        $dados = array();
        $limite = 20;
        $offset = 0;
        $dados['users'] = $this->usuario->get_all(NULL, $limite, $offset);

        $this->conteudo.=$this->load->view('adm/clientes_view', $dados, TRUE);
        set_tema('conteudo', $this->conteudo);
        load_template();
    }

    public function adminstrador() {
        $dados['tela'] = 'admin';
        $this->form_validation->set_rules('senha', 'Senha', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('email', 'Email', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('login', 'Login', "trim|required|min_length[4]|strtolower");
        if ($this->form_validation->run()):
            $senha = md5($this->input->post('senha', TRUE));
            $email = $this->input->post('email', TRUE);
            $login = $this->input->post('login', TRUE);
            $this->load->model('usuarios_model', 'usuarios');

            $query = $this->usuarios->do_cadastro_adm($senha, $email, $login);
            if ($query != FALSE) {
                redirect('adm/cliente/adminstrador');
            } else {
                echo 'cadastro falhou';
            }
        endif;
        $this->conteudo.=$this->load->view('cadastro', $dados, true);
        set_tema('conteudo', $this->conteudo);
        load_template();
    }

    public function cadastro($id = NULL) {
        if (!is_null($id)) {
            $user = $this->usuarios->get_byId($id);
            $user = $user[0];
            if (count($user) < 0) {
                $this->conteudo.='<div class="form-control has-error"><input type="text" value="Usuário não identificável"/></div>';
            } else {
                $dados['nome'] = $user->nome;
                $dados['tela'] = "disabled";
                $dados['login'] = $user->login;
                $dados['email'] = $user->email;
                $dados['data_nascimento'] = $user->data_nascimento;
                $dados['cpf'] = $user->cpf;
                $dados['rua'] = $user->rua;
                $dados['bairro'] = $user->bairro;
                $dados['cidade'] = $user->cidade;
                $dados['estado'] = $user->estado;
                $dados['cep'] = $user->cep;
                $dados['complemento'] = $user->complemento;
                $dados['optin'] = ($user->optin == 1) ? 'checked=checked' : '';
                $this->conteudo.=$this->load->view('cadastro', $dados, true);
            }
        } else {
            $this->conteudo.= '<div class="form-control has-error"><input type="text" value="Usuário não identificável"/></div>';
        }
        set_tema('conteudo', $this->conteudo);
        load_template();
    }

    public function cadastro_news() {
        $email = $this->input->post('email', true);
        $this->load->model('news_model', 'news');
        echo $this->news->do_cadastro($email);
    }

    public function news() {
        $this->load->model('news_model', 'news');
        $dados['news']=$this->news->get_all();

        $this->conteudo.=$this->load->view('news_view',$dados,TRUE);
        set_tema('conteudo',$this->conteudo);
        load_template();
    }
    public function file_xls()
{
   $this->load->helper('php-excel');
   $query = $this->db->get('news');
            foreach ($query->result() as $row)
            {
                    $data_array[] = array( $row->email );
            }
   $xls = new Excel_XML;
   $xls->addArray ($data_array);
   $xls->generateXML ( "news" );
}

}

/*
 * End of file cliente.php
 * Location: application/controllers/cliente.php
 */
?>

