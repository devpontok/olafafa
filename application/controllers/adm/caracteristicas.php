<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class caracteristicas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_painel();
    }
    public function tipo($id=null){
    	$this->load->model('tipos_model','tipos');
    	$dados=array();
        $dados['tipos']=null;
        $dados['caracteristicas']=null;
    	if(is_null($id)){
		//cadastro de tipo  
			$dados['id']='';
			$dados['nome']='';
			$dados['acao']='insert';
    	}else{
    		//atualização de tipo
    		$dado=$this->tipos->get_all($id);
            $dados['id']=$dado[0]->id;
            $dados['nome']=$dado[0]->nome;
    		$dados['acao']='update';
            $this->load->model('caracteristicas_model','caracteristicas');
            $dados['caracteristicas']=$this->caracteristicas->get_all_by_tipo($id);
    	}
        $this->form_validation->set_rules('nome','Nome',"trim|required|min_length[3]|strtolower");
        if($this->form_validation->run()==true):
            $nome=$this->input->post('nome',true);
            if(is_null($id)){
                $this->tipos->do_cadastro(ucfirst(strtoupper(trim($nome))));
            }else{
                $this->tipos->do_update(ucfirst(strtoupper(trim($nome))),$id);

                
            }   
        endif;
        $dados['tipos']=$this->tipos->get_all();
        $conteudo = $this->load->view('adm/menu_view', $dados, TRUE);
        $conteudo.=$this->load->view('adm/tipos_view',$dados, TRUE);
        set_tema('conteudo', $conteudo);
        load_template();
    }
    public function nova_caracteristica(){
        $this->load->model('caracteristicas_model','caracteristica');
        $this->caracteristica->do_cadastro(strtolower($_POST['tipo']),$_POST['nome']);
        redirect("adm/caracteristicas/tipo/".$_POST['tipo']);
    }
    public function update_tipo(){
        $id=$_POST['id'];
        $valor=$_POST['valor'];
        $this->load->model('caracteristicas_model','caracteristica');
        echo $this->caracteristica->do_update(strtolower($valor),$id);
    }
}
?>