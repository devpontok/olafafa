<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class estoque extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_painel();
        //$this->load->model('estoque_model', 'categoria');
        $this->conteudo = $this->load->view('adm/menu_view', '', TRUE);
    }

    public function cadastro($produto = NULL) {
        //caracteristica tipo_id==1=>cor; tipo_id==2=>tamanho; tipo_id==3=>modelo
        If (!is_null($produto)) {
            $this->load->model('produtos_model', 'produtos');
            $dados['produtos'] = $this->produtos->get_productsById($produto);
            if ($dados['produtos'] != FALSE) {
                $dados['tela'] = 'lista';
                $this->db->select("produtos_tb.id, estoque_tb.tamanhos_id as tamanhos_id,
                    estoque_tb.id as estoque_id,cor_id as cor,
                    modelo_id as modelo, estoque_tb.quantidade as qtd");
              //  $this->db->join('produto_tamanho', 'produto_tamanho.produtos_tb_id=produtos_tb.id');
                //$this->db->join('tamanhos', 'produto_tamanho.tamanhos_id=tamanhos.id');
                $this->db->join('estoque_tb', 'estoque_tb.produtos_tb_id=produtos_tb.id', 'inner');
                $this->db->group_by('estoque_tb.id');
                 $this->db->order_by('estoque_tb.id','desc');
                $this->db->where(array('produtos_tb.id' => $produto));
                $dados['estoque'] = $this->db->get('produtos_tb')->result();
                if (empty($dados['estoque'])) {
                    $this->db->where(array('produtos_tb_id' => $produto));
                    $dados['quantidade'] = $this->db->get('estoque_tb')->result();
                }

                $this->db->where(array('tipo_id'=>2));
                $tamanhos=$this->db->get('caracteristicas_tb')->result_array();
                $dados['tamanhos']=array();
                $dados['tamanhos'][0]='Tamanhos';
                foreach($tamanhos as $tamanho){
                    $dados['tamanhos'][$tamanho['id']]=$tamanho['nome'];
                }

                $this->db->where(array('tipo_id'=>1));
                $cores=$this->db->get('caracteristicas_tb')->result_array();
                $dados['cores']=array();
                $dados['cores'][0]='Cores';
                foreach($cores as $cor){
                    $dados['cores'][$cor['id']]=$cor['nome'];
                }

                $this->db->where(array('tipo_id'=>3));
                $modelos=$this->db->get('caracteristicas_tb')->result_array();
                $dados['modelos']=array();
                $dados['modelos'][0]='Modelos';
                foreach($modelos as $modelo){
                    $dados['modelos'][$modelo['id']]=$modelo['nome'];
                }
            } else {
                $this->conteudo = "Este produto não existe";
            }
       //    echo var_dump($dados);
            $this->conteudo.=$this->load->view("adm/estoque_view", $dados, true);
        } else {
            $this->conteudo.='Determine o produto a ser cadastrado';
        }
        set_tema('conteudo', $this->conteudo);
        load_template();
    }
    public function novo_estoque($produto){
        $this->load->model('produtos_model', 'produtos');
        $dados['produtos'] = $this->produtos->get_productsById($produto);
        $this->db->where(array('tipo_id'=>2));
        $tamanhos=$this->db->get('caracteristicas_tb')->result_array();
        $dados['tamanhos']=array();
        $dados['tamanhos'][0]='Tamanhos';
        foreach($tamanhos as $tamanho){
            $dados['tamanhos'][$tamanho['id']]=$tamanho['nome'];
        }

        $this->db->where(array('tipo_id'=>1));
        $cores=$this->db->get('caracteristicas_tb')->result_array();
        $dados['cores']=array();
        $dados['cores'][0]='Cores';
        foreach($cores as $cor){
            $dados['cores'][$cor['id']]=$cor['nome'];
        }

        $this->db->where(array('tipo_id'=>3));
        $modelos=$this->db->get('caracteristicas_tb')->result_array();
        $dados['modelos']=array();
        $dados['modelos'][0]='Modelos';
        foreach($modelos as $modelo){
            $dados['modelos'][$modelo['id']]=$modelo['nome'];
        }
        $dados['tela']='novo_estoque';
        echo $this->load->view("adm/estoque_view", $dados, true);
    }
    public function add() {
        $valor = $this->input->post('valor', true);
        $id = $this->input->post('id', true);
        $produto = $this->input->post('produto', true);
        $tamanho = $this->input->post('tamanho', true);
        $modelo = $this->input->post('modelo', true);
        $cor = $this->input->post('cor', true);
        $quantidade= $this->input->post('qtd', true);
        if(!(is_null($produto)) && !(is_null($tamanho)) && !(is_null($cor)) && !(is_null($modelo)) && !(is_null($quantidade)) && $quantidade>=0){
            $data=array('tamanhos_id' => $tamanho,'produtos_tb_id'=>$produto, 'quantidade'=>$quantidade,'cor_id'=>$cor,'modelo_id'=>$modelo);
            $str = $this->db->insert_string('estoque_tb', $data);
            $inseriu= $this->db->query($str);
            if($inseriu==1){
                echo $this->db->insert_id();
            }else{echo 0;}
        }else{
            echo 0;
        }      
    }
     public function update() {
        $valor = $this->input->post('valor', true);
        $id = $this->input->post('id', true);
        $produto = $this->input->post('produto', true);
        $tamanho = $this->input->post('tamanho', true);
        $modelo = $this->input->post('modelo', true);
        $cor = $this->input->post('cor', true);
        $quantidade= $this->input->post('qtd', true);
        $data=array('tamanhos_id' => $tamanho, 'quantidade'=>$quantidade,'cor_id'=>$cor,'modelo_id'=>$modelo);
        $where='id='.$id;
        $str = $this->db->update_string('estoque_tb', $data,$where);
        echo $str;
        $inseriu= $this->db->query($str);
        if($inseriu==1){
           echo 1;
        }else{echo 0;}
    
    }
}

    /*
     * End of file estoque.php
     * Location: application/controllers/estoque.php
     */
    ?>

