<?php 
 IF(!esta_logado(FALSE)){ ?>
    <div id="login-cadastro-home">
        <a href="#" id="show-login">Login</a>
        <a href="<?php echo base_url('/site/cadastro');?>">Cadastre-se</a>	
    </div>
<?php } else { ?>
    <div id="boas-vindas">
        <p id="ola">olá <?php echo $user['user_nome']; ?>!</p>
        <ul id="link-perfil">
        	<li><a href="<?php echo base_url("site/perfil");?>">Meu Perfil</a></li>
        	<li><a href="#" id="un-login">Sair</a></li>
        </ul>
    </div>
<?php } ?>
