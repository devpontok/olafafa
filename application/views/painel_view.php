<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title><?php if(isset($titulo)): ?>{titulo}<?php endif?> | {titulo_padrao}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        {headerinc}
        <link rel="shortcut icon" href="images/favicon.ico">
    </head>
    <body>
        
        <div class="container demo-row">
            <img src="<?php echo base_url('imagens/marca.png');?>"/>
        <?php if(esta_logado(FALSE)): ?>
        <div><h2>Painel Administrativo</h2>
        <a href="<?php echo  base_url('usuarios/login');?>"></a>
        </div>
        <?php endif; ?>
           {conteudo}
        </div> <!-- /container -->

        <footer>
         {rodape}
        </footer>
        {footerinc}
    </body>
</html>
