<div class="row demo-row">
    <div class="col-md-12">
        <div class="navbar navbar-inverse">
          	<div class="navbar-header">
	          	<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-collapse-01"></button>
          	</div>            
            <div class="navbar-collapse collapse navbar-collapse-01">
              <ul class="nav navbar-nav navbar-left">
                <li>
                    <a href="<?php echo base_url('adm/cliente/lista');?>">
                      Clientes
                      <span class="navbar-unread">1</span>
                    </a>
                    <ul>
                    <!--    <li><a href="<?php echo base_url('adm/cliente/adminstrador');?>">Administrador</a></li>-->
                      <li><a href="<?php echo base_url('adm/cliente/lista');?>">Dados de clientes</a></li>
                      <li><a href="<?php echo base_url('adm/cliente/news');?>">Base de dados para Newsletter</a></li>    
                    </ul> <!-- /Sub menu -->
                  </li>
                  <li>
                    <a href="<?php echo base_url('adm/produtos/lista');?>">
                      Produtos
                      <span class="navbar-unread">1</span>
                    </a>
                    <ul>
                      <li><a href="<?php echo base_url('adm/produtos/cadastro');?>">Cadastro de Produtos</a></li>
                      <li><a href="<?php echo base_url('adm/produtos/lista');?>">Lista de produtos</a></li>
                      <li><a href="<?php echo base_url('adm/produtos/destaques');?>">Destaques</a></li>
                      <li><a href="<?php echo base_url('adm/produtos/slider');?>">Imagens do slider na HOME</a></li>    
                      <li><a href="<?php echo base_url('adm/produtos/frete_gratis');?>">Produtos com frete gratuito</a></li>
                    </ul> <!-- /Sub menu -->
                  </li>
                  <li>
                    <a href="<?php echo base_url('adm/produtos/lista');?>">
                      Caracter&iacute;sticas
                      <span class="navbar-unread">1</span>
                    </a>
                    <ul>
                      <li><a href="<?php echo base_url('adm/categorias/cadastro');?>">Cadastro de categorias</a></li>
                      <li><a href="<?php echo base_url('adm/caracteristicas/tipo');?>">Cadastro de Tipos de caracterísitcas de produtos</a></li>
                    </ul> <!-- /Sub menu -->
                  </li>
                  <li>
                    <a href="<?php echo base_url('adm/produtos/lista_promocao');?>">
                      Promo&ccedil;&otilde;es
                      <span class="navbar-unread">1</span>
                    </a>
                    <ul>
                      <li><a href="<?php echo base_url('adm/produtos/lista_promocao');?>">Todas as Promo&ccedil;&otilde;es</a></li>    
                      <li><a href="<?php echo base_url('adm/produtos/promocao');?>">Cadastro de Promo&ccedil;&atilde;o</a></li>    
                    </ul> <!-- /Sub menu -->
                  </li>
                  <li>
                    <a href="<?php echo base_url('adm/compras/lista');?>">
                      Relatório de Vendas
                      <span class="navbar-unread">1</span>
                    </a>
                  </li>
                  <li>
                    <a href="#" id="un-login">Sair <span class="navbar-unread">1</span></a>
                  </li>
              </ul>
            </div><!--/.nav -->
          </div>
    </div>
</div>