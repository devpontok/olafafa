<div>
    <div class="col-sm-12">
        <?php
        echo form_open('', array('id' => 'promocoes', 'class' => 'costum loginform'));
        echo form_fieldset("Promo&ccedil;&otilde;es");
        echo errors_validacao();
        echo '<div class="row"><div class="col-sm-3">';
        echo form_input(array('name' => 'nome','id'=>'nome', "placeholder" => "nome", 'class' => 'form-control'), set_value(''), 'autofocus');
        echo '</div><div class="col-sm-3">';
      //  echo '<p>Date: <input type="text" id="datepicker" size="30" class="hasDatepicker"></p>';
        echo form_input(array('name' => 'data_inicial','id'=>'data_inicial', "placeholder" => "Data inicial", 'class' => 'form-control data hasDatepicker'), set_value(''));
         ?>
         <div id="data-inicial-datepicker"class="date-picker"></div>
        <?php 
        echo '</div><div class="col-sm-3">';
        echo form_input(array('name' => 'data_final','id'=>'data_final', "placeholder" => "Data Final", 'class' => 'form-control data hasDatepicker'), set_value(''));
        ?>
        <div id="data-final-datepicker"class="date-picker"></div>
        <?php
        echo '</div><div class="col-sm-3">';
        echo form_submit(array('name' => 'Pesquisar', 'class' => 'btn btn-defalut btn-info', 'style' => 'float:right'), 'Pesquisar');
        echo '</div></div><br/>';
        
        echo form_fieldset_close();
        echo form_close();
        ?>
    </div>

    <?php
if(isset($lista) && $lista){
//Listagem de promoções do site
?>

    <div class='col-lg-20'>
        <div class="row">
            <div class='col-sm-3'>Id</div>
            <div class='col-sm-3'>Nome</div>
            <div class='col-sm-3'>Data Inicial</div>
            <div class='col-sm-3'>Data Final</div>
        </div>
        <img id='LoadingImage' 	style='display:none;' src="<?php echo base_url('/imagens/loading.gif'); ?>"/>
        <div id="resultado" class="row">
            <?php 
            if(!empty($promocoes) && is_array($promocoes)){
            foreach ($promocoes as $promocao) { ?>
                <div class='col-sm-3'><a href="<?php echo base_url('adm/produtos/promocao/'.$promocao->id);?>"><?php echo $promocao->id; ?></a>&nbsp;&nbsp;<a href="javascript:void(0);" class='remove_promocao <?php echo $promocao->id;?>'>Excluir</a>
                	&nbsp;&nbsp;<a href="<?php echo base_url('adm/produtos/add_produto_promocao/'.$promocao->id);?>" class='ver_promocao <?php echo $promocao->id;?>'>Ver Produtos</a></div>
                <div class='col-sm-3'><a href="<?php echo base_url('adm/produtos/promocao/'.$promocao->id);?>"><?php echo mb_convert_encoding($promocao->nome,'utf8'); ?></a></div>
                <div class='col-sm-3'><a href="<?php echo base_url('adm/produtos/promocao/'.$promocao->id);?>"><?php echo (!empty($promocao->data_inicial)&&!is_null($promocao->data_inicial))?$promocao->data_inicial:'Sem data para inicio'; ?></a></div>
                <div class='col-sm-3'><a href="<?php echo base_url('adm/produtos/promocao/'.$promocao->id);?>"><?php echo (!empty($promocao->data_final)&&!is_null($promocao->data_final))?$promocao->data_final:'Sem data de finaliza&ccedil;&atilde;o'; ?></div>
            <?php }
            
            } ?>
        </div>

    </div>

<?php	
}else{
	$fieldset="Dados da Promo&ccedil;&atilde;o";
	if(isset($cadastro)){
		echo form_open(base_url('adm/produtos/promocao'), array('id' => 'promocao_cadastro', 'class' => 'costum loginform col-sm-12'));
	}else{
		echo form_open(base_url('adm/produtos/promocao/'.$promocao[0]->id.'/atualiza'), array('id' => 'promocao_atualizacao', 'class' => 'costum loginform col-sm-12'));
		$fieldset.="<a href='javascript:void(0);' id='produtos_promocao' style='margin-left:1vh;'> Ver produtos desta promo&ccedil;&atilde;o</a>";
		$fieldset.=form_checkbox(array('name'=>'finalizar','id'=>'finalizar', 'value' => 'finalizar','style'=>'margin-left:2vh !important;'));
		$fieldset.=form_label('Finalizar promo&ccedil;&atilde;o', 'finalizar');
	}
	echo form_fieldset($fieldset);

	echo errors_validacao();
	echo '<div class="row" style="margin-bottom:2vh;">';
	$nome=mb_convert_encoding($promocao[0]->nome,'utf8');
	if(!isset($cadastro)){
		echo '<div class="col-sm-12">';
		echo form_input(array('id' => 'id','name'=>'id','disabled'=>'disabled', "placeholder" => "ID",'value'=>isset($promocao[0]->id)?$promocao[0]->id:'','class' => 'form-control col-sm-1'));
		echo form_input(array('name' => 'nome', 'id' => 'nome', "placeholder" => "nome",'value'=>isset($promocao[0]->nome)?$nome:'', 'class' => 'form-control col-sm-10','style'=>'margin-left: 4.95vh !important;'), '', 'autofocus');
		echo '</div>';
	}else{
		echo form_input(array('name' => 'nome', 'id' => 'nome', "placeholder" => "nome",'value'=>isset($promocao[0]->nome)?$nome:'', 'class' => 'form-control col-sm-11','style'=>'left:2%'), '', 'autofocus');
	}
	
	echo "</div><div class='row col-sm-12' style='margin-bottom:2vh;left: -1%;'>";
	echo '<div class="col-sm-2">';
	echo form_checkbox(array('name'=>'percentual','id'=>'percentual', 'value' => 'percentual','checked' => $promocao[0]->percentual));
	echo form_label('Percentual?', 'percentual');
	echo '</div><div class="col-sm-2">';
	echo form_checkbox(array('name'=>'site_todo','id'=>'site_todo', 'value' => 'site_todo','checked' => $promocao[0]->site_todo));
	echo form_label('Todo o site?', 'site_todo');
	echo '</div><div class="col-sm-2">';
	if(isset($promocao[0]->data_inicial) && !empty($promocao[0]->data_inicial)){
		$datas=explode('-',$promocao[0]->data_inicial);
		$data_inicial=$datas[2].'/'.$datas[1].'/'.$datas[0];
	}else{
		$data_inicial='';
	}
	echo form_input(array('name' => 'data_inicial2', 'id' => 'data_inicial2', "placeholder" => "Data Inicial",'value'=>$data_inicial, 'class' => 'form-control data'), '', 'autofocus');
	?>
         <div id="data-inicial2-datepicker"class="date-picker"></div>
        <?php 
	if(isset($promocao[0]->data_final) && !empty($promocao[0]->data_final)){
		$datas=explode('-',$promocao[0]->data_final);
		$data_final=$datas[2].'/'.$datas[1].'/'.$datas[0];
		$data_final=$promocao[0]->data_final;
	}else{
		$data_final='';
	}
	echo '</div><div class="col-sm-2">';
	echo form_input(array('name' => 'data_final2', 'id' => 'data_final2', "placeholder" => "Data Final",'value'=>$data_final,'class' => 'form-control data'), '', 'autofocus');
	?>
         <div id="data-final2-datepicker"class="date-picker"></div>
        <?php 
	echo '</div><div class="col-sm-2">';
	echo form_input(array('name' => 'valor', 'id' => 'valor', "placeholder" => "Valor",'value'=>isset($promocao[0]->valor)?$promocao[0]->valor:'', 'class' => 'form-control valor'), '', 'autofocus');
	echo '</div>';
	if(isset($cadastro)){
		echo form_submit(array('name' => 'cadastrar', 'class' => 'btn btn-defalut btn-info', 'style' => 'float:right'), 'Cadastrar');
	}else{
		echo form_submit(array('name' => 'atualizar', 'class' => 'btn btn-defalut btn-info', 'style' => 'float:right'), 'Atualizar');
	}
	echo '</div></div>';
	
	echo form_fieldset_close();
	echo form_close();
}
?>
</div>