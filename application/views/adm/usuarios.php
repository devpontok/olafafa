<?php
if ( !defined('BASEPATH')) exit('Acesso ao script não é permitido');
switch($tela):
    case 'login':
        echo '<div class="col-md-5 mg-left-29">';
        echo form_open('usuarios/login',array('class'=>'costum loginform'));
        echo form_fieldset("Identifique-se");
        echo errors_validacao();
        echo form_input(array('name'=>'usuario',"placeholder"=>"Nome",'class'=>'form-control'),  set_value('usuario'),'autofocus');
        echo "<br/>";
        echo form_password(array('name'=>'senha',"placeholder"=>"Senha",'class'=>'form-control'),  set_value('senha'));
        echo "<br/>";
        echo form_submit(array('name'=>'Logar','class'=>'btn btn-defalut btn-info','style'=>'float:right'),'Login');
       // echo '<p>'.anchor('usuarios/nova_senha','Esqueci Minha senha').'</p>';
        echo form_fieldset_close();
        echo form_close();
        echo '</div>';
        break;
    default:
    case 'login_adm':
        echo '<div class="col-md-5 mg-left-29">';
        echo form_open('usuarios/login_adm',array('class'=>'costum loginform'));
        echo form_fieldset("Identifique-se");
        echo errors_validacao();
        echo form_input(array('name'=>'usuario',"placeholder"=>"Nome",'class'=>'form-control'),  set_value('usuario'),'autofocus');
        echo "<br/>";
        echo form_password(array('name'=>'senha',"placeholder"=>"Senha",'class'=>'form-control'),  set_value('senha'));
        echo "<br/>";
        echo form_submit(array('name'=>'Logar','class'=>'btn btn-defalut btn-info','style'=>'float:right'),'Login');
        //echo '<p>'.anchor('usuarios/nova_senha','Esqueci Minha senha').'</p>';
        echo form_fieldset_close();
        echo form_close();
        echo '</div>';
        break;
    default:
        echo '<div class="form-group has-error"><p>A tela solicitada nao existe</p></div>';
        break;
endswitch;
 