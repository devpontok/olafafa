<table class='table table-hover' style='clear:left;'>
		<tr></tr>
		<tr>
			<td colspan='3'>
				<h3 style='float:left;display:block;font-size:24px;'>Relat&oacute;rio de vendas.</h3>
			</td>
			<td colspan='2'>
				<p style='float:left;display:block;font-size:20px;'>Data de impress&atilde;o: <?php echo $data;?></p>
			</td>
				<?php echo (isset($periodo) && !empty($periodo))?"<td colspan='3'><p style='float:left;display:block;'>Data de impress&atilde;o: {$periodo}</p></td>":"<td></td><td></td><td></td>";?>
			<td>

			</td>
		</tr>
		<tr class='info'>
			<th>Id</th>
	        <th>Nome do cliente</th>
	        <th>Lista de produtos</th>
	        <th>Status de Pgto</th>
	        <th>Forma de Pgto</th>
	        <th>Taxas</th>
	        <th>Valor</th>
        </tr>
        <?php
            $status='';
            if (!empty($compras) && is_array($compras)) {
            	$total=0;
                foreach ($compras as $compra) {
                    if(!empty($compra['itens'])){
                    	if($compra['status']=='l' || $compra['status']=='p'){
		                    $total+=$compra['valor'];
		                }
                        switch ($compra['status']) {
                            case 'a':
                                $status='Aguardando Pgto';
                                break;
                            case 'c':
                                $status= 'Pgto cancelado';
                                break;
                            case 'p':
                                $status= 'Pgto aprovado';
                                break;
                            case 'e':
                                $status= 'Pgto em análise';
                                break;
                            default:
                                $status= 'Pgto em aberto';
                        } ?>
                       <tr>
                       	<td><?php echo $compra['compra']; ?></td>
                        <td><?php echo $compra['nome']; ?></td>
                        <td><?php foreach ($compra['itens'] as $item) { echo mb_convert_encoding($item,'utf-8')."<br/>";} ?></td>
                        <td><?php echo $status;?></td>
                        <td><?php echo mb_convert_encoding($compra['forma_pagamento'],'utf-8');?></td>
                        <td><?php echo 'R$ '.number_format($compra['taxas'],2,',','.');?></td>
                        <td><?php echo 'R$ '.number_format($compra['valor'],2,',','.');?></td>
                       </tr> 
            <?php
        			}
        		}
        		echo "<tr><td></td><td></td><td></td><td></td><td></td><td colspan='2' style='text-align:right;'>Total R$: ".number_format($total,2,',','.')."</td></tr>";
        	}
        ?>

	
</table>