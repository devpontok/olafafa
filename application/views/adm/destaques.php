 <?php
 echo form_open_multipart('adm/produtos/slider',array('class'=>'col-sm-12'));
 echo form_fieldset('Cadastro de imagens para home',array('class'=>'col-sm-12'));
 ?>
 <div class="container">

                <div class="row fileupload-buttonbar">
                    <div class="col-lg-7">
                        <span class="btn btn-success fileinput-button">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span>Adicione Arquivos...</span>
                            <input type="file" name="arquivo[]" multiple>
                        </span>
                <?php 
                echo  form_submit(array('name' => 'cadastro', 'class' => 'btn btn-defalut btn-info'), 'Enviar Arquivos');
                ?>
                <!-- The global file processing state -->
                <span class="fileupload-process"></span>
            </div>
            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
<?php
	echo form_fieldset_close();
	echo form_close();
/*
echo form_open_multipart('adm/produtos/slider');
echo form_fieldset('Cadastro de imagens para home');
echo validation_errors();
echo form_upload(array('name' => 'arquivo', 'placeholder' => 'Arquivo', 'class' => "form-control btn btn-block btn-lg btn-default","value="));
*/


foreach($destaques as $destaque){
    ?>
<div class="foto">
    <img id="<?php echo $destaque;?>" class="apagar" src="<?php echo base_url("galeria/destaques/$destaque");?>"/>
</div>


<?php
}