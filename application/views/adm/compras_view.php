

<div id="resultado">
    <div class="col-sm-12" style="margin-bottom:3%;">
        <?php
        echo form_open('', array('id' => 'pesquisa_compra', 'class' => 'costum loginform'));
        echo form_fieldset("Relat&oacute;rio de Compras");
        echo errors_validacao();
       /* echo '<div class="row"><div class="col-sm-6">';
        echo form_input(array('name' => 'nome', 'id' => 'nome', "placeholder" => "nome", 'class' => 'form-control'), set_value(''), 'autofocus');
        echo '</div><div class="col-sm-6">';
        echo form_input(array('name' => 'cpf', 'id' => 'cpf', "placeholder" => "CPF", 'class' => 'form-control'), set_value(''));
        echo '</div></div><br/>';
        */
?>

<?php //  echo form_input(array('name' => 'data_inicial','id'=>'data_inicial', "placeholder" => "Data inicial", 'class' => 'span form-control data datepicker col-xs-3','style'=>'margin-right:20%'), set_value(''));
        echo form_input(array('name' => 'data_inicial','id'=>'data_inicial', "placeholder" => "Data Inicial", 'class' => 'form-control data hasDatepicker col-xs-3'), set_value(''));
        ?>
        <div id="data-inicial-datepicker"class="date-picker"></div>
        <?php 
        echo form_input(array('name' => 'data_final','id'=>'data_final', "placeholder" => "Data Final", 'class' => 'form-control data hasDatepicker col-xs-3'), set_value(''));
        ?>
        <div id="data-final-datepicker"class="date-picker"></div>
        <?php
        echo form_submit(array('name' => 'Pesquisar', 'class' => 'btn btn-defalut btn-info', 'style' => 'float:right'), 'Pesquisar');
        echo form_fieldset_close();
        echo form_close();
        ?>
    </div>
    <div class='col-lg-20'>
        <div class="row" style='margin-bottom:3%;border-bottom:solid 1px #34495e;'>
            <button href='javascript:void(0);' id='pdf_compras' class='btn btn-defalut btn-danger'>Imprimir PDF</button>&nbsp;&nbsp;&nbsp;
            <button href='javascript:void(0);'  id='excel_compras' class='btn btn-defalut btn-success'>Imprimir EXCEL</button>
            <br/> 
            <strong>Sinaliza&ccedil;&atilde;o de Status de envio de produtos</strong><br/>
            <img src='<?php echo base_url('imagens/images/liberar_frete.png');?>' />Liberar Frete - <span style='color:red;font-size:13px;'>ao clicar nesta imagem o produto &eacute; marcado como liberado para o cliente</span>&nbsp;&nbsp;
            <br/>
            <img src='<?php echo base_url('imagens/images/frete_liberado.png');?>' />Produto liberado&nbsp;&nbsp;
            <br/>
            <img src='<?php echo base_url('imagens/images/frete_cancelado.png');?>' />Envio do produto cancelado&nbsp;&nbsp;
        </div>
         <?php
            if(!empty($compras)){ ?>
        <div class="row" style='margin-bottom:3%;border-bottom:solid 1px #34495e;'>
            <div class='col-sm-1'>Id</div>
            <div class='col-sm-2'>Nome do cliente</div>
            <div class='col-sm-3'>Lista de produtos</div>
            <div class='col-sm-2'>Status de Pgto</div>
            <div class='col-sm-2'>Forma de Pgto</div>
            <div class='col-sm-1'>Taxas</div>
            <div class='col-sm-1'>Valor</div>
        </div>
        <?php }?>
        <img id='LoadingImage'  style='display:none;position:fixed;margin-left:45%;margin-top:45%;' src="<?php echo base_url('/imagens/loading.gif'); ?>"/>
        <div id="resultado" class="row">

            <?php

            if(!empty($compras)){
                
                
                if (!empty($compras) && is_array($compras)) {
                    
                    foreach ($compras as $compra) {
                        $class="class='liberar_frete {$compra['compra']}'";
                        $status='';
                        $alt='Liberar frete';
                        $cursor='style="cursor:pointer;"';
                        $carrinho=base_url();
                        if(!empty($compra['itens'])){
                            switch ($compra['status']) {
                                case 'a':
                                    $status='Aguardando Pagamento';
                                    $carrinho.='imagens/images/liberar_frete.png';
                                    break;
                                case 'c':
                                    $status= 'Pgto cancelado';
                                    $class='';
                                    $alt='Frete cancelado';
                                    $cursor='';
                                     $carrinho.='imagens/images/frete_cancelado.png';
                                    break;
                                case 'p':
                                    $status= 'Pgto aprovado';
                                    $carrinho.='imagens/images/liberar_frete.png';
                                    break;
                                case 'e':
                                    $status= 'Pgto em análise';
                                    $carrinho.='imagens/images/liberar_frete.png';
                                    break;
                                case 'l':
                                    $status= 'Pgto aprovado';
                                    $carrinho.='imagens/images/frete_liberado.png';
                                    $alt='Produto enviado';
                                    $cursor='';
                                    break;
                                default:
                                    $status= 'Pgto em aberto';
                                    $carrinho.='imagens/images/liberar_frete.png';
                            } 
                            $title="title='{$alt}'";
                            $alt="alt='{$alt}'";
                            ?>
                            <div class='col-sm-1'><a href="<?php echo base_url('adm/compras/itens/' . $compra['compra']); ?>"><?php echo $compra['compra']; ?></a></div>
                            <div class='col-sm-2'><?php echo $compra['nome']; ?></div>
                            <div class='col-sm-3' style='margin-bottom:2%;'><?php foreach ($compra['itens'] as $item) { echo "$item<br/>";} ?></div>
                            <div class='col-sm-2'><?php echo $status."<img src='{$carrinho}' {$alt} {$title} {$class} {$cursor} />";?></div>
                            <div class='col-sm-2'><?php echo $compra['forma_pagamento'];?></div>
                            <div class='col-sm-1'><span style='font-size:15px'>R$ <?php echo $compra['taxas'];?></span></div>
                            <div class='col-sm-1'><span style='font-size:15px'>R$ <?php echo $compra['valor'];?></span></div>
                            <br class="clear-line">
                            <hr style='border:dotted 1px #34495e;'/>
                    <?php
                        }
                    }
                    echo"<div class='col-sm-2' style='float:right;'>Total R$: {$total}</div>";
                }
            }else{
                echo $mensagem;
            }
            ?>
        </div>
    </div>
</div>

