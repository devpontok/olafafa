<div class="carrinho">

<p id="carrinho-titulo-content">Seu carrinho</p>
<?php

if(isset($carrinho) && !empty($carrinho)):
    
    foreach($carrinho as $id=>$item):
    $contador=0;
if(count($item['tamanhos'])>0 && is_array($item['tamanhos'])){
   // echo var_dump($item['tamanhos']);
    foreach($item['tamanhos'] as $key=>$tamanho):

    $nome='';
    $this->db->where(array('id'=>$key));
    $tam=$this->db->get('estoque_tb')->result();
    
    $this->db->where(array('id'=>$tam[0]->tamanhos_id));
    $tamanho_item=$this->db->get('caracteristicas_tb')->result_array();
    $this->db->where(array('id'=>$tam[0]->cor_id));
    $cor_item=$this->db->get('caracteristicas_tb')->result_array();
    $this->db->where(array('id'=>$tam[0]->modelo_id));
    $modelo_item=$this->db->get('caracteristicas_tb')->result_array();
    $nome="Modelo: {$modelo_item[0]['nome']}. Tamanho: {$tamanho_item[0]['nome']}. Cor: {$cor_item[0]['nome']}";
?>
    <div id="<?php echo $item['id']."_".$contador;?>" class="item">
        <div class="um_quinto"> 
        <input  type="text" value="<?php echo $tamanho;?>" />
    </div>
        <div class="um_terco"><?php echo $item['name'].". Tamanho: ".$nome;?> <input type="hidden" class="tamanho-item" value="<?php echo $key;?>"/><input type="hidden" class="peso-item" value="<?php echo $item['peso'];?>"/></div>
        <div class="um_terco" style="width: 69px; margin: 0px;">
            <input type="hidden" class="unidade" value="<?php echo $item['price'];?>" />R$<label> <?php echo $tamanho*(float)$item['price'];?> </label>
        </div>
    <span class="div-carrinho"></span>
    </div>

<?php
    $contador++;
    endforeach;
}else{
    ?>
    <div id="<?php echo $item['id']."_0";?>" class="item">
        <div class="um_quinto"> 
        <input  type="text" value="<?php echo $item['qty'];?>" />
    </div>
        <div class="um_terco"><?php echo $item['name']?> <input type="hidden" class="tamanho-item" value="0"/><input type="hidden" class="peso-item" value="<?php echo $item['peso'];?>"/></div>
        <div class="um_terco" style="width: 69px; margin: 0px;">
            <input type="hidden" class="unidade" value="<?php echo $item['price'];?>" />R$<label> <?php echo $item['qty']*(float)$item['price'];?> </label>
        </div>
    <span class="div-carrinho"></span>
    </div>

<?php
}
    endforeach;
?>
<br/>

<div style="display:none">
        <label id="itens"><?php echo $itens;?></label>
    </div>
<div id="total">
    Total: R$ <label><?php echo $total;?></label>
    
</div> 

    
<?php
    
endif;
?>
<br class="clear-line"/>
</div>
<form id="pag" target="_self" method="post"   action="https://pagseguro.uol.com.br/v2/checkout/payment.html">
	<div class="envio">
	Tipo de envio
	<input type="radio" value="1" name="shippingType" checked="on"><label>PAC</label>
	<input type="radio" value="2" name="shippingType"><label>SEDEX</label> 	
</div>
<br/>
</form>
<a class="btn-chekout" style="width:100px!important; background-size: 100% !important; background-repeat: none !important; height:19px;" href="#">Checkout</a>