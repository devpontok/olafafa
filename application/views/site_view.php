<html>
    <head>
        <meta lang="pt-br"></meta>
        <meta charset="utf-8"></meta>
        <link rel="shortcut icon" href="<?php echo base_url("imagens/favicon.png");?>" type="image/x-icon"/>
        <title>{titulo_padrao}!<?php echo (isset($titulo)) ? " | {$titulo}" : ""; ?></title>
        <?php echo (isset($headerinc)) ? $headerinc : ''; ?>
    </head>
    <body>
      
<!--INICIO DO CABEÇALHO-->

	<header id="cabecalho">
		<div class="site">
			<span id="nuvem-esquerda">
				<img src="<?php echo base_url('imagens/nuvem.png');?>" id="esquerda">
			</span>
			<div id="box-login">
                    <form action="#" id="logar">
                    <p>Login</p>
                    <input id="login" name="login" type="text" placeholder=" e-mail" required="required"/>
                    <input id="senha" name="senha" type="password" placeholder=" senha"  required="required"/>
                    <a href="javascript:void(0);" onClick="renovar_senha();" class="link-home" title='Digite seu email antes de clicar aqui'>Esqueci Minha senha</a>
                    <input type="submit" id="btn-login" value=''/>
                     
                    </form>
                </div>
			<span id="nuvem-direita">
				<img src="<?php echo base_url('imagens/nuvem.png');?>" id="esquerda">
			</span>
		</div>
	</header>

<!--FAIXA DO CABEÇALHO-->
	
	<div id="faixa-cabecalho">
		<div class="site">
			<div id="marca">
				<a href="<?php echo base_url();?>"><img src="<?php echo base_url('imagens/marca.png');?>"></a>
			</div>
                   
			<!--Links do cabeçalho para animação-->
                          {login}
                          <div id="container-sacola">
				<div id="sacola">
					<img src=<?php echo base_url("imagens/sacola.png");?>>
					<hr>
					<p ><label><?php echo $itens;?></label> ITENS</p>
					<hr>
					<P>R$ <label><?php echo $total;?></label></P>
				</div>
			</div>	
                          <div id="resultado"></div>
   
			<nav id="menu">
				<ul>
					<div id="borda-menu">
						<a id="bebe" href="<?php echo base_url('categorias/bebe');?>" <?php echo strpos($_SERVER['REQUEST_URI'], "bebe")>0?' class="ativo" ':'';?>>Bebê</a>
                            <a id="menino" href="<?php echo base_url('categorias/menino');?>" <?php echo strpos($_SERVER['REQUEST_URI'],"menino")>0?' class="ativo" ':'';?>>Menino</a>
                            <a id="menina" href="<?php echo base_url('categorias/menina');?>" <?php echo strpos($_SERVER['REQUEST_URI'],"menina")>0?' class="ativo" ':'';?>>Menina</a>
                            <a id="sapato" href="<?php echo base_url('categorias/sapato');?>" <?php echo strpos($_SERVER['REQUEST_URI'], "sapato")>0?' class="ativo" ':'';?>>Sapato</a>
                            <a id="acessorios" href="<?php echo base_url('categorias/acessorios');?>" <?php echo strpos($_SERVER['REQUEST_URI'],"acessorios")>0?' class="ativo" ':'';?>>Acessórios</a>
					</div>
				</ul>
			</nav>
                          
		</div>
	</div>

<!--FIM DO CABEÇALHO / INICIO DO CONTEÚDO-->	

        <div id="conteudo">
         <!--   <div class="site">-->
                <?php echo (isset($conteudo)) ? $conteudo : 'ERROR 404'; ?>
            <!--</div>-->
        </div>
        <?php echo (isset($rodape)) ? $rodape : ''; ?>
        <?php echo (isset($footerinc)) ? $footerinc : ''; ?>
    </body>
</html>
