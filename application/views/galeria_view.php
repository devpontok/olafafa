<?php
/*
echo form_open_multipart("adm/produtos/add_Imagem/{$id}",array("id"=>"galeria"));

echo form_upload(array('name' => 'arquivo', 'placeholder' => 'Adicionar arquivo', 'class' => "col-md-5 form-control btn btn-block btn-lg btn-default"));
echo ' <label class="radio rigth col-md-2 mg-left-3">
            <input type="radio" name="prinicipal" id="principal" value="principal" data-toggle="radio">
            Principal
          </label>';
 echo form_submit(array('name' => 'cadastro', 'class' => 'btn btn-defalut btn-info', 'style' => 'float:right'), 'Cadastrar');
echo form_close();
echo '<Br/><Br/><Br/>
<div class="row">';
if (is_array($galeria)):
    foreach ($galeria as $imagem):
$check= ($imagem->principal==1)?'checked=checked':'';
        ?>

        <div class="imagem">
            <label class="radio rigth col-md-2" id="<?php echo $imagem->arquivo;?>-<?php echo $id;?>">
                <input type="radio" name="principal_ativo" class="principal"  value="principal" data-toggle="radio" <?php echo $check; ?>>
                Principal
            </label>
          <a href="#" class="remove"id="<?php echo $imagem->arquivo;?>" class="mg-left-16 btn btn-lg col-md-2 btn-error"><span class="fui-cross"></span></a>
<br/>
           <img style="width:100px;"    src="<?php echo base_url("galeria/roupas/{$imagem->arquivo}"); ?>"/>
        </div>

        <?php
    endforeach;

endif;
?>
        </div>
    
*/ ?>
<div class="container">
    <!-- The file upload form used as target for the file upload widget -->
    <form id="fileupload" action="<?php echo base_url('adm/produtos/add_Imagem/'.$id);?>" method="POST" enctype="multipart/form-data">
        <!-- Redirect browsers with JavaScript disabled to the origin page -->
        <noscript><input type="hidden" name="redirect" value="http://blueimp.github.io/jQuery-File-Upload/"></noscript>
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <input type='hidden' id='produto' name='poduto' value='<?php echo $produto;?>'/>
            <div class="col-lg-7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add files...</span>
                    <input type="file" name="files[]" multiple>
                </span>
                <button type="submit" class="btn btn-primary start">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start upload</span>
                </button>
                                <!-- The global file processing state -->
                <span class="fileupload-process"></span>
            </div>
            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
    </form>
    <br>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Imagens do produto</h3>
        </div>
        <div class="panel-body">
           <?php
           echo '<div class="row">';
            if (is_array($galeria)):
                foreach ($galeria as $imagem):
            $check= ($imagem->principal==1)?'checked=checked':'';
                    ?>

                    <div class="imagem">
                       <ul>
                            <li>
                                <label class="radio rigth col-md-2" id="<?php echo $imagem->arquivo;?>-<?php echo $id;?>">
                                    <input type="radio" name="principal_ativo" class="principal"  value="principal" data-toggle="radio" <?php echo $check; ?>>
                                    Principal
                                </label>
                            </li>
                            <li>
                                <a href="#" class="remove"id="<?php echo $imagem->arquivo;?>" class="mg-left-16 btn btn-lg col-md-2 btn-danger"><i class="glyphicon glyphicon-trash"></i></a>
                            </li>                        
                        </ul>
                       <img style="width:100px;height:100px;"   src="<?php echo base_url("galeria/roupas/{$imagem->arquivo}"); ?>"/>
                       <input type='text' name='alt'  style='margin-top:2px' class="form-control <?php echo $imagem->arquivo;?> alt" placeholder='ALT da imagem' value='<?php echo $imagem->alt;?>'/>
                       <input type='text' name='title' style='margin-top:2px' class="form-control <?php echo $imagem->arquivo;?> title" placeholder='TITLE da imagem' value='<?php echo $imagem->title;?>'/>
                    </div>

                    <?php
                endforeach;

            endif;
           ?>
        </div>
    </div>
</div>