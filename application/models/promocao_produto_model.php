<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');

class promocao_produto_model extends CI_Model {
	public function add($promocao_id,$produto_id){
		$data=array('produtos_tb_id'=>$produto_id,'promocao_tb_id'=>$promocao_id);
		$str = $this->db->insert_string('produtos_promocao_tb', $data);
		$this->db->query($str);
	//	echo $this->db->insert_id();
	}
	public function deleta_tudo($promocao){
		$this->db->where('promocao_tb_id', $promocao);
		$this->db->delete('produtos_promocao_tb');
	}
	public function remove($promocao_id,$produto_id){
		$data=array('produtos_tb_id'=>$produto_id,'promocao_tb_id'=>$promocao_id);
		$this->db->where($data);
		$this->db->delete('produtos_promocao_tb');
	//	echo $this->db->last_query();
	}
	public function get_produtos_promocoes_array($promocoes_concorrentes){
		$retorno=array();
		if(!empty($promocoes_concorrentes)){
			$this->db->where_in('promocao_tb_id',$promocoes_concorrentes);
			$this->db->from('produtos_promocao_tb');
			$this->db->select('produtos_tb_id');
			$dado=$this->db->get()->result();
			foreach($dado as $item){
				array_push($retorno,$item->produtos_tb_id);
			}
		}
		return $retorno;
	}
}
?>