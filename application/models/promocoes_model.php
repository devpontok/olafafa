<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');

class promocoes_model extends CI_Model {
	public function get_all(){
		$this->db->from('promocao_tb');
		$dado=$this->db->get();
		return $dado->result();
	}
	public function delete_promocao($id=null){
		$hoje=strtotime(date('Y-m-d'));
		$this->db->from('promocao_tb');
		$this->db->where(array('id'=>$id));
		$dado=$this->db->get();
		$dado=$dado->result();
		$data=strtotime($dado[0]->data_inicial);
		if(empty($data) || $data>$hoje){
          	$saida=$this->db->delete('promocao_tb',array('id'=>$id));
           	if($saida==1){
           		echo 'Promoção foi encerrada com sucesso';
           	}else{
				echo "A promoção não pode ser encerrada";
			}
		}else{
			if(strtotime($data)>$hoje){
				echo 'A promoção não pode ser encerrada, pois já foi ativada';
			}else{
				echo "A promoção não pode ser encerrada";
			}
	       
	    }
        
    }
	public function get_promocao($array){
	//	echo var_dump($array);
		
		if(is_array($array)){
			
			$where='';
			if($array['nome']!=''){
				$this->db->like('nome',$array['nome']);
			}
			if($array['data_inicial']!=''){
				$data=explode('/',$array['data_inicial']);
				$data=$data[2]."/".$data[1]."/".$data[0];
				$this->db->where('data_inicial >=',$data);

			}
			if($array['data_final']!=''){
				$data=explode('/',$array['data_final']);
				$data=$data[2]."/".$data[1]."/".$data[0];
				$str='data_final <='.$data." or isnull(data_final)";
				$this->db->or_where($str,null,false);
			}
			$dado=$this->db->get('promocao_tb');
			return $dado->result();
		}else{
			return 'Por favor, selecione dados para a pesquisa';
		}
	}
	public function get_by_id($id=null){
		$retorno='';
		if(is_null($id) || !isset($id)){
			$retorno='A promoção selecionada não foi localizada';
		}else{
			$this->db->from('promocao_tb');
			$this->db->where(array('id' => $id));
			$dado=$this->db->get();
			$retorno=$dado->result();
		}
		return $retorno;
	}
	public function add($nome,$valor,$data_inicial,$data_final,$percentual,$todo_site){
		$data=array('nome'=>$nome,'percentual'=>($percentual=='percentual')?1:0,'site_todo'=>($todo_site=='site_todo')?1:0,'valor'=>$valor);
		//trocar valor de datas
		if(!empty($data_inicial)&&($data_inicial!='')){
			$datas=explode('/',$data_inicial);
			$data_inicial=$datas[2].'/'.$datas[1].'/'.$datas[0];
			$data['data_inicial']=date('Y-m-d',strtotime($data_inicial));
		}else{
			$data_inicial=null;
		}
		if(!empty($data_final)&&($data_final!='')){
			$datas=explode('/',$data_final);
			$data_final=$datas[2].'/'.$datas[1].'/'.$datas[0];
			$data['data_final']=date('Y-m-d',strtotime($data_final));
		}else{
			$data['data_final']=null;
		}

	
		$str = $this->db->insert_string('promocao_tb', $data);
		$this->db->query($str);
		return $this->db->insert_id();
	}
	public function atualiza($id,$nome,$valor,$data_inicial,$data_final,$percentual,$todo_site){
		$data=array('nome'=>$nome,'percentual'=>($percentual=='percentual')?1:0,'site_todo'=>($todo_site=='site_todo')?1:0,'valor'=>$valor);
		//trocar valor de datas
		if(!empty($data_inicial)&&($data_inicial!='')){
			$datas=explode('/',$data_inicial);
			$data_inicial=$datas[2].'/'.$datas[1].'/'.$datas[0];
			$data['data_inicial']=date('Y-m-d',strtotime($data_inicial));
		}else{
			$data_inicial=null;
		}
		if(!empty($data_final)&&($data_final!='')){
			$datas=explode('/',$data_final);
			$data_final=$datas[2].'/'.$datas[1].'/'.$datas[0];
			$data['data_final']=date('Y-m-d',strtotime($data_final));
		}else{
			$data['data_final']=null;
		}

		//Trocar valor de dados decimais
		$data['valor']=str_replace(',','.',str_replace('.','',$valor));
		$this->db->where('id',$id);
		$str = $this->db->set($data);
		return $this->db->update('promocao_tb');
	}
	public function promocao_site_todo($id){
		$this->db->select('site_todo');
		$this->db->from('promocao_tb');
		$this->db->where(array('id'=>$id,'site_todo'=>1));
		$dado=$this->db->get()->result();
		if(!empty($dado)){
			return true;
		}else{
			return false;
		}
	}
	public function promocao_percentual($id){
		$this->db->select('percentual');
		$this->db->from('promocao_tb');
		$this->db->where(array('id'=>$id,'percentual'=>1));
		$dado=$this->db->get()->result();
		if(!empty($dado)){
			return true;
		}else{
			return false;
		}
	}
	public function colocar_site_inteiro($promocao){
		$this->db->query('update promocao_tb set site_todo=1 where id='.$promocao);
	}
	public function remover_site_inteiro($promocao){
		$this->db->query('update promocao_tb set site_todo=0 where id='.$promocao);
	}
	public function promocao_vigente_produtos($produto=null){
		$hoje=date('Y-m-d');
		$data=array();
		$query="select 
			distinct(promocao_tb.id),promocao_tb.percentual,promocao_tb.site_todo,
			promocao_tb.valor,produtos_promocao_tb.promocao_tb_id
		 from 
		 	promocao_tb 
		 left join 
		 	produtos_promocao_tb on 
		 		produtos_promocao_tb.promocao_tb_id=promocao_tb.id 
		 where 
		 	data_inicial<='{$hoje}' and (data_final>='{$hoje}' OR ISNULL( data_final ))";
		 if(!is_null($produto)){
		 	$query.=" and (promocao_tb.site_todo=1 or produtos_promocao_tb.produtos_tb_id='{$produto}') ORDER BY promocao_tb.site_todo asc;";
		 }else{
		 	$query.=" and promocao_tb.site_todo=1 ORDER BY promocao_tb.site_todo asc;";
		 }
		 //echo $query;
		$promocoes=$this->db->query($query)->result();
		foreach($promocoes as $promocao){
			array_push($data,array('id'=>$promocao->id,'valor'=>$promocao->valor,'percentual'=>$promocao->percentual));
		}
		return $data;
	}
	public function promocao_por_periodo($data_inicial,$data_final,$promocao){
		$data=array();
		//$this->db->from('promocao_tb');
		//$this->db->where(array('data_inicial>='=>$data_inicial,'data_final<='=>$data_final));
		$query="select id from promocao_tb where data_inicial<='{$data_inicial}' and data_final>='{$data_final}' and id!='{$promocao}';";
		
		$promocoes=$this->db->query($query)->result();
	
		foreach($promocoes as $promocao){
			array_push($data,$promocao->id);
		}
		return $data;
	}
		
}
/*
 * End of file promoções_model.php
 * Location: application/models/promocoes_model.php
 */

?>