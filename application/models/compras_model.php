<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');

class compras_model extends CI_Model {

    public function get_all() {
        $this->db->select('compras_tb.forma_pagamento,compras_tb.taxas,compras_tb.id as compra,login_tb.email,data_compra,cliente_tb.nome,status,valor');
        $this->db->join('cliente_tb', 'cliente_tb.id=compras_tb.cliente_tb_id');
        $this->db->join('login_tb', 'cliente_tb.id=login_tb.cliente_tb_id');
        $retorno= $this->db->get('compras_tb');
       // echo $this->db->last_query();
        return $retorno->result();
    }

    public function do_cadastro() {
        $this->load->library('session', 'database');
        $usuario = $this->session->userdata('user_id');

        if(!isset($_SESSION)){
            session_start();
        }
        $dados['carrinho'] = $_SESSION['carrinho'];
        $data = array('valor' => $_SESSION['total'], 'status' => 'n','cliente_tb_id'=>$usuario);
        $data['data_compra'] =date ("Y-m-d H:i:s");
        $str = $this->db->insert_string('compras_tb', $data);
        $this->db->query($str);
        return $this->db->insert_id();
    }
    public function get_itensCompra($idcompra){
        $this->db->select('produtos_tb.*,itens.quantidade');
        $this->db->from('produtos_tb');
        $this->db->join('itens','produtos_tb_id=produtos_tb.id');
        $this->db->where(array('compras_tb_id'=>$idcompra));
        return $this->db->get()->result();
    }
    public function get_dados($data_inicial=null,$data_final=null) {
        $sql='select compras_tb.forma_pagamento,compras_tb.taxas,compras_tb.id as compra,login_tb.email,data_compra,cliente_tb.nome,status,valor 
        from compras_tb join cliente_tb on cliente_tb.id=compras_tb.cliente_tb_id  join login_tb on cliente_tb.id=login_tb.cliente_tb_id';
        if(!is_null($data_inicial) || !empty($data_final)){
           $sql.=" where ";
           if(!is_null($data_inicial) && !empty($data_inicial)){
                $sql.=" compras_tb.data_compra>= '{$data_inicial} 00:00:00' ";
                if(!is_null($data_final) && !empty($data_final)){
                    $sql.=" and compras_tb.data_compra<= '{$data_final} 00:00:00' ";
                }
           }else{
                if(!is_null($data_final) && !empty($data_final)){
                    $sql.=" compras_tb.data_compra<= '{$data_final} 00:00:00' ";
                }
           }
        //   echo $sql;
        }
        $retorno= $this->db->query($sql);
        return $retorno->result();
    }
    public function atualiza_frete($id=null){
        if(!is_null($id)){
            $data=array('status'=>'l');
            $where=array('id'=>$id);
            echo $this->db->query($this->db->update_string('compras_tb', $data,$where));
        }else{
            echo 0;
        }
    }
}

/*
 * End of file compras.php
 * Location: application/models/compras.php
 */
?>
