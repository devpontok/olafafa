<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');

class itens extends CI_Model {

    public function get_all($id = NULL) {
    	
        $this->db->select('cliente_tb.nome as cliente,tamanhos.nome as tamanho,produtos_tb.nome as produto, produtos_tb.valor as unidade,itens.quantidade,data_compra,status,cliente_tb.rua,cliente_tb.bairro,cliente_tb.cidade,cliente_tb.estado,cliente_tb.cep,cliente_tb.complemento,cliente_tb.telefone,login_tb.email');
        $this->db->join('compras_tb', 'compras_tb.id=itens.compras_tb_id');
	    $this->db->join('tamanhos',"itens.tamanhos_id=tamanhos.id", 'left');	
        $this->db->join('cliente_tb', 'compras_tb.cliente_tb_id=cliente_tb.id');
		$this->db->join('login_tb', 'login_tb.cliente_tb_id=cliente_tb.id');
        $this->db->join('produtos_tb', 'produtos_tb.id=itens.produtos_tb_id');
        $retorno= $this->db->get('itens')->result();
      //  echo $this->db->last_query();
        return $retorno;
    }

    public function get_byCompra($compra) {
        $this->db->select("produtos_tb.id,produtos_tb.nome as produto,tamanhos.nome as tamanho,itens.quantidade");
        $this->db->join("produtos_tb", "itens.produtos_tb_id=produtos_tb.id");
        $this->db->join("tamanhos", "tamanhos.id=itens.tamanhos_id", "left");
        $this->db->where(array("compras_tb_id" => "{$compra->compra}"));
        return $this->db->get("itens")->result_array();
    }

    public function do_cadastro($idcompra = NULL) {
        if (!is_null($idcompra)) {
            if (!isset($_SESSION)) {
                session_start();
            }

            $dados['carrinho'] = $_SESSION['carrinho'];
            # echo var_dump($dados);
            foreach ($dados['carrinho'] as $item) {
                //echo var_dump($item);
                $data = array('produtos_tb_id' => "{$item['id']}",
                    'compras_tb_id' => "$idcompra"
                );
                if (count($item['tamanhos']) > 0) {
                    $data = array();
                    foreach ($item['tamanhos'] as $key=>$tamanho) {
                        $data = array('produtos_tb_id' => "{$item['id']}",
                            'compras_tb_id' => "$idcompra"
                        );

                        //tamanho[0]=tamanho_id
                        //tamanho[1]=qtd de itens deste tamanho
                        $data['tamanhos_id'] = "{$key}";
                        $data['quantidade'] = "{$tamanho}";
                        $str = $this->db->insert_string('itens', $data);
                        //     echo $str;
                        $this->db->query($str);
                    }
                } else {
                    $data['quantidade'] = "{$item['qty']}";
                    $str = $this->db->insert_string('itens', $data);
                    //   echo $str;
                    $this->db->query($str);
                }
            }
        } else {
            return false;
        }
    }

}

/*
 * End of file itens.php
 * Location: application/models/itens.php
 */
?>
