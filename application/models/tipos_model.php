<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');

class tipos_model extends CI_Model {
	public function get_all($id=NULL){
		if(!is_null($id)){
			$this->db->where(array('id'=>$id));
		}
		return $this->db->get('tipo_tb')->result();
	}
	public function do_cadastro($nome=NULL){
		if(!is_null($nome)){
			$data=array('nome'=>$nome);
			$this->db->query($this->db->insert_string('tipo_tb',$data));
		}else{
			return false;
		}
		
	}
	public function do_update($nome=NULL,$id=NULL){
		if(!is_null($nome) && !is_null($id)){
			$data=array('nome'=>$nome);
			$where = array("id" => $id);
			$this->db->query($this->db->update_string('tipo_tb',$data,$where));
		}else{
			return false;
		}
		
	}
}
?>