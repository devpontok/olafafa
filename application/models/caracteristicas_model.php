<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');

class caracteristicas_model extends CI_Model {
	public function get_all($id=NULL){
		if(!is_null($id)){
			$this->db->where(array('id'=>$id));
		}
		return $this->db->get('caracteristicas_tb')->result();
	}
	public function get_all_by_tipo($id=NULL){
		if(!is_null($id)){
			$this->db->where(array('tipo_id'=>$id));
		}
		return $this->db->get('caracteristicas_tb')->result();
	}
	public function do_cadastro($tipo=null,$nome=NULL){
		if(!is_null($nome) && !is_null($tipo)){
			$data=array('tipo_id'=>$tipo,'nome'=>$nome);
			$this->db->query($this->db->insert_string('caracteristicas_tb',$data));
		}else{
			return false;
		}
		
	}
	public function do_update($nome=NULL,$id=NULL,$tipo=null){
		if(!is_null($nome) && !is_null($id)){
			$data=array('nome'=>$nome);
			if(!is_null($tipo)){
				$data['tipo']=$tipo;
			}
			$where = array("id" => $id);
		//	echo $this->db->update_string('caracteristicas_tb',$data,$where);
			return $this->db->query($this->db->update_string('caracteristicas_tb',$data,$where));
		}else{
			return false;
		}
		
	}
}
?>