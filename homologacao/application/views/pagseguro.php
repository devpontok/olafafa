<?php
if (esta_logado(false)) {
    if (isset($_SESSION['carrinho'])) {
        ?>

        <!-- Campos obrigatórios -->  
        <input type="hidden" name="receiverEmail" value="josefinasarangel@gmail.com"/>  
        <input type="hidden" name="currency" value="BRL"/>  
        <?php
        #$idcompra=>id da última compra realizada
        $cont = 1;
        foreach ($carrinho as $item) {
            if (count($item['tamanhos']) > 0) {
                //tamanho[0]=tamanho_id
                //tamanho[1]=qtd de itens deste tamanho
                
                foreach ($item['tamanhos'] as $tamanho) {
                    $this->db->where(array('id' => $tamanho['0']));
                    $tam = $this->db->get('tamanhos')->result();
                    $nome = $tam['0']->nome;
                    ?>
                    <input type="hidden" name="itemId<?php echo $cont; ?>" value="<?php echo $item['id']; ?>">  
                    <input type="hidden" name="itemWeight<?php echo $cont; ?>" value="<?php echo $item['peso'];?>">  
                    <input type="hidden" name="itemDescription<?php echo $cont; ?>" value="<?php echo $item['name'] . ". Tamanho: " . $nome; ?>">  
                    <input type="hidden" name="itemAmount<?php echo $cont; ?>" value="<?php echo $item['price']; ?>">  
                    <input type="hidden" name="itemQuantity<?php echo $cont; ?>" value="<?php echo $tamanho[1]; ?>">     
                    <?php
                    $cont++;
                }
            } else {
                if ($item['qty'] > 0):
                    ?>
                    <input type="hidden" name="itemId<?php echo $cont; ?>" value="<?php echo $item['id']; ?>">  
                    <input type="hidden" name="itemWeight<?php echo $cont; ?>" value="<?php echo $item['peso'];?>">  
                    <input type="hidden" name="itemDescription<?php echo $cont; ?>" value="<?php echo $item['name']; ?>">  
                    <input type="hidden" name="itemAmount<?php echo $cont; ?>" value="<?php echo $item['price']; ?>">  
                    <input type="hidden" name="itemQuantity<?php echo $cont; ?>" value="<?php echo $item['qty']; ?>">  
                    <?php
                    $cont++;
                endif;
            }
        }
        ?>
        <input type="hidden" name="reference" value="<?php echo $idcompra; ?>"/>  
        <!--<input type="hidden" name="shippingType" value="1">  -->
        <input type="hidden" name="shippingAddressPostalCode" value="<?php echo str_ireplace('-','',$cliente[0]->cep);?>">  
        <input type="hidden" name="shippingAddressStreet" value="<?php echo $cliente[0]->rua;?> ">  
        <input type="hidden" name="shippingAddressNumber" value="">  
        <input type="hidden" name="shippingAddressComplement" value="<?php echo $cliente[0]->complemento;?>">  
        <input type="hidden" name="shippingAddressDistrict" value="<?php echo $cliente[0]->bairro;?>">  
        <input type="hidden" name="shippingAddressCity" value="<?php echo $cliente[0]->cidade;?>">  
        <input type="hidden" name="shippingAddressState" value="<?php echo $cliente[0]->estado;?>">  
        <input type="hidden" name="shippingAddressCountry" value="BRA">  

        <!-- submit do form (obrigatório) -->  
        <input type="image" name="submit"  style="display:none;" src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/pagamentos/120x53-pagar.gif"   alt="Pague com PagSeguro"/>  




        <?php
    } else {
        return false;
    }
} else {
    return false;
}
?>
   