
<div id="resultado">
    <div class="col-sm-12">
        <?php
        echo form_open('', array('id' => 'pesquisa', 'class' => 'costum loginform'));
        echo form_fieldset("Compras");
        echo errors_validacao();
        echo '<div class="row"><div class="col-sm-6">';
        echo form_input(array('name' => 'nome', 'id' => 'nome', "placeholder" => "nome", 'class' => 'form-control'), set_value(''), 'autofocus');
        echo '</div><div class="col-sm-6">';
        echo form_input(array('name' => 'cpf', 'id' => 'cpf', "placeholder" => "CPF", 'class' => 'form-control'), set_value(''));
        echo '</div></div><br/>';
        echo form_submit(array('name' => 'Pesquisar', 'class' => 'btn btn-defalut btn-info', 'style' => 'float:right'), 'Pesquisar');
        echo form_fieldset_close();
        echo form_close();
        ?>
    </div>
    <div class='col-lg-20'>
        <div class="row">
            <div class='col-sm-3'>Id</div>
            <div class='col-sm-3'>E-mail do cliente</div>
            <div class='col-sm-3'>Data de compra</div>
            <div class='col-sm-3'>Status da compra</div>

        </div>
        <div id="resultado" class="row">
            <?php
            if (!empty($compras) && is_array($compras)) {
                foreach ($compras as $compra) {
                    ?>

                    <div class='col-sm-3'><a href="<?php echo base_url('adm/compras/itens/' . $compra->compra); ?>"><?php echo $compra->compra; ?></div>
                    <div class='col-sm-3'><?php echo $compra->email; ?></div>
                    <div class='col-sm-3'><?php echo $compra->data_compra; ?></div>
                    <div class='col-sm-3'><?php
            switch ($compra->status) {
                case 'a':
                    echo 'Aguardando pagamento';
                    break;
                case 'c':
                    echo 'Pagamento cancelado';
                    break;
                case 'p':
                    echo 'Pagamento aprovado';
                    break;
                case 'e':
                    echo 'Pagamento em análise';
                    break;
                default:
                    echo 'Pagamento em aberto';
            }
                    ?></div>
<br class="clear-line">
                <?php
                }
            }
            ?>
        </div>

    </div>
</div>
