<?php

if (isset($tela)) {
    switch ($tela):
        case 'lista':
            if (is_array($tamanhos) && !empty($tamanhos)) {
               ?>
                <p class="text-info"><?php echo $produtos[0]->nome;?>.</p>
                <?php
                
                foreach($tamanhos as $linha){
                    if(is_null($linha->estoque_id)){
                        $id=rand(0,40)."0_";
                        $qtd=0;
     
                    }else{
                        $id=$linha->estoque_id;
                        $qtd=$linha->qtd;
                        
                    }
                ?>
                <div class="form-control no-bord">
                <p class="col-xs-3  form-control">Tamanho da peça: <?php echo $linha->tamanho;?></p>
                <input class="col-xs-3  form-control change-estoque" type="text" id="<?php echo $id;?>" value="<?php echo $qtd;?>" class="change-estoque"/>  
                <input type="hidden" class="produto" value="<?php echo $produtos[0]->id;?>"/>
                <input type="hidden" class="tamanho" value="<?php echo $linha->tamanhos_id;?>"/>
                <input type="hidden" class="tamanho" value="<?php echo $linha->tamanhos_id;?>"/>
                <input type="button" class="col-xs-3  form-control btn btn-primary" value="Cadastrar"/>
                </div>
                <br>
                <?php
                }
                ?>
                
            <?php } else {
                if(!empty($quantidade)){
                    $id=$quantidade[0]->id;
                    $qtd=$quantidade[0]->quantidade;
                }else{
                    $id="_0";
                    $qtd=0;
                }
                ?>
                <p class="text-info"><?php echo $produtos[0]->nome;?>. Este produto é tamanho único.</p>
                <div class="form-control no-bord">
                <p class="col-xs-4  form-control no-bord">Digite  a quantidade que tem em estoque</p>
                <input type="text" class="col-xs-3  form-control change-estoque" id="<?php echo $id;?>" value="<?php echo $qtd;?>" />    
                <input type="hidden" class="produto" value="<?php echo $produtos[0]->id;?>"/>
                <input type="hidden" class="tamanho" value="0"/>
                <input type="button" class="col-xs-3  form-control btn btn-primary" value="Cadastrar"/>
                </div>
                <br>
                <?php

            }
            ?>
                <a href="<?php echo base_url("/adm/estoque/cadastro/{$produtos[0]->id}");?>" class="btn btn-block btn-lg btn-default ">Finalizar procedimento</a>
<?php
              break;
        default:
            echo 'Escolha um modo de cadastro de estoque para exibir';
            break;
    endswitch;
} else {
    echo 'Erro na identificação da operação';
}
?>
