<div id="box-menu">
	<div class="site">
		<div id="body-nome">
			<h1>Seu Perfil</h1>
		</div>
	</div>
</div>
<div class="site page-perfil">
    <?php echo errors_validacao('site'); ?>
    <div class="form-cadastro-clientes">

        <div class="left">
            <p id="title-dados-pessoais" class="abre-accordion">Últimas Compras</p>
            <div class="accordion">
                <?php
                foreach ($compras as $compra):
                   
                    ?>
                    <div class="lista-compras">
                    	<p><?php echo 'Identificação da compra: '.$compra['compra'].' Data da compra: ' . $compra['data_compra'] . ' - Valor da compra: R$' . $compra['valor']; ?></p>
                        <?php
                        if (count($compra['itens'])>0) {
                            foreach ($compra['itens'] as $item):
                                ?>
                                <div>
                                    <a href="<?php echo base_url("roupas-infantis/{$item['id']}"); ?>">
                                        <?php
                                        echo "{$item['produto']}";
                                        echo isset($item['tamanho']) ? ". Tamanho: " . $item['tamanho'] : "";
                                        echo ", quantidade: ".$item['quantidade'];
                                        ?>
                                    </a>
                                </div>
                                <?php
                            endforeach;
                        }
                        ?>
                    </div>
                    <?php
                endforeach;
                ?>
            </div>
        </div>
        <form action="<?php echo base_url('site/perfil'); ?>" method="post" accept-charset="utf-8">

            <div class="right">
                <p id="title-dados-pessoais" class="abre-accordion">Endereço Cadastrado</p>
                    <input id="inpt-rua" type="text" placeholder="Digite sua rua" value="<?php echo isset($rua) ? $rua : ''; ?>" name="rua"/>
                    <input id="inpt-bairro" type="text" placeholder="Digite seu bairro" value="<?php echo isset($bairro) ? $bairro : ''; ?>" name="bairro"/>
                    <input id="inpt-cidade" type="text" placeholder="Digite sua cidade" value="<?php echo isset($cidade) ? $cidade : ''; ?>" name="cidade"/>
                    <input id="inpt-estado" type="text" placeholder="Digite seu estado"  value="<?php echo isset($estado) ? $estado : ''; ?>" name="estado"/>
                    <input id="inpt-cep" type="text" placeholder="Digite seu CEP" value="<?php echo isset($cep) ? $cep : ''; ?>" name="cep" class="cep"/>
                    <input id="inpt-complemento" type="text" placeholder="Algum complemento?" value="<?php echo isset($complemento) ? $complemento : ''; ?>" name="complemento"/>	
                    <input id="inpt-telefone" type="text" placeholder="Telefone" value="<?php echo isset($telefone) ? $telefone : ''; ?>" name="telefone"/>
            </div>
            <div class="left">
                <p id="title-dados-pessoais" class="abre-accordion">Dados Pessoais</p>
               
                    <input id="inpt-nome" type="text" placeholder="Digite seu nome"value="<?php echo isset($nome) ? $nome : ''; ?>" name="nome"/>
                    <input id="inpt-login" type="text" placeholder="Digite seu login" value="<?php echo isset($login) ? $login : ''; ?>" name="login"/>
                    <input id="inpt-email" type="text" placeholder="Digite seu e-mail" value="<?php echo isset($email) ? $email : ''; ?>" name="email"/>
                    <input id="inpt-senha" type="password" placeholder="Digite sua Senha" value="<?php echo isset($senha) ? $senha : ''; ?>" name="senha"/>
                    <input id="inpt-dtanascimento" type="text" placeholder="Sua data de nascimento" value="<?php echo isset($nascimento) ? $nascimento : ''; ?>" class="data" name="data_nascimento"/>
                    <input id="inpt-cpf" type="text" placeholder="Digite seu CPF" value="<?php echo isset($cpf) ? $cpf : ''; ?>" name="cpf" class="cpf"/>
            </div>
            <div class="right">
           <!--           <p id="title-dados-pessoais">Endereço para Entrega</p>
           <div class="accordion">
                     <input id="inpt-rua" type="text" placeholder="Digite sua rua"value="<?php echo isset($rua_entrega) ? $rua_entrega : ''; ?>"/>
                     <input id="inpt-bairro" type="text" placeholder="Digite seu bairro" value="<?php echo isset($bairro_entrega) ? $bairro_entrega : ''; ?>"/>
                     <input id="inpt-cidade" type="text" placeholder="Digite sua cidade" value="<?php echo isset($cidade_entrega) ? $cidade_entrega : ''; ?>"/>
                     <input id="inpt-estado" type="text" placeholder="Digite seu estado"value="<?php echo isset($estado_entrega) ? $estado_entrega : ''; ?>"/>
                     <input id="inpt-cep" type="text" placeholder="Digite seu CEP" value="<?php echo isset($cep_entrega) ? $cep_entrega : ''; ?>"/>
                     <input id="inpt-complemento" type="text" placeholder="Algum complemento?"value="<?php echo isset($complemento_entrega) ? $complemento_entrega : ''; ?>"/>
           </div>
                -->
                <input id="inpt-cadastrar" type="submit" value="Atualizar Dados">	
            </div>
        </form>
    </div>
</div>