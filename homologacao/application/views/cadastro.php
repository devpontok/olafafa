<?php
switch ($tela) {
    case 'usuario':
        echo '<div class="site">
	<div class="form-cadastro-clientes">';
        echo errors_validacao('site');
        echo form_open('site/cadastro');
        echo '<div class="left">
        <p id="title-dados-pessoais">Dados Pessoais</p>';
        echo form_input(array('name' => 'nome', "id"=>"nome","placeholder" => "Digite seu Nome", 'class' => 'form-control', 'value' => ''), 'autofocus');
        echo form_input(array('name' => 'login',"id"=>"login-cadastro", "placeholder" => "Digite seu Login", 'class' => 'form-control', 'value' => ''));
        echo form_input(array('name' => 'email',  "id"=>"email-cadastro","placeholder" => "Digite seu Email", 'class' => 'form-control', 'value' => ''));
        echo form_input(array('name' => 'senha', "placeholder" => "Digite sua Senha", 'class' => 'form-control', 'value' => '', 'type' => 'password'));
        echo form_input(array('name' => 'data_nascimento', "placeholder" => "Digite sua Data de nascimento", 'class' => 'form-control data', 'value' => ''));
        echo form_input(array('name' => 'cpf', "id"=>"cpf-cadastro", "placeholder" => "Digite seu CPF", 'class' => 'form-control cpf', 'value' => ''));
        echo '</div><div class="right">';
        echo '<p id="title-dados-pessoais">Informações do seu endereço</p>';
        echo form_input(array('name' => 'tel', "placeholder" => "Digite seu Telefone", 'class' => 'form-control tel', 'value' => ''));
        echo form_input(array('name' => 'rua', "placeholder" => "Digite sua Rua", 'class' => 'form-control', 'value' => ''));
        echo form_input(array('name' => 'bairro', "placeholder" => "Digite seu Bairro", 'class' => 'form-control', 'value' => ''));
        echo form_input(array('name' => 'cidade', "placeholder" => "Digite seu Cidade", 'class' => 'form-control', 'value' => ''));
        echo form_input(array('name' => 'estado', "placeholder" => "Digite seu Estado", 'class' => 'form-control', 'value' => ''));
        echo form_input(array('name' => 'cep', "placeholder" => "Digite seu CEP", 'class' => 'form-control cep', 'value' => ''));
        echo form_input(array('name' => 'complemento', "placeholder" => "Digite o Complemento", 'class' => 'form-control', 'value' => ''));
        echo ' <label class="radio rigth col-md-2">
            <input type="radio" name="aceite" id="aceite" value="1" data-toggle="radio" >
            Aceito termos de uso
          </label>';
        echo '<a href="'.base_url("termosdeservico").'" target="__blank">Termos de Uso</a>';
        echo form_submit(array('name' => 'cadastro', 'class' => 'btn btn-defalut btn-info', 'style' => 'float:right'), 'Cadastro');
     //   echo form_fieldset_close();
        echo '</div></div></div>';
        echo form_close();
        break;
     case 'disabled':
        echo form_open('usuarios/cadastro');
        echo form_fieldset('Cadastro de usuários');
        echo errors_validacao();
        echo form_input(array('name' => 'nome', "placeholder" => "Nome", 'class' => 'form-control', 'value' => $nome,'disabled'=>"disabled"), 'autofocus');
        echo form_input(array('name' => 'login', "placeholder" => "Login", 'class' => 'form-control', 'value' => $login,'disabled'=>"disabled"));
        echo form_input(array('name' => 'email', "placeholder" => "Email", 'class' => 'form-control', 'value' => $email,'disabled'=>"disabled"));
        echo form_input(array('name' => 'data_nascimento', "placeholder" => "Data de nascimento", 'class' => 'form-control', 'value' => $data_nascimento,'disabled'=>"disabled"));
        echo form_input(array('name' => 'cpf', "placeholder" => "CPF", 'class' => 'form-control', 'value' => $cpf,'disabled'=>"disabled"));
        echo form_input(array('name' => 'rua', "placeholder" => "Rua", 'class' => 'form-control', 'value' => $rua,'disabled'=>"disabled"));
        echo form_input(array('name' => 'bairro', "placeholder" => "Bairro", 'class' => 'form-control', 'value' =>$bairro,'disabled'=>"disabled"));
        echo form_input(array('name' => 'cidade', "placeholder" => "Cidade", 'class' => 'form-control', 'value' => $cidade,'disabled'=>"disabled"));
        echo form_input(array('name' => 'estado', "placeholder" => "Estado", 'class' => 'form-control', 'value' => $estado,'disabled'=>"disabled"));
        echo form_input(array('name' => 'cep', "placeholder" => "CEP", 'class' => 'form-control', 'value' => $cep,'disabled'=>"disabled"));
        echo form_input(array('name' => 'complemento', "placeholder" => "Complemento", 'class' => 'form-control', 'value' => $complemento,'disabled'=>"disabled"));
        echo ' <label class="radio rigth col-md-2">
            <input type="radio" name="aceite" id="aceite" value="1" '.$optin.' data-toggle="radio" ,disabled="disabled">
            Aceitou os termos de uso
          </label>';
       
        echo form_fieldset_close();
        echo form_close();
        break;
    case 'admin':
        echo form_open('usuarios/cadastro');
        echo form_fieldset('Cadastro de usuários');
        echo errors_validacao();
        echo form_input(array('name' => 'login', "placeholder" => "Nome", 'class' => 'form-control', 'value' => ''));
        echo form_input(array('name' => 'email', "placeholder" => "Email", 'class' => 'form-control', 'value' => ''));
        echo form_input(array('name' => 'senha', "placeholder" => "Senha", 'class' => 'form-control', 'value' => '', 'type' => 'password'));
        echo form_submit(array('name' => 'cadastro', 'class' => 'btn btn-defalut btn-info', 'style' => 'float:right'), 'Cadastro');
        echo form_fieldset_close();
        echo form_close();
    default:
        break;
}
?>
