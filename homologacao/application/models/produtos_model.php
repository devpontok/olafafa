<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');

class produtos_model extends CI_Model {

    public function get_productsByDestaques() {
        $this->db->where(array('destaques' => 1));

        $this->db->join('categorias_tb', 'categorias_tb.id=produtos_tb.categorias_tb_id', 'left');
        $this->db->join('galeria', 'galeria.produtos_tb_id=produtos_tb.id', 'left');
        $this->db->from('produtos_tb');
        $this->db->select('produtos_tb.id as produto_id,produtos_tb.nome as produto,categorias_tb.alias as categoria,galeria.arquivo as imagem');

        $this->db->order_by('Rand()');
        $this->db->order_by('principal desc');
        $destaques = $this->db->get()->result();

        return $destaques;
    }

    public function get_productsByCategoria($id = NULL) {
        if (!is_null($id)) {
            $this->db->where(array('categorias_tb_id' => $id));
            $this->db->join('galeria', 'galeria.produtos_tb_id=produtos_tb.id', 'left');
            # $this->db->where('principal',1);
            $this->db->group_by('produtos_tb.id');
            $this->db->from('produtos_tb');
            $this->db->select("produtos_tb.id as id,produtos_tb.valor,produtos_tb.nome,produtos_tb.peso,produtos_tb.detalhes,produtos_tb.categorias_tb_id,produtos_tb.itens_id,galeria.principal,galeria.arquivo");
            $produtos = $this->db->get()->result();

            if ($produtos) {
                return $produtos;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function get_productsById($id = NULL) {
        if (!is_null($id)) {
            $this->db->select('produtos_tb.*,FLOOR(produtos_tb.peso) as peso_gramas');
            $this->db->where(array('produtos_tb.id' => $id));
            $produtos = $this->db->get('produtos_tb')->result();
    
            if ($produtos) {
                return $produtos;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function do_cadastro($nome = NULL, $valor = NULL, $peso = NULL, $detalhes = NULL, $destaques, $categoria = NULL) {

        $retorno = FALSE;

        if (!is_null($nome) && !is_null($valor) && !is_null($categoria)) {
            $data = array('nome' => $nome, 'valor' => number_format($valor, 2), 'categorias_tb_id' => $categoria);
            if (!is_null($peso)) {
                $data['peso'] = number_format($peso, 2);
            }
            if (!is_null($detalhes)) {
                $data['detalhes'] = $detalhes;
            }

            if (!is_null($destaques)) {
                $data['destaques'] = ($destaques == 'on') ? 1 : 0;
            }
          

            $str = $this->db->insert_string('produtos_tb', $data);
            if ($this->db->query($str)) {
                $retorno = $this->db->insert_id();
            }
        } else {
            if (is_null($nome)) {
                $retorno .= 'Por favor informe o nome do produto<br/>';
            }
            if (is_null($valor)) {
                $retorno .= 'Por favor informe o preço do produto<br/>';
            }
            if (is_null($categoria)) {
                $retorno .= 'Por favor informe a categoria do produto<br/>';
            }
        }
        return $retorno;
    }

    public function do_update($nome = NULL, $valor = NULL, $peso = NULL, $detalhes = NULL, $destaques = NULL, $categoria = NULL, $id = NULL) {
        $retorno = FALSE;

        if (!is_null($nome) && !is_null($valor) && !is_null($categoria) && !is_null($id)) {
            $data = array('nome' => $nome, 'valor' => number_format($valor, 2), 'categorias_tb_id' => $categoria);

            $where = array("id" => $id);
            if (!is_null($peso)) {
                $data['peso'] = number_format($peso, 2);
            }
            if (!is_null($detalhes)) {
                $data['detalhes'] = $detalhes;
            }
            if (!is_null($destaques)) {
                $data['destaques'] = ($destaques == 'on') ? 1 : 0;
            }

            $str = $this->db->update_string('produtos_tb', $data, $where);
            $retorno = $this->db->query($str);
        } else {
            if (is_null($nome)) {
                $retorno .= 'Por favor informe o nome do produto<br/>';
            }
            if (is_null($valor)) {
                $retorno .= 'Por favor informe o preço do produto<br/>';
            }
            if (is_null($categoria)) {
                $retorno .= 'Por favor informe a categoria do produto<br/>';
            }
        }
        return $retorno;
    }

    public function get_all($where = NULL, $limite = NULL, $offset = NULL) {
        $this->db->order_by('produtos_tb.nome desc');

        if (!is_null($where)) {
            $this->db->where = $where;
        }
        if (!is_null($limite)) {
            $this->db->limit = $limite;
        }
        if (!is_null($offset)) {
            $this->db->offset = $offset;
        }
        return $this->db->get('produtos_tb')->result();
    }

    Public function delete($id = NULL) {
        if (!is_null($id)) {
            if ($this->db->query("delete from proidutos_tb where id='$id';")) {
                return 'Produto removido com sucesso';
            } else {
                return 'Ocorreu um problema na exclusão do produto';
            }
        } else {
            return 'O produto não foi identificado';
        }
    }

}

/*
 * End of file produtos_model.php
 * Location: application/models/produtos_model.php
 */
?>
