<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');

class Usuarios_model extends CI_Model {

    //login_tb
    public function do_login($user = NULL, $senha = NULL) {
        if ($user && $senha) {
            $this->db->where('email', $user);
            $this->db->where('senha', $senha);
            $this->db->where('optin', 1);
            $this->db->from('login_tb');
            $this->db->join('cliente_tb', 'cliente_tb.id=login_tb.cliente_tb_id', 'inner');
            $query = $this->db->get();

            if ($query) {
                if ($query->num_rows == 1) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        } else {
            return FALSE;
        }
    }

    public function do_login_adm($user = NULL, $senha = NULL) {
        if ($user && $senha) {
            $this->db->where('login', $user);
            $this->db->where('senha', $senha);
            $this->db->from('acesso_tb');
            $query = $this->db->get();

            if ($query) {
                if ($query->num_rows == 1) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        } else {
            return FALSE;
        }
    }

    public function get_bylogin($login = NULL, $senha = NULL) {
        if ($login != NULL && $senha != NULL) {
            $this->db->from('login_tb');
            $this->db->join('cliente_tb', 'cliente_tb.id=login_tb.cliente_tb_id', 'inner');
            $this->db->where('email', $login);
            $this->db->where('senha', $senha);
            $this->db->limit(1);
            return $this->db->get();
        } else {
            return FALSE;
        }
    }

    public function get_bylogin_adm($login = NULL) {
        if ($login != NULL) {
            $this->db->where('login', $login);
            $this->db->limit(1);
            return $this->db->get('acesso_tb');
        } else {
            return FALSE;
        }
    }

    public function get_all($where = NULL, $limite = NULL, $offset = NULL) {
        $this->db->order_by('nome desc');
        $this->db->join('login_tb', 'login_tb.cliente_tb_id=cliente_tb.id');
        if (!is_null($where)) {
            $this->db->where = $where;
        }
        if (!is_null($limite)) {
            $this->db->limit = $limite;
        }
        if (!is_null($offset)) {
            $this->db->offset = $offset;
        }
        return $this->db->get('cliente_tb')->result();
    }

    public function do_cadastro($nome = NULL, $senha = NULL, $email = NULL, $login = NULL, $data_nascimento = NULL, $cpf = NULL, $rua = NULL, $bairro = NULL, $cidade = NULL, $estado = NULL, $cep = NULL, $complemento = NULL, $aceite = NULL, $tel = NULL) {
        $data = array('nome' => $nome, 'data_nascimento' => $data_nascimento, 'cpf' => $cpf, 'rua' => $rua, 'bairro' => $bairro, 'cidade' => $cidade, 'estado' => $estado, 'cep' => $cep, 'complemento' => $complemento, 'optin' => $aceite, 'telefone' => $tel);
        $str = $this->db->insert_string('cliente_tb', $data);
        if ($this->db->query($str)) {
            $cliente_id = $this->db->insert_id();
            $data = array('senha' => $senha, 'email' => $email, 'login' => $login, 'cliente_tb_id' => $cliente_id);
            $str = $this->db->insert_string('login_tb', $data);

            if ($this->db->query($str)) {

                return $cliente_id;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function do_cadastro_adm($senha = NULL, $email = NULL, $login = NULL) {
        $data = array('login' => $email, 'senha' => $senha, 'nome' => $login);
        $str = $this->db->insert_string('aceso_tb', $data);
        return ($this->db->query($str));
    }

    public function get_byId($id = NULL) {
        if (!is_null($id)) {
            $this->db->where('cliente_tb.id', $id);
            $this->db->from('cliente_tb');
            $this->db->join('login_tb', 'login_tb.cliente_tb_id=cliente_tb.id');
            return $this->db->get()->result();
        } else {
            return FALSE;
        }
    }

    public function do_update($id = NULL, $nome = NULL, $senha = NULL, $email = NULL, $login = NULL, $data_nascimento = NULL, $cpf = NULL, $rua = NULL, $bairro = NULL, $cidade = NULL, $estado = NULL, $cep = NULL, $complemento = NULL, $tel = NULL) {
        if (!is_null($id)) {
            //update de usuário
            $data = array();
            if (isset($nome) && !is_null($nome))
                $data['nome'] = $nome;
            if (isset($data_nascimento) && !is_null($data_nascimento))
                $data['data_nascimento'] = $data_nascimento;
            if (isset($cpf) && !is_null($cpf))
                $data['cpf'] = $cpf;
            if (isset($rua) && !is_null($rua))
                $data['rua'] = $rua;
            if (isset($bairro) && !is_null($bairro))
                $data['bairro'] = $bairro;
            if (isset($cidade) && !is_null($cidade))
                $data['cidade'] = $cidade;
            if (isset($estado) && !is_null($estado))
                $data['estado'] = $estado;
            if (isset($cep) && !is_null($cep))
                $data['cep'] = $cep;
            if (isset($complemento) && !is_null($complemento))
                $data['complemento'] = $complemento;
            if (isset($tel) && !is_null($tel))
                $data['telefone'] = $tel;
            $where=array('id' => $id);
            $str = $this->db->update_string('cliente_tb', $data, $where);
            return($this->db->query($str)!=FALSE)?'Dados atualizados com sucesso':"Tivemos problemas com a atualização deste cadastro";
            //update de dados de acesso do usuário
            $data = array();
            if (isset($senha) && !is_null($senha))
                $data['senha'] = $senha;
            if (isset($email) && !is_null($email))
                $data['email'] = $email;
            if (isset($login) && !is_null($login))
                $data['login'] = $login;
            $where=array('cliente_tb_id' => $id);
            $str = $this->db->update_string('cliente_tb', $data, $where);
            return ($this->db->query($str)!=FALSE)?'Login atualizado com sucesso':"Tivemos problemas com a atualização dos dados de login de cadastro";
        }else {
            return "Usuário não identificado";
        }
    }

}

/*
 * End of file usuarios_model.php
 * Location: application/models/usuarios_model.php
 */
?>
