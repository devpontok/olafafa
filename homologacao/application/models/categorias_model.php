<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');

class categorias_model extends CI_Model {
    public function get_categoriasPai(){
        $retorno='';
        $this->db->where(array('pai_id'=>NULL));
        $this->db->limit(5);
        $query=$this->db->get('categorias_tb');
 
        return $query->result();
    }
    public function get_byAlias($alias=NULL){
        if(isset($alias) && $alias!=NULL){
            $this->db->where(array('alias'=>$alias));
            $query=$this->db->get('categorias_tb');
            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }
    }
        public function get_byId($id=NULL){
        if(isset($id) && $id!=NULL){
            $this->db->where(array('id'=>$id));
            $query=$this->db->get('categorias_tb');
            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }
    }
    public function get_categoriasFilhas($id=NULL){
        if(isset($id) && $id!=NULL){
            $this->db->where(array('pai_id'=>$id));
            $query=$this->db->get('categorias_tb');
            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
            
        }else{
            return FALSE;
        }
    }
      public function get_all($where=NULL,$limite=NULL,$offset=NULL) {
        $this->db->order_by('nome desc');
        if(!is_null($where)){
            $this->db->where=$where;
        }
        if(!is_null($limite)){
            $this->db->limit=$limite;
        }
        if(!is_null($offset)){
            $this->db->offset=$offset;
        }
        return $this->db->get('categorias_tb')->result();
    }
    public function do_cadastro($nome=NULL,$categoria=NULL,$alias=NULL){
        if(!is_null($nome) && !is_null($categoria) &&!is_null($alias)){
            $data['nome']=$nome;
            $data['pai_id']=$categoria;
            $data['alias']=$alias;
            $str=$this->db->insert('categorias_tb',$data);
            
        }else{
            return FALSE;
        }
    }
        
}

/*
 * End of file categorias_model.php
 * Location: application/models/categorias_model.php
 */
?>
