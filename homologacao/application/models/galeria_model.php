<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');

class galeria_model extends CI_Model {

    public function do_cadastro($img = NULL, $produto = NULL, $principal = NULL) {
        $data = array();
        $insert = true;
        if (!is_null($img)) {
            $data['arquivo'] = $img;
        } else {
            $insert = false;
        }
        if (!is_null($produto)) {
            $data['produtos_tb_id'] = $produto;
        } else {
            $insert = false;
        }
        if (!is_null($principal)) {
            $data['principal'] = $principal;
        } else {
            $data['prinicpal'] = 0;
        }
        if ($insert == true) {

            $insert = $this->db->insert_string('galeria', $data);

            return $this->db->query($insert);
        }
    }

    public function do_update($img = NULL, $produto = NULL, $principal = NULL) {
        $data = array();
        $insert = true;
        if (!is_null($img)) {
            $data['arquivo'] = $img;
        } else {
            $insert = false;
        }
        if (!is_null($produto)) {
            $where="produtos_tb_id= {$produto} and principal='1'";
        } else {
            $insert = false;
      }
      
            $data['principal'] = 1;

        if ($insert == true) {
            $insert = $this->db->update_string('galeria', $data,$where);

            return $this->db->query($insert);
        }
    }
    public function get_all(){
        return $this->db->get('galeria')->result();
    }
    public function delete($id=NULL){
        if(!is_null($id)){
            $str="delete from galeria where produtos_tb_id={$id} and principal=1";
            return $this->db->query($str);
        }ELSE{
            return false;
        }
    }
    public function do_delete($imagem=NULL){
        if(!is_null($imagem)){
            $str="delete from galeria where arquivo like '{$imagem}'";
            return $this->db->query($str);
        }else{
            return FALSE;
        }
    }
}

/*
 * End of file galeria_model.php
 * Location: application/models/galeria_model.php
 */
?>
