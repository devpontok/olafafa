<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class estoque extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_painel();
        //$this->load->model('estoque_model', 'categoria');
        $this->conteudo = $this->load->view('adm/menu_view', '', TRUE);
    }

    public function cadastro($produto = NULL) {
        If (!is_null($produto)) {
            $this->load->model('produtos_model', 'produtos');
            $dados['produtos'] = $this->produtos->get_productsById($produto);
            if ($dados['produtos'] != FALSE) {
                $dados['tela'] = 'lista';
                $this->db->select("produtos_tb.id, produto_tamanho.id as produto_tamanho,tamanhos.nome as tamanho,tamanhos.id as tamanhos_id,estoque_tb.id as estoque_id, estoque_tb.quantidade as qtd");
                $this->db->join('produto_tamanho', 'produto_tamanho.produtos_tb_id=produtos_tb.id');
                $this->db->join('tamanhos', 'produto_tamanho.tamanhos_id=tamanhos.id');
                $this->db->join('estoque_tb', 'estoque_tb.produtos_tb_id=produtos_tb.id and estoque_tb.tamanhos_id=tamanhos.id', 'left');
                $this->db->where(array('produtos_tb.id' => $produto));
                $dados['tamanhos'] = $this->db->get('produtos_tb')->result();
                if (empty($dados['tamanhos'])) {
                    $this->db->where(array('produtos_tb_id' => $produto));
                    $dados['quantidade'] = $this->db->get('estoque_tb')->result();
                }
            } else {
                $this->conteudo = "Este produto não existe";
            }
            $this->conteudo.=$this->load->view("adm/estoque_view", $dados, true);
        } else {
            $this->conteudo.='Determine o produto a ser cadastrado';
        }
        set_tema('conteudo', $this->conteudo);
        load_template();
    }

    public function add() {
        $valor = $this->input->post('valor', true);
        $id = $this->input->post('id', true);
        $produto = $this->input->post('produto', true);
        $tamanho = $this->input->post('tamanho', true);
        if (is_null($valor) && is_null($id) && is_integer($valor) && $valor < 0 && is_integer($produto) && $produto < 0 && is_integer($tamanho) && $tamanho < 0) {
            echo 'Valores inválidos';
        } else {
            $itens = explode('_', $id);
            if (count($itens) == 2) {
                //adiciona item  
                $data = array("produtos_tb_id" => $produto,
                    "quantidade" => $valor);
                if ($tamanho != 0) {
                    $data["tamanhos_id"] = $tamanho;
                }
                $str = $this->db->insert_string('estoque_tb', $data);
                echo $this->db->query($str);
            } else {
                //update item
                    $data[""]= $valor;
                    $where='id='.$id;
                    $str ="update estoque_tb set quantidade='{$valor}' where {$where}";
                    echo  $this->db->query($str);
            }
        }
    }
}

    /*
     * End of file estoque.php
     * Location: application/controllers/estoque.php
     */
    ?>

