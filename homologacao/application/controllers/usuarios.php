<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Usuarios extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_painel();
        $this->load->model('usuarios_model', 'usuarios');
    }

    public function index() {
//  $this->load->view("");
    }

//Login de clientes
    public function login() {
        $this->form_validation->set_rules('usuario', 'Usuário', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('senha', 'Senha', "trim|required|min_length[4]|strtolower");
        if ($this->form_validation->run() == TRUE):
            $usuario = $this->input->post('usuario', TRUE);
            $senha = md5($this->input->post('senha', TRUE));
            if ($this->usuarios->do_login($usuario, $senha)) {
                $query = $this->usuarios->get_bylogin($usuario)->row();
                $dados = array(
                    'user_id' => $query->id,
                    'user_nome' => $query->nome,
                    'user_logado' => true
                );

                $this->session->set_userdata($dados);
                if (isset($_POST['ajax'])) {
                    echo true;
                    return;
                }
                redirect('adm/painel');
            } else {
                echo 'login falhou';
            }
        endif;
        set_tema('titulo', 'login');
        set_tema('conteudo', load_modulo('usuarios', 'login'));
        set_tema('rodape', '');
//carregar o módulo de usuários e mostrar tela de login
        load_template();
    }

//carregar login de adm
    public function login_adm() {
        $this->form_validation->set_rules('usuario', 'Usuário', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('senha', 'Senha', "trim|required|min_length[4]|strtolower");


        if ($this->form_validation->run() == TRUE):
            $usuario = $this->input->post('usuario', TRUE);
            $senha = md5($this->input->post('senha', TRUE));

            if ($this->usuarios->do_login_adm($usuario, $senha)) {

                $query = $this->usuarios->get_bylogin($usuario);
                $dados = array(
                    'user_id' => $query->id,
                    'user_nome' => $query->nome,
                    'user_logado' => true,
                    'user_adm' => true
                );

                $this->session->set_userdata($dados);
                redirect('adm/painel');
            } else {
                echo 'login falhou';
            }
        endif;
        set_tema('titulo', 'login_adm');
        set_tema('conteudo', load_modulo('usuarios', 'login_adm'));
        set_tema('rodape', '');
//carregar o módulo de usuários e mostrar tela de login
        load_template();
    }

    public function pesquisar() {
        $nome = $this->input->post('nome', TRUE);
        $cpf = $this->input->post('cpf', TRUE);
        if ($nome != '') {
            $this->db->like('nome', $nome);
        }
        if ($cpf != '') {
            $this->db->where('nome', $cpf);
        }
        $dados['users'] = $this->usuarios->get_all();
        # echo var_dump($dados);
        echo $this->load->view('adm/clientes_view', $dados, true);
    }

    public function login_ajax() {
        $this->form_validation->set_rules('login', 'Usuário', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('senha', 'Senha', "trim|required|min_length[4]|strtolower");
        if ($this->form_validation->run() == TRUE):
            
            $usuario = $this->input->post('login', TRUE);
            $senha = md5($this->input->post('senha', TRUE));
            $this->load->model('usuarios_model', 'usuarios');
            $this->db->where('optin', 1);
            $query = $this->usuarios->get_bylogin($usuario, $senha)->row();
            
            if ($query != FALSE) {
                $dados = array(
                    'user_id' => $query->id,
                    'user_nome' => $query->nome,
                    'user_logado' => true
                );
                $this->session->set_userdata($dados);
            } else {
                echo 'login falhou';
          }
        endif;
    }

    public function deslogar() {
        $this->session->sess_destroy();
    }

    public function cadastro() {
        $this->load->library('session');
      
            $nome = $this->input->post('nome', TRUE);
            $senha = md5($this->input->post('senha', TRUE));
            $email = $this->input->post('email', TRUE);
            $login = $this->input->post('login', TRUE);
            $data_nascimento = $this->input->post('data_nascimento', TRUE);
            $cpf = $this->input->post('cpf', TRUE);
            $rua = $this->input->post('rua', TRUE);
            $bairro = $this->input->post('bairro', TRUE);
            $cidade = $this->input->post('cidade', TRUE);
            $estado = $this->input->post('estado', TRUE);
            $cep = $this->input->post('cep', TRUE);
            $complemento = $this->input->post('complemento', TRUE);
            $aceite = $this->input->post('aceite', TRUE);

            $this->load->model('usuarios_model', 'usuarios');

            $query = $this->usuarios->do_cadastro($nome, $senha, $email, $login, $data_nascimento, $cpf, $rua, $bairro, $cidade, $estado, $cep, $complemento, $aceite);
            if ($query != FALSE) {
                $dados = array(
                    'user_id' => $query,
                    'user_nome' => $nome,
                    'user_logado' => true
                );

                $this->session->set_userdata($dados);

                redirect('');
            } else {
                echo 'Cadastro falhou';
            }

        $dados['tela'] = 'usuario';
        $conteudo = $this->load->view('cadastro', $dados, true);
        set_tema('conteudo', $conteudo);
        load_template();
    }

}

/*
 * End of file sistema.php 
 * Location: Application/controllers/usuarios.php
 */
