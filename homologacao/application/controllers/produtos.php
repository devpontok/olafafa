<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class produtos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_site();
    }

    public function index() {
        set_tema('footerinc', (load_js('http://gs-libs.googlecode.com/files/jquery.gs-innerzoom.min.js', 'js', TRUE)), false);
        set_tema('footerinc', load_js('funcao'), false);
        set_tema('footerinc', load_js('zoom'), false);
        $conteudo = "";
        $segs = $this->uri->segment_array();
        if (!is_numeric($segs[count($segs)])) {
            set_tema('conteudo', '<h1>O produto não foi localizado</h1>');
        } else {
            $id = $segs[count($segs)];
            $conteudo = '';
            $dados = array();
            unset($segs[1]);
//Montar dados de produto
            $this->load->model('produtos_model', 'produto');
            $this->db->join('estoque_tb', 'estoque_tb.produtos_tb_id=produtos_tb.id');
            $this->db->where(array('quantidade >' => '0'));
            $produto = $this->produto->get_productsById($id);
            if ($produto != false) {
                $dados['imagens'] = array();


                $this->load->model('categorias_model', 'categoria');
                $dados['produto'] = $produto[0];
                if (count($segs) == 1) {
//pegar categorias do produto
                    $cat = $this->categoria->get_byId($dados['produto']->categorias_tb_id);
                    $cat2 = '';
                    $url = $cat[0]->alias;
                    if (!is_null($cat[0]->pai_id)) {
                        $cat2 = $this->categoria->get_byId($cat[0]->pai_id);
                        $url.='/' . $cat2[0]->alias;
                    }
                    $url = 'roupas-infantis/' . $url . "/{$id}";
                    redirect(base_url($url));
                } else {

                    $cat = $this->categoria->get_byAlias($segs[2]);
                }
                $dados['categoria'] = $cat[0];
                $dados['filhos'] = $this->categoria->get_categoriasFilhas($dados['categoria']->id);
                if ($dados['filhos']) {
                    $dados['sem_titulo'] = true;
                    $conteudo.= $this->load->view('submenu_view', $dados, TRUE);
                }
                set_tema('headerinc', load_css(array('bebe', 'zoom')), FALSE);
                $this->load->model('galeria_model', 'galeria');
                $this->db->where('produtos_tb_id', $id);
                $this->db->order_by('principal', 'desc');
                $dados['imagens'] = $this->galeria->get_all();

                /* obter tamanhos disponíveis */
                $this->db->where(array('produto_tamanho.produtos_tb_id' => $id));
                $this->db->join('tamanhos', 'tamanhos.id=produto_tamanho.tamanhos_id');
                $this->db->join('estoque_tb', 'estoque_tb.produtos_tb_id=produto_tamanho.produtos_tb_id and estoque_tb.tamanhos_id=produto_tamanho.tamanhos_id');
                $this->db->where(array('estoque_tb.quantidade >' => '0'));
                $this->db->order_by('tamanhos.id asc');
                $dados['tamanhos'] = $this->db->get('produto_tamanho')->result();

                /* fim dos tamanhos disponiveis */

                $conteudo.=$this->load->view('produtos_view', $dados, TRUE);
            } else {
                $conteudo = '<p id="error-perfil" >sem dados para este produto</p>';
            }
        }

        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function adiciona_carrinho() {

        if (!isset($_SESSION))
            session_start();
//tratamento de dados enviados
        $tamanhos = array();

        if ($this->input->post('1', TRUE)) {
            array_push($tamanhos, array('1', 1));
        }
        if ($this->input->post('2', TRUE)) {
            array_push($tamanhos, array('2', 1));
        }
        if ($this->input->post('3', TRUE)) {
            array_push($tamanhos, array('3', 1));
        }
        if ($this->input->post('4', TRUE)) {
            array_push($tamanhos, array('4', 1));
        }
        if ($this->input->post('5', TRUE)) {
            array_push($tamanhos, array('5', 1));
        }
        if ($this->input->post('6', TRUE)) {
            array_push($tamanhos, array('6', 1));
        }

        if ($this->input->post('7', TRUE)) {
            array_push($tamanhos, array('7', 1));
        }
        if ($this->input->post('8', TRUE)) {
            array_push($tamanhos, array('8', 1));
        }
        if ($this->input->post('9', TRUE)) {
            array_push($tamanhos, array('9', 1));
        }
        if ($this->input->post('10', TRUE)) {
            array_push($tamanhos, array('10', 1));
        }
        if ($this->input->post('11', TRUE)) {
            array_push($tamanhos, array('11', 1));
        }
        if ($this->input->post('12', TRUE)) {
            array_push($tamanhos, array('12', 1));
        }
        if ($this->input->post('13', TRUE)) {
            array_push($tamanhos, array('13', 1));
        }
        if ($this->input->post('14', TRUE)) {
            array_push($tamanhos, array('14', 1));
        }
        if ($this->input->post('15', TRUE)) {
            array_push($tamanhos, array('15', 1));
        }
        if ($this->input->post('16', TRUE)) {
            array_push($tamanhos, array('16', 1));
        }
        if ($this->input->post('17', TRUE)) {
            array_push($tamanhos, array('17', 1));
        }
        if ($this->input->post('18', TRUE)) {
            array_push($tamanhos, array('18', 1));
        }
        if ($this->input->post('19', TRUE)) {
            array_push($tamanhos, array('19', 1));
        }
        if ($this->input->post('20', TRUE)) {
            array_push($tamanhos, array('20', 1));
        }
        if ($this->input->post('21', TRUE)) {
            array_push($tamanhos, array('21', 1));
        }
        if ($this->input->post('22', TRUE)) {
            array_push($tamanhos, array('22', 1));
        }
        if ($this->input->post('23', TRUE)) {
            array_push($tamanhos, array('23', 1));
        }
        if ($this->input->post('24', TRUE)) {
            array_push($tamanhos, array('24', 1));
        }
        if ($this->input->post('25', TRUE)) {
            array_push($tamanhos, array('25', 1));
        }
        if ($this->input->post('26', TRUE)) {
            array_push($tamanhos, array('26', 1));
        }
        if ($this->input->post('27', TRUE)) {
            array_push($tamanhos, array('27', 1));
        }
        if ($this->input->post('28', TRUE)) {
            array_push($tamanhos, array('28', 1));
        }
        if ($this->input->post('29', TRUE)) {
            array_push($tamanhos, array('29', 1));
        }
        if ($this->input->post('30', TRUE)) {
            array_push($tamanhos, array('30', 1));
        }
        if ($this->input->post('31', TRUE)) {
            array_push($tamanhos, array('31',1));
        }
        if ($this->input->post('32', TRUE)) {
            array_push($tamanhos, array('32',1));
        }
        if ($this->input->post('33', TRUE)) {
            array_push($tamanhos, array('33',1));
        }
        if ($this->input->post('34', TRUE)) {
            array_push($tamanhos, array('34',1));
        }
        //tamanho[0]=tamanho_id
        //tamanho[1]=qtd de itens deste tamanho
        $id = $this->input->post('id', TRUE);
        $data = array(
            'id' => $id,
            'qty' => $this->input->post('qtd', TRUE),
            'price' => $this->input->post('valor', TRUE),
            'name' => $this->input->post('nome', TRUE),
            'peso' => $this->input->post('peso', TRUE),
            'tamanhos' => $tamanhos
        );
//recuperar dados da sessão$tamanhos
        if (isset($_SESSION['carrinho'])) {
            $carrinho = $_SESSION['carrinho'];
//tratamento de adição de valores
            $adicionou = false;
            $cont = 0;
            foreach ($carrinho as $item) {
                if ($item['id'] == $id) {
                    $carrinho[$cont] = $data;
                    $adicionou = true;
                }
                $cont++;
            }
            if ($adicionou == false) {
                array_push($carrinho, $data);
            }
            $_SESSION['carrinho'] = $carrinho;
        } else {
            $_SESSION['carrinho'][0] = $data;
            $carrinho = $_SESSION['carrinho'];
        }
        $_SESSION['itens'] = 0;
        $_SESSION['total'] = 0;
        /*
          foreach ($carrinho as $item) {
          $dados_itens = count($item['tamanhos']) > 0 ? count($item['tamanhos']) : 1;
          $_SESSION['itens']+=$dados_itens;
          $_SESSION['total']+=($dados_itens * $item['price']);
          } */
        $this->determina_itens_total_carrinho();
        echo $_SESSION['total'] . '-' . $_SESSION['itens'];
    }

    public function remove_item_carrinho() {

        //remover tamanho de item do carrinho
        //não existe carrinho
        if (!isset($S_SESSION) || !isset($_SESSION['carrinho'])) {
            echo false;
        } else {
            //existe carrinho - obter carrinho
            $carrinho = $_SESSION['carrinho'];
            //busca elemento no carrinho
            $id = $this->input->post('id');
            $tamanho = $this->input->post('tamanho');
            foreach ($carrinho as $item) {
                if ($item['id'] == $id) {
                    //tamanho[0]=tamanho_id
                    //tamanho[1]=qtd de itens deste tamanho
                    $tamanhos = $item['tamanhos'];
                    $cont = 0;
                    foreach ($tamanhos as $tamanho) {
                        $cont++;
                    }
                }
            }
        }

        //item existe no carrinho?
        //buscar remover item do carrinho
    }

    //manipula via ajax a quantidade de itens do carrinho
    public function aumenta_item_carrinho() {
        //obter os itens do carrinho
        $produto = $this->input->post('produto_id');
        $qtd = $this->input->post('quantidade');
        $tamanho_recebido = $this->input->post('tamanho_id');
        if (!is_null($tamanho_recebido) && !is_null($produto) && !is_null($qtd)) {
            if (!isset($_SESSION)) {
                session_start();
                //     echo "Carrinho vazio";
            } else {
                if (empty($_SESSION['carrinho']) || !isset($_SESSION['carrinho'])) {
                    //       echo "Carrinho vazio";
                } else {
                    $carrinho = $_SESSION['carrinho'];
                    $cont = array_keys($carrinho);
                    foreach ($carrinho as $item) {
                        if ($item['id'] == $produto) {
                            foreach ($cont as $count) {
                                $contador = 0;
                                //adição de itens com múltiplos tamanhos
                                if (count($carrinho[$count]['tamanhos']) > 0) {
                                    foreach ($item['tamanhos'] as $tamanho) {
                                        //buscar tamanho a atualizar
                                        //tamanho[0]=tamanho_id
                                        //tamanho[1]=qtd de itens deste tamanho
                                        if ($tamanho[0] == $tamanho_recebido) {
                                            //achou tamanho, atualiza quantidade do item no carrinho
                                            if ($qtd != 0) {
                                                if (count($carrinho[$count]['tamanhos']) > 0) {
                                                    $carrinho[$count]['tamanhos'][$contador]['1'] = $qtd;
                                                } else {
                                                    $carrinho[$count]['qty'] = $qtd;
                                                }
                                            } else {
                                                if (count($carrinho[$count]['tamanhos']) > 0) {
                                                    if (count($carrinho[$count]['tamanhos']) == 1) {
                                                        unset($carrinho[$count]);
                                                    } else {
                                                        unset($carrinho[$count]['tamanhos'][$contador]);
                                                    }
                                                } else {
                                                    unset($carrinho[$count]);
                                                }
                                            }
                                            $_SESSION['carrinho'] = $carrinho;
                                            $this->determina_itens_total_carrinho();
                                            echo $_SESSION['total'] . '-' . $_SESSION['itens'];
                                        }
                                        $contador++;
                                    }
                                    //fim da adição de itens com múltiplos tamanhos
                                } else {
                                    //erro
                                    if ($carrinho[$count]['id'] == $produto) {
                                        if ($qtd == 0) {
                                            unset($carrinho[$count]);
                                        } else {
                                            $carrinho[$count]['qty'] = $qtd;
                                        }
                                        $_SESSION['carrinho'] = $carrinho;
                                        $this->determina_itens_total_carrinho();
                                        echo $_SESSION['total'] . '-' . $_SESSION['itens'];
                                    }
                                }
                            }
                        }
                    }
                }
                //            echo 'carrinho atualizado com sucesso';
            }
        } else {
            echo "Faltam dados para modificar o carrinho";
        }
    }

    private function determina_itens_total_carrinho() {
        $_SESSION['itens'] = 0;
        $_SESSION['total'] = 0;
        $carrinho = $_SESSION['carrinho'];
        foreach ($carrinho as $item) {
            //   echo count($item['tamanhos']);
            if (count($item['tamanhos']) == 0) {
                $_SESSION['itens']+=(int) $item['qty'];
                $_SESSION['total']+=((int) $item['qty'] * (float) $item['price']);
            } else {
                if (is_array($item['tamanhos']) && count($item['tamanhos']) > 0) {
                    foreach ($item['tamanhos'] as $tamanhos) {
                        $_SESSION['itens']+=(int) $tamanhos['1'];
                        $_SESSION['total']+=((int) $tamanhos['1'] * (float) $item['price']);
                    }
                }
            }
        }
    }

    public function tem_tamanhos() {
        $id = $this->input->post('id');
        if (!is_null($id)) {
            $this->db->where(array('produtos_tb_id' => $id));
            $dados = $this->db->get('produto_tamanho')->result();
            echo count($dados);
        } else {
            echo 'Não pode verificar produto';
        }
    }

    public function destroi_carrinho() {
        unset($_SESSION['carrinho']);
        unset($_SESSION['total']);
        unset($_SESSION['itens']);
    }

}

/*
 * End of file produtos.php
 * Location: application/controllers/produtos.php
 */
?>

