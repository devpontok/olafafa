<?php
if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
function notificationPost() {
    $postdata = 'Comando=validar&Token=' . 'A735233311754A138DB551C679161C29';
    foreach ($_POST as $key => $value) {
        $valued = clearStr($value);
        $postdata .= "&$key=$valued";
    }
    return verify($postdata);
}

function clearStr($str) {
    if (!get_magic_quotes_gpc()) {
        $str = addslashes($str);
    }
    return $str;
}
function verify($data) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, "https://pagseguro.uol.com.br/pagseguro-ws/checkout/NPI.jhtml");
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_TIMEOUT, 20);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $result = trim(curl_exec($curl));
    curl_close($curl);
    return $result;
}

?>