<!DOCTYPE html>

<head>
	<meta charset="UTF-8">
	<title>Bebê</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>	

<!--INICIO DO CABEÇALHO-->

	<header id="cabecalho">
		<div class="site">
			<span id="nuvem-esquerda">
				<img src="imagens/nuvem.png" id="esquerda">
			</span>
			<div id="box-login">
					<p>Login</p>
					<input id="login" placeholder=" e-mail">
					<input id="senha" placeholder=" senha">
				<div id="btn-login">
					<img src="imagens/btn-login.png">
				</div> 
			</div>
			<span id="nuvem-direita">
				<img src="imagens/nuvem.png" id="esquerda">
			</span>
		</div>
	</header>

	<div id="faixa-cabecalho">
		<div class="site">
			<!--Links do cabeçalho para animação-->
			<div id="login-cadastro">
				<a href="">Login</a>
				<a href="">Cadastre-se</a>	
			</div>
			<div id="marca">
				<a href="index.html"><img src="imagens/marca.png"></a>
			</div>

			<div id="boas-vindas">
				<p id="ola">olá</p>
				<p>Fabíola! Seja bem vinda.</p>
				<a href="">Sair</a>
			</div>
			<div id="container-sacola">
				<div id="sacola">
					<img src="imagens/sacola.png">
					<hr>
					<p >0 ITENS</p>
					<hr>
					<P>R$ 0,00</P>
				</div>
			</div>	
		</div>		
	</div>

	<div id="box-menu-two">
	<div class="site">
		<nav id="menu-bebe">
			<ul>
				<div id="borda-menu-bebe">
					<a class="menu-efeito" id="bebe" href="bebe.html">Bebê</a>
					<a class="menu-efeito" href="default">Menino</a>
					<a class="menu-efeito" href="default">Menina</a>
					<a class="menu-efeito" href="default">Sapato</a>
					<a class="menu-efeito" href="default">Acessórios</a>
				</div>
			</ul>
		</nav>
		<h1 id="body-nome">Quem Somos</h1>
	</div>
</div>
		<div class="site quem-somos-page">
		<p id="title-dados-pessoais">Olá fafá!</p>
		<p>
			É um e-commerce multimarcas  para os pequenos cheios de estilo.
			Trazemos facilidade e comodidade para o dia-a-dia dos pais que buscam produtos diferenciados para os pequenos de 0 a 6 anos.<br>
			Com uma proposta divertida e um olhar apurado para tudo que é bacana no mundo infantil, montamos um mix de produtos descolados de marcas do Brasil e de fora.<br>
			Fazemos uma seleção criteriosa de todos os nossos parceiros, prezando sempre pelo conforto, qualidade e originalidade de cada produto.<br>
			Queremos que as nossas crianças nunca percam a espontaneidade e a alegria de viver!<br>

		</p>

		</div>
	</div>
</div>

	<footer id="rodape">
		<div id="rodape-topo"></div>
		<div id="rodape-baixo">
			<div class="site">	
				<div class="creditos">
					<ul>
						<h2>Sobre nós</h2>
						<li><a href="">Quem Somos</a></li>
						<li><a href="">Política de Privacidade</a></li>
					</ul>
				</div>
				<div class="creditos">
					<ul>
						<h2>Política</h2>
						<li><a href="">Termos e Condições</a></li>
						<li><a href="">Pagamento</a></li>
					</ul>
				</div>
				<div class="creditos">
					<ul>
						<h2>Contato</h2>
						<li><a href="">contato@olafafa.com.br</a></li>
					</ul>
				</div>
			
				<div class="creditos">
					<div id="redes-sociais">
						<a target="_blank" href=" http://www.facebook.com.br"><img src="imagens/btn-face.png"></a>
						<a target="_blank" href="https://twitter.com/" ><img src="imagens/btn-twitter.png"></a>
					</div>				
				</div>
				<div id="cadastro">
					<p>Cadastre-se e receba nossas novidades!</p>
					<input id="email" placeholder=" e-mail">
					<input id="botao" type="button" value="Ok">			
				</div>
				<div class="creditos">
					<img src="imagens/cartoes.png">
				</div>
				<div id="linha"></div>
				<div id="direito-de-copia"><p>Copyright &copy 2013 Olá Fafá. Todos os direitos reservados. CNPJ: XXXXXX-XX</p></div>
				<div id="logo-k">
					<img src="imagens/logo-k.png">
				</div>
			</div>		
		</div>
	</footer>

</body>				