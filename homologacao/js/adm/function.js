/*                                                                                                                                      * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    //local
   var host="http://olafafa.com/homologacao/";
    //online
  //  var host="http://localhost/olafafa-marco/";
    if($('p').attr('class')==='btn-danger'){
        $('.tooltip-inner').addClass('btn-danger');
        $('.tooltip-arrow').attr('style','border-top-color:#e74c3c !important');
    }
    //máscara de valor
    $('.valor').blur(function(){
        var txt=$(this).val();
        if(txt.indexOf(',',0)>-1){
            txt=txt.replace(".","");
            txt=txt.replace(",",".");
            $(this).val(txt);
        }
       
    });
    $("select").selectpicker({
        style: 'btn-hg btn-primary', 
        menuStyle: 'dropdown-inverse'
    });

    //Frete grátis, determinar valor para todo site
    $('.frete').change(function(){
        var funcao='frete_pago_todos';
        var txt='Nenhum produto do site será oferecido com frete gratuito';
        if($(this).attr('checked')=='checked'){
            funcao='frete_gratis_todos';
            txt='Todos os produtos do site serão oferecidos com frete gratuito';
        }
        $.ajax({
            url:host+'adm/produtos/'+funcao,
            success:function(){
                alert(txt);
                location.reload();
            }
        });
    });

    //remover o frete de produto
    $('.remove-frete-produto').click(function(){
        var remove=$(this).attr('id');
         $.ajax({
                url:host+'adm/produtos/remove_frete_gratis',
                data:'id='+remove,
                type:'POST',
                success:function(){
                    alert('Este produto não será mais oferecido com frete gratuito');
                    location.reload();
                }
            });
    });
    //frete na página do produto
    $(".frete_gratis").change(function(){
        var funcao='remove_frete_gratis';
        var id=$(this).attr('id');
        var txt='Este produto não será mais oferecido com frete gratuito';
        if($(this).attr('checked')=='checked'){
            funcao='adiciona_frete_gratis';
            txt='Este produto será oferecido com frete gratuito';
        }
        $.ajax({
            url:host+'adm/produtos/'+funcao,
            data:'id='+id,
            type:'POST',
            success:function(){
                alert(txt);
                location.reload();
            }
        });
    });
    //submit do form de pesquisa de usuários
    
    $("#pesquisa").submit(function(){
        var nome=$("#nome").val();
        var cpf=$('#cpf').val();
        $.ajax({
            url:host+"/usuarios/pesquisar",
            data:'nome='+nome+'&cpf='+cpf,
            type:'POST',
            success:function(e){
                $('#resultado').html(e);
                return false;
            },
            error:function(e){
                
                return false;
            }
        });
        return false;
        
    });
    $("#buscar_produtos").submit(function(){

        var nome=$("#nome").val();
        var categoria=$('#categoria').find(':selected').val();

        $.ajax({
            
            url:host+"/adm/produtos/pesquisar",
            data:'nome='+nome+'&categoria='+categoria,
            type:'POST',
            success:function(e){
                $('#resultado').html(e);
		    $(".apaga-produto").click(function(){
        var identifica=$(this).attr('id');
        var id=identifica.substr(0,identifica.length-1);
        var confirma=confirm("Deseja remover o produto "+id+"?");
        if(confirma==true){
            $.ajax({
                url:host+'/adm/produtos/apaga_produto',
                type:'POST',
                data:'id='+id,
                success:function(e){
                    
                }
            });
        }
    });
                return false;
            },
            error:function(e){
                
                return false;
            }
        });
        return false;
        
    });
    $('#un-login').click(function(){

        $.ajax({
            url:host+"/usuarios/deslogar",
            success:function(e){
                window.location.reload();
            }
        });
    });
    $('#destalhes').blur(function(){
        if($('#destalhes').val().length>600){
            alert('O texto de detalhes do produto está maior que 600 caracteres');
        }
    });
    $('.apagar').click(function(){
        var id=$(this).attr('id');
        var confirma=confirm("Deseja remover a imagem "+id+"?");
        //   alert(id);
        if(confirma==true){
            $.ajax({
                url:host+"adm/produtos/apagarSlider",
                data:"id="+id,
                type:'POST',
                success:function(e){
                    window.location.reload();
                }
            });
            return false;
        //window.location.reload(true);
        } 
    });
    $('.remove').click(function(){
        var id=$(this).attr('id');
        var confirme=confirm("Deseja excluiir esta imagem? "+id);

        if(confirme==true){
            $.ajax({
                data:'id='+id,
                url:host+"/adm/produtos/delete_imagem",
                type:"POST",
                success:function(e){
                    window.location.reload(true);
                }
            })
        }
    });
    $(".radio").click(function(){
        var imagem=$(this).attr('id');

        if(imagem==null){

        //return false;
        }else{
            $.ajax({
                url:host+"/adm/produtos/troca_principal",
                type:"POST",
                data:"imagem="+imagem/*,
            success:function(e){
                alert(e);
            }*/
            });
        }
    });
    $(".apaga-produto").click(function(){
        var identifica=$(this).attr('id');
        var id=identifica.substr(0,identifica.length-1);
        var confirma=confirm("Deseja remover o produto "+id+"?");
        if(confirma==true){
            $.ajax({
                url:host+'/adm/produtos/apaga_produto',
                type:'POST',
                data:'id='+id,
                success:function(e){
                    
                }
            });
        }
    });
    $(".btn-primary").click(function(){
        var id=$(this).parents('div').find('.change-estoque').attr('id');
        var val=$(this).parents('div').find('.change-estoque').val();
        var produto=$(this).parents('div').find('.produto').val();
        var tamanho=$(this).parents('div').find('.tamanho').val();
        $.ajax({
            url:host+"adm/estoque/add",
            data:'id='+id+"&valor="+val+'&tamanho='+tamanho+"&produto="+produto,
            type:"POST",
            success:function(e){
                if(e==1){
                    alert("Cadastro realizado com sucesso");
                }else{
                    alert("Algo deu errado no cadastro, atualize a página e tente novamente");
                }
            }/*,error:function(xhr){
	    
	      alert(xhr.status);
	    }*/
        })
    });
});