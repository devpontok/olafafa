<?php
if (strpos($_SERVER['REQUEST_URI'], 'localhost') >= 0 || strpos($_SERVER['REQUEST_URI'], 'homologacao') >= 0) {
//URL de testes
    $pdo = new PDO('mysql:host=dbmy0044.whservidor.com;dbname=olafafa', 'olafafa', 'Brunoa12');
    //token de testes
    define('TOKEN', '4E81907FAF5746FE9AF1994D605FCF65');
} else {
    $pdo = new PDO('mysql:host=dbmy0038.whservidor.com;dbname=olafafa_1', 'olafafa_1', 'Brunoa12');
    define('TOKEN', 'A735233311754A138DB551C679161C29');
}

$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

header('Content-Type: text/html; charset=ISO-8859-1');

class PagSeguroNpi {

    private $timeout = 20; // Timeout em segundos

    public function notificationPost() {
        $postdata = 'Comando=validar&Token=' . TOKEN;
        foreach ($_POST as $key => $value) {
            $valued = $this->clearStr($value);
            $postdata .= "&$key=$valued";
        }
        return $this->verify($postdata);
    }

    private function clearStr($str) {
        if (!get_magic_quotes_gpc()) {
            $str = addslashes($str);
        }
        return $str;
    }

    private function verify($data) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://pagseguro.uol.com.br/pagseguro-ws/checkout/NPI.jhtml");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = trim(curl_exec($curl));
        curl_close($curl);
        return $result;
    }

    public function atualiza_estoque($idcompra) {
        if (strpos($_SERVER['REQUEST_URI'], 'localhost') >= 0 || strpos($_SERVER['REQUEST_URI'], 'homologacao') >= 0) {
            $pdo = new PDO('mysql:host=dbmy0044.whservidor.com;dbname=olafafa', 'olafafa', 'Brunoa12');
        } else {
            $pdo = new PDO('mysql:host=dbmy0038.whservidor.com;dbname=olafafa_1', 'olafafa_1', 'Brunoa12');
        }
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $tr = "select quantidade,compras_tb_id,produtos_tb_id,tamanhos_id from itens where compras_tb_id='{$idcompra}';";
        
        //Log de quantidade de itens antes de comprar
        $stmt = $pdo->prepare("insert pagseguro (dados)values(:dados);");
        $stmt->bindParam(':dados', $tr);
        $stmt->execute();

        // if (count($itens) > 0) {
        foreach ($pdo->query($tr) as $item) {

            $prod = $item['produtos_tb_id'];
            $tamanho = $item['tamanhos_id'];
            foreach ($pdo->query("select quantidade,id from estoque_tb where produtos_tb_id='{$prod}' and tamanhos_id='{$tamanho}';") as $dados) {

                $qtd = $dados['quantidade'] - $item['quantidade'];

                $update = $pdo->prepare("update estoque_tb set quantidade=':quantidade' where id=':tabela_id';");
                $update->bindParam(':quantidade', $qtd);
                $update->bindParam(':tabela_id', $item['id']);
                echo $update->execute();

                 //Log de quantidade de itens após de comprar
                $stmt = $pdo->prepare("insert pagseguro (dados)values({$qtd} - {$item['id']});");
                $stmt->execute();
            }
        }
        //}
    }

}

if (count($_POST) > 0) {
    // POST recebido, indica que é a requisição do NPI.
    $npi = new PagSeguroNpi();
    $result = $npi->notificationPost();
    if (strpos($_SERVER['REQUEST_URI'], 'localhost') >= 0 || strpos($_SERVER['REQUEST_URI'], 'homologacao') >= 0) {
        $pdo = new PDO('mysql:host=dbmy0044.whservidor.com;dbname=olafafa', 'olafafa', 'Brunoa12');
    } else {
        $pdo = new PDO('mysql:host=dbmy0038.whservidor.com;dbname=olafafa_1', 'olafafa_1', 'Brunoa12');
    }
    $dado='';
    foreach ($_POST as $item => $value) {
        $dado.=$item.' -> '.$value.'<br/>';
    }
    $stmt = $pdo->prepare("insert pagseguro (dados)values(:dados);");
    $stmt->bindParam(':dados', $dado);
    $stmt->execute();
    $transacaoID = isset($_POST['TransacaoID']) ? $_POST['TransacaoID'] : '';

    if ($result == "VERIFICADO") {

        $StatusTransacao = isset($_POST['StatusTransacao']) ? $_POST['StatusTransacao'] : '';

        switch ($StatusTransacao) {
            case 'Completo':
                $status = 'o';
                //  $npi->atualiza_estoque($_POST['Referencia']);
                break;
            case 'Aguardando Pagto':
                $status = 'a';
                break;
            case 'Aprovado':
                $status = 'p';
                //atuallizar estoque
                $npi->atualiza_estoque($_POST['Referencia']);
                break;
            case 'Em Análise':
                $status = 'e';
                break;
            case 'Cancelado':
                $status = 'c';
                break;
        }

        $Referencia = isset($_POST['Referencia']) ? $_POST['Referencia'] : '';
        $stmt = $pdo->prepare("UPDATE compras_tb SET status=:status WHERE id=:idtabela;");
        $stmt->bindParam(':idtabela', $Referencia);
        $stmt->bindParam(':status', $status);
        $stmt->execute();
    } else if ($result == "FALSO") {
        //O post não foi validado pelo PagSeguro.
        echo 'post falso';
    } else {
        //Erro na integração com o PagSeguro.
        echo ($result);
        echo 'erro na integração';
    }
} else {
    // POST não recebido, indica que a requisição é o retorno do Checkout PagSeguro.
    // No término do checkout o usuário é redirecionado para este bloco.
    ?>
    <html>
    <head>
        <meta lang="pt-br"></meta>
        <meta charset="utf-8"></meta>
        <link rel="shortcut icon" href="<?php echo "./imagens/favicon.png";?>" type="image/x-icon"/>
         <link rel="stylesheet" type="text/css" href="./css/main.css" media="all"/>
        <title>Ol&aacute; Faf&aacute;</title>
    </head>
    <body>
      
<!--INICIO DO CABEÇALHO-->

    <header id="cabecalho">
        <div class="site">
            <span id="nuvem-esquerda">
                <img src="<?php echo './imagens/nuvem.png';?>" id="esquerda">
            </span>
            <div id="box-login">
                    <form action="#" id="logar">
                    <p>Login</p>
                    <input id="login" name="login" type="text" placeholder=" e-mail">
                    <input id="senha" name="senha" type="password" placeholder=" senha">
                    <input type="submit" id="btn-login" value=''/>
                     
                    </form>
                </div>
            <span id="nuvem-direita">
                <img src="<?php echo './imagens/nuvem.png';?>" id="esquerda">
            </span>
        </div>
    </header>

<!--FAIXA DO CABEÇALHO-->
    
    <div id="faixa-cabecalho">
        <div class="site">
            <div id="marca">
                <a href="./"><img src="<?php echo './imagens/marca.png';?>"></a>
            </div>
                   <?php if(!isset($_SESSION)){
                        session_start();
                    }
                   
                    ?>
            <!--Links do cabeçalho para animação-->
                          <div id="boas-vindas">
        <p id="ola">ol&aacute;!</p>
        <ul id="link-perfil">
            <li><a href="<?php echo "./site/perfil";?>">Meu Perfil</a></li>
            <li><a href="#" id="un-login">Sair</a></li>
        </ul>
    </div>
                <div id="container-sacola">
                <div id="sacola">
                    <img src=<?php echo "./imagens/sacola.png";?>>
                    <hr>
                    
                    <p ><label><?php (!empty($_SESSION['itens']))?$_SESSION['itens']:'0';?></label> ITENS</p>
                    <hr>
                    <P>R$ <label><?php echo (!empty($_SESSION['total']))?$_SESSION['total']:'0';?></label></P>
                </div>
            </div>  
                          <div id="resultado"></div>
   
            <nav id="menu">
                <ul>
                    <div id="borda-menu">
                        <a id="bebe" href="<?php echo './categorias/bebe';?>" <?php echo strpos($_SERVER['REQUEST_URI'], "bebe")>0?' class="ativo" ':'';?>>Beb&ecirc;</a>
                            <a id="menino" href="<?php echo './categorias/menino';?>" <?php echo strpos($_SERVER['REQUEST_URI'],"menino")>0?' class="ativo" ':'';?>>Menino</a>
                            <a id="menina" href="<?php echo './categorias/menina';?>" <?php echo strpos($_SERVER['REQUEST_URI'],"menina")>0?' class="ativo" ':'';?>>Menina</a>
                            <a id="sapato" href="<?php echo './categorias/sapato';?>" <?php echo strpos($_SERVER['REQUEST_URI'], "sapato")>0?' class="ativo" ':'';?>>Sapato</a>
                            <a id="acessorios" href="<?php echo './categorias/acessorios';?>" <?php echo strpos($_SERVER['REQUEST_URI'],"acessorios")>0?' class="ativo" ':'';?>>Acess&oacute;rios</a>
                    </div>
                </ul>
            </nav>
                          
        </div>
    </div>

<!--FIM DO CABEÇALHO / INICIO DO CONTEÚDO-->    

        <div id="conteudo">
         <!--   <div class="site">-->
         <br>
                <h2>Obrigado por efetuar a compra.</h2>
            <!--</div>-->
        </div>
        <footer id="rodape">
        <div id="logos">
            <div class="site">
                <img src="./imagens/baner-rodape.png">
            </div>
        </div>
        <div id="rodape-topo"></div>
        <div id="rodape-baixo">
            <div class="site">  
                <div class="creditos">
                    <ul>
                        <h2>Sobre n&oacute;s</h2>
                        <li><a href="./quemsomos">Quem Somos</a></li>
                        <li><a href="./privacidade">Pol&iacute;tica de Privacidade</a></li>
                    </ul>
                </div>
                <div class="creditos">
                    <ul>
                        <h2>Pol&iacute;tica</h2>
                        <li><a href="./termosdeservico">Termos e Condi&ccedil;&oatilde;es</a></li>
                        <li><a href="./pagamento">Pagamento</a></li>
                    </ul>
                </div>
                <div class="creditos">
                    <ul>
                        <h2>Contato</h2>
                        <li><a href="">contato@olafafa.com</a></li>
                    </ul>
                </div>
            
                <div class="creditos" id="redes_sociais">
                    <a href="https://www.facebook.com/olafafa" target="blank"><img src="http://localhost/olafafa/imagens/icon-facebook.png"></a>
                    <a href="http://instagram.com/olafafa#" target="blank"><img src="http://localhost/olafafa/imagens/icon-instagram.png"></a>          
                </div>
                <form action="#" id="cadastro">
                    <p>Cadastre-se e receba nossas novidades!</p>
                    <input id="email" type="email" placeholder=" e-mail" required>
                    <input id="botao" type="submit" value="Ok">         
                </form>
                <div id="linha"></div>
                <div id="direito-de-copia"><p>Copyright &copy 2013 Olá Fafá. Todos os direitos reservados. CNPJ 19.307.950/0001-91</p></div>
                <div id="logo-k">
                    <img src="./imagens/logo-k.png">
                </div>
            </div>      
        </div>
    </footer>
        <script type="text/javascript" src="./js/jquery.js"></script>
        <script type="text/javascript" src="./js/jquery.lazyload.min.js"></script>
        <script type="text/javascript" src="./js/jquery.orbit-1.2.3.min.js"></script>
        <script type="text/javascript" src="./js/mask.js"></script>
        <script type="text/javascript" src="./js/functions.js"></script>
    </body>
</html>
    
    <?php
}
?>