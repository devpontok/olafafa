-- phpMyAdmin SQL Dump
-- version 3.5.3
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 18/11/2013 às 19:38:59
-- Versão do Servidor: 5.5.28-log
-- Versão do PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `mydb`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acesso_tb`
--

CREATE TABLE IF NOT EXISTS `acesso_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(45) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `nome` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabela de gestão de administradores - todo e qualquer admin ' AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `acesso_tb`
--

INSERT INTO `acesso_tb` (`id`, `login`, `senha`, `nome`) VALUES
(1, 'adm@olafafa.com.br', '81dc9bdb52d04dc20036dbd8313ed055', 'Josy');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias_tb`
--

CREATE TABLE IF NOT EXISTS `categorias_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(20) NOT NULL,
  `pai_id` int(11) DEFAULT NULL,
  `alias` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_categorias_categorias1` (`pai_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Extraindo dados da tabela `categorias_tb`
--

INSERT INTO `categorias_tb` (`id`, `nome`, `pai_id`, `alias`) VALUES
(6, 'Bebê', NULL, 'bebe'),
(7, 'Body', 6, 'body'),
(8, 'Menino', NULL, 'menino'),
(9, 'Menina', NULL, 'menina'),
(10, 'Sapato', NULL, 'sapato'),
(11, 'Acessório', NULL, 'acessorio');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente_tb`
--

CREATE TABLE IF NOT EXISTS `cliente_tb` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `data_nascimento` datetime DEFAULT NULL,
  `cpf` varchar(12) NOT NULL,
  `rua` varchar(45) NOT NULL,
  `bairro` varchar(45) NOT NULL,
  `cidade` varchar(30) NOT NULL,
  `estado` varchar(10) NOT NULL,
  `cep` varchar(10) NOT NULL,
  `optin` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `cpf_UNIQUE` (`cpf`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabela de clientes' AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `cliente_tb`
--

INSERT INTO `cliente_tb` (`id`, `nome`, `data_nascimento`, `cpf`, `rua`, `bairro`, `cidade`, `estado`, `cep`, `optin`) VALUES
(1, 'josy', '2013-11-18 00:00:00', '1', '1', '1', '2', 'BAHIA', '1', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `compras_tb`
--

CREATE TABLE IF NOT EXISTS `compras_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_compra` datetime NOT NULL,
  `valor` float NOT NULL,
  `status` varchar(1) DEFAULT NULL,
  `cliente_tb_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_compras_tb_cliente_tb1` (`cliente_tb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela que identifica compras de um cliente\no status pode se' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `itens`
--

CREATE TABLE IF NOT EXISTS `itens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantidade` int(11) NOT NULL,
  `compras_tb_id` int(11) NOT NULL,
  `produtos_tb_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `item_tb_UNIQUE` (`id`),
  KEY `fk_table1_compras_tb1` (`compras_tb_id`),
  KEY `produtos_tb_id` (`produtos_tb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_tb`
--

CREATE TABLE IF NOT EXISTS `login_tb` (
  `id` int(11) NOT NULL,
  `login` varchar(45) NOT NULL,
  `email` varchar(60) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `cliente_tb_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_login_tb_cliente_tb` (`cliente_tb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela de credenciais de usuários';

--
-- Extraindo dados da tabela `login_tb`
--

INSERT INTO `login_tb` (`id`, `login`, `email`, `senha`, `cliente_tb_id`) VALUES
(0, 'josy', 'josefinasarangel@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `email` varchar(100) DEFAULT NULL,
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_tb`
--

CREATE TABLE IF NOT EXISTS `produtos_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `valor` decimal(10,0) NOT NULL,
  `peso` decimal(10,0) DEFAULT NULL,
  `detalhes` varchar(600) DEFAULT NULL,
  `destaques` tinyint(4) DEFAULT '0',
  `categorias_tb_id` int(11) NOT NULL,
  `itens_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_produtos_tb_categorias_tb1` (`categorias_tb_id`),
  KEY `fk_produtos_tb_itens1` (`itens_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Extraindo dados da tabela `produtos_tb`
--

INSERT INTO `produtos_tb` (`id`, `nome`, `valor`, `peso`, `detalhes`, `destaques`, `categorias_tb_id`, `itens_id`) VALUES
(3, 'body infantil', '45', '0', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum luctus ipsum eu urna pharetra auctor. Proin urna dui, dignissim nec eros sed, dignissim sollicitudin mauris. Donec id fermentum augue, non imperdiet erat. Fusce quis arcu lorem. Sed pellentesque ultrices neque, vel posuere enim hendrerit vitae. Integer in lectus non nibh bibendum congue ac in velit. Maecenas posuere mattis eleifend. Morbi placerat tellus metus, eu lacinia ligula feugiat eu. Praesent iaculis elit id eros ultrices viverra. Aenean vestibulum tristique urna nec vulputate. Vivamus aliquam sapien quis tincidunt dict', 1, 6, NULL),
(4, 'colant de bailarina', '23', '0', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum luctus ipsum eu urna pharetra auctor. Proin urna dui, dignissim nec eros sed, dignissim sollicitudin mauris. Donec id fermentum augue, non imperdiet erat. Fusce quis arcu lorem. Sed pellentesque ultrices neque, vel posuere enim hendrerit vitae. Integer in lectus non nibh bibendum congue ac in velit. Maecenas posuere mattis eleifend. Morbi placerat tellus metus, eu lacinia ligula feugiat eu. Praesent iaculis elit id eros ultrices viverra. Aenean vestibulum tristique urna nec vulputate. Vivamus aliquam sapien quis tincidunt dict', 1, 8, NULL),
(5, 'boné', '15', '0', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum luctus ipsum eu urna pharetra auctor. Proin urna dui, dignissim nec eros sed, dignissim sollicitudin mauris. Donec id fermentum augue, non imperdiet erat. Fusce quis arcu lorem. Sed pellentesque ultrices neque, vel posuere enim hendrerit vitae. Integer in lectus non nibh bibendum congue ac in velit. Maecenas posuere mattis eleifend. Morbi placerat tellus metus, eu lacinia ligula feugiat eu. Praesent iaculis elit id eros ultrices viverra. Aenean vestibulum tristique urna nec vulputate. Vivamus aliquam sapien quis tincidunt dict', 1, 7, NULL),
(6, 'Passadeira', '2', '0', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum luctus ipsum eu urna pharetra auctor. Proin urna dui, dignissim nec eros sed, dignissim sollicitudin mauris. Donec id fermentum augue, non imperdiet erat. Fusce quis arcu lorem. Sed pellentesque ultrices neque, vel posuere enim hendrerit vitae. Integer in lectus non nibh bibendum congue ac in velit. Maecenas posuere mattis eleifend. Morbi placerat tellus metus, eu lacinia ligula feugiat eu. Praesent iaculis elit id eros ultrices viverra. Aenean vestibulum tristique urna nec vulputate. Vivamus aliquam sapien quis tincidunt dict', 1, 11, NULL),
(7, 'macacão', '35', '0', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum luctus ipsum eu urna pharetra auctor. Proin urna dui, dignissim nec eros sed, dignissim sollicitudin mauris. Donec id fermentum augue, non imperdiet erat. Fusce quis arcu lorem. Sed pellentesque ultrices neque, vel posuere enim hendrerit vitae. Integer in lectus non nibh bibendum congue ac in velit. Maecenas posuere mattis eleifend. Morbi placerat tellus metus, eu lacinia ligula feugiat eu. Praesent iaculis elit id eros ultrices viverra. Aenean vestibulum tristique urna nec vulputate. Vivamus aliquam sapien quis tincidunt dict', 1, 8, NULL),
(8, 'sapatinho de bebê em tricô', '56', '0', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum luctus ipsum eu urna pharetra auctor. Proin urna dui, dignissim nec eros sed, dignissim sollicitudin mauris. Donec id fermentum augue, non imperdiet erat. Fusce quis arcu lorem. Sed pellentesque ultrices neque, vel posuere enim hendrerit vitae. Integer in lectus non nibh bibendum congue ac in velit. Maecenas posuere mattis eleifend. Morbi placerat tellus metus, eu lacinia ligula feugiat eu. Praesent iaculis elit id eros ultrices viverra. Aenean vestibulum tristique urna nec vulputate. Vivamus aliquam sapien quis tincidunt dict', 1, 11, NULL),
(9, 'lacinho de cabeça', '2', '0', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum luctus ipsum eu urna pharetra auctor. Proin urna dui, dignissim nec eros sed, dignissim sollicitudin mauris. Donec id fermentum augue, non imperdiet erat. Fusce quis arcu lorem. Sed pellentesque ultrices neque, vel posuere enim hendrerit vitae. Integer in lectus non nibh bibendum congue ac in velit. Maecenas posuere mattis eleifend. Morbi placerat tellus metus, eu lacinia ligula feugiat eu. Praesent iaculis elit id eros ultrices viverra. Aenean vestibulum tristique urna nec vulputate. Vivamus aliquam sapien quis tincidunt dict', 1, 11, NULL),
(10, 'roupa de pagãozinho', '70', '0', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum luctus ipsum eu urna pharetra auctor. Proin urna dui, dignissim nec eros sed, dignissim sollicitudin mauris. Donec id fermentum augue, non imperdiet erat. Fusce quis arcu lorem. Sed pellentesque ultrices neque, vel posuere enim hendrerit vitae. Integer in lectus non nibh bibendum congue ac in velit. Maecenas posuere mattis eleifend. Morbi placerat tellus metus, eu lacinia ligula feugiat eu. Praesent iaculis elit id eros ultrices viverra. Aenean vestibulum tristique urna nec vulputate. Vivamus aliquam sapien quis tincidunt dict', 1, 11, NULL),
(11, 'vestido floral', '45', '0', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum luctus ipsum eu urna pharetra auctor. Proin urna dui, dignissim nec eros sed, dignissim sollicitudin mauris. Donec id fermentum augue, non imperdiet erat. Fusce quis arcu lorem. Sed pellentesque ultrices neque, vel posuere enim hendrerit vitae. Integer in lectus non nibh bibendum congue ac in velit. Maecenas posuere mattis eleifend. Morbi placerat tellus metus, eu lacinia ligula feugiat eu. Praesent iaculis elit id eros ultrices viverra. Aenean vestibulum tristique urna nec vulputate. Vivamus aliquam sapien quis tincidunt dict', 1, 8, NULL);

--
-- Restrições para as tabelas dumpadas
--

--
-- Restrições para a tabela `categorias_tb`
--
ALTER TABLE `categorias_tb`
  ADD CONSTRAINT `fk_categorias_categorias1` FOREIGN KEY (`pai_id`) REFERENCES `categorias_tb` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `compras_tb`
--
ALTER TABLE `compras_tb`
  ADD CONSTRAINT `fk_compras_tb_cliente_tb1` FOREIGN KEY (`cliente_tb_id`) REFERENCES `cliente_tb` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `itens`
--
ALTER TABLE `itens`
  ADD CONSTRAINT `fk_table1_compras_tb1` FOREIGN KEY (`compras_tb_id`) REFERENCES `compras_tb` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `itens_ibfk_1` FOREIGN KEY (`produtos_tb_id`) REFERENCES `produtos_tb` (`id`);

--
-- Restrições para a tabela `login_tb`
--
ALTER TABLE `login_tb`
  ADD CONSTRAINT `fk_login_tb_cliente_tb` FOREIGN KEY (`cliente_tb_id`) REFERENCES `cliente_tb` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `produtos_tb`
--
ALTER TABLE `produtos_tb`
  ADD CONSTRAINT `fk_produtos_tb_categorias_tb1` FOREIGN KEY (`categorias_tb_id`) REFERENCES `categorias_tb` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_produtos_tb_itens1` FOREIGN KEY (`itens_id`) REFERENCES `itens` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
