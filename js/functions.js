$(document).ready(function(){
// var host="http://olafafa.com/homologacao/";            
var host="http://localhost/olafafa/";            
    $('#feature').orbit({
        bullets: true,
        bulletThumbs: true,
        captions: false,
        directionalNav: false, 
        timer: true,
        animate_speed:500
      //timer:false
    });
    $("#fechar-carrinho").click(function(){
    
    	$("#resultado").slideToggle('slow');
    });
    /*
    $("#email-cadastro").blur(function(){
        var email=$(this).val();
        alert(email);
        $.ajax({
            url:host+"site/checa_email",
            data:'email='+email,
            type:'POST',
            success:function(e){
                if(e!==0){
                     alert(e);
                }
            },error:function(e){
                alert(e);
            }
        });
    });
    $("#login-cadastro").blur(function(){
        var login=$(this).val();
       // alert(login);
        $.ajax({
            url:host+"site/checa_login",
            data:'login='+login,
            type:'POST',
            success:function(e){
                if(e!==0){
                     alert(e);
                }
            },error:function(e){
                alert(e);
            }
        });
    });*/
    $('img.lazy-img').lazyload({
	effect : "fadeIn"
    });
    $(".abre-accordion").click(function(){
        $(".accordion").slideToggle('slow');
    });
    $('#show-login').click(function(){
        $('#cabecalho').slideToggle('slow');
    });
   $('#logar').submit(function(){
        $.ajax({
            data:$("#logar").serialize(),
            url:host+'usuarios/login_ajax',
            type:"POST",
            success:function(e){
     
                if(e=='login falhou'){
                    alert('Usuário não identificado');
                }else{
                  window.location.reload();
                }
            } 
        });
        return false;
    });
    $('#un-login').click(function(){

        $.ajax({
            url:host+"site/deslogar",
            success:function(e){
                window.location.reload();
            }
        });
    });
  
    $('#destalhes').blur(function(){
        if($('#destalhes').val().length>600){
            alert('O texto de detalhes do produto está maior que 600 caracteres');
        }
    });
    $("#cadastro").submit(function(){
        var email=$("#cadastro #email").val(); 
        $.ajax({
            type:'POST',
            data:'email='+email,
            url:host+'adm/cliente/cadastro_news',
            success:function(){
                alert('Cadastro reaizado com sucesso, a partir de agora poderá receber nossos informativos');
            }
        })
    })
    $('.galeria img').click(function(){
        var img=$(this).attr('src');
        $("#principal").attr('src',img);
    });
    $(".btn-add-cart").click(function(){

        var checks=$(".jcart > #id").val();
        if($('#resultado').css('display')=='block'){
            $("#resultado").css('display','none');  
            $("#resultado").empty();
        }
        $.ajax({
            url:host+"produtos/tem_tamanhos",
            data:"id="+$(".jcart > #id").val(),
            type:'POST',
            success:function(e){
                if(e>0){
                    var checkeds=$(":checked").val();
                    if(checkeds!='on'){
                        alert('Escolha o tamanho do produto');
                    }else{
                        var checks2="&checks=";
                        var checks=$(':checked');
                        $.each(checks,function(){
                            checks2=checks2+"_"+$(this).attr('id');
                        })
                        $.ajax({ 
                            url:host+'produtos/adiciona_carrinho',
                            data:$('.jcart').serialize()+checks2,
                            type:'POST',
                            success:function(e){
                                console.log(e);
                                var txt=e.split("-");
                                $("#sacola > p > label").first().text(txt[1]);
                                $("#sacola > p > label").last().text(txt[0]);
                            }
                        });  
                        return false;
                    }
                }/*else{
                    $.ajax({ 
                        url:host+'produtos/adiciona_carrinho',
                        data:$('.jcart').serialize(),
                        type:'POST',
                        success:function(e){
                            var txt=e.split("-");
                            $("#sacola > p > label").first().text(txt[1]);
                            $("#sacola > p > label").last().text(txt[0]);

                        }
                    });  
                    return false;
                }*/
            },error:function(xhr, ajaxOptions, thrownError){
                 alert(xhr.status);
                alert(thrownError);
            }
 
        });
        return false;
    
    });
    $('.btn-chekout').click(function(){
        //verifica se está logado
        comprar(host);
    });
   
    $('#sacola img').click(function(){
        var html=$("#resultado").html();
        if(html!=''){
            $("#resultado").slideToggle('slow');
        }else{
            $.ajax({
                url:host+"/carrinho",
                success:function(e){
                    if(html==''){
                        $("#resultado").html(e);
                    }
                    $('.btn-chekout').click(function(){
                        //verifica se está logado
                        comprar(host);
                    });
                    $(".um_quinto :input").change(function(){             
                    
                        if($(this).val()>=0){
                            var unidade=$(this).parents('.item');
                            var id_tamanho=$("#"+unidade.attr('id')+" .um_terco :hidden").val();
                            var produto=unidade.attr('id').split("_");
                            var id_item=produto[0];
                            var qtd=$(this).val();
                            // alert('produto_id='+id_item+'&tamanho_id='+id_tamanho+'&quantidade='+qtd);
                            $.ajax({
                                url:host+"produtos/aumenta_item_carrinho",
                                data:'produto_id='+id_item+'&tamanho_id='+id_tamanho+'&quantidade='+qtd,
                                type: 'POST',
                                success:function(e){
                                    console.log(e);
                                    var valor=e.split('-');
                                    $("#sacola > p > label").first().text(valor[1]);
                                    $("#sacola > p > label").last().text(valor[0]);
                                    $("#total > label").text(valor[0]);
                                    if(qtd==0){
                                        unidade.html('');
                                    }
                                    return false;
                                },
                                error:function(e){
                                    //  alert(e);
                                    return false;
                                }
                            });
                            //comprar();    
                            return false;
                        }else{
                            alert('Desculpe-me, mas não permitimos valores menores que 0 para quantidade de itens');
                        }
                    });
                }
            });
            $("#resultado").slideToggle('slow');
        }
      
         
        $.ajax({
            url:host+"/carrinho",
            success:function(e){
               
                if(html==''){
                    $("#resultado").html(e);
                }
                $('.btn-chekout').click(function(){
                    //verifica se está logado
                    comprar(host);
                });
                $(".um_quinto :input").change(function(){             
                    if($(this).val()>=0){
                        var unidade=$(this).parents('.item');
                        var id_tamanho=$("#"+unidade.attr('id')+" .um_terco :hidden").val();
                        var produto=unidade.attr('id').split("_");
                        var id_item=produto[0];
                        var qtd=$(this).val();
                        
                        // alert('produto_id='+id_item+'&tamanho_id='+id_tamanho+'&quantidade='+qtd);
                        $.ajax({
                            url:host+"produtos/aumenta_item_carrinho",
                            data:'produto_id='+id_item+'&tamanho_id='+id_tamanho+'&quantidade='+qtd,
                            type: 'POST',
                            success:function(e){
                                //alert(e);
                                var valor=e.split('-');
                                $("#sacola > p > label").first().text(valor[1]);
                                $("#sacola > p > label").last().text(valor[0]);
                                $("#total > label").text(valor[0]);
                                var contador;
                                return false;
                            },
                            error:function(e){
                                //  alert(e);
                                return false;
                            }
                        });
                        if($(this).val()==0){ 
                            unidade.remove();
                            var itens=$(".item").length;
                            if(itens==0){
                                $(".carrinho").remove();
                                $("#resultado").empty();
                                $.ajax({
                                    url:host+"/produtos/destroi_carrinho"
                                })
                            }
                        }
                        //comprar();    
                        return false;
                    }else{
                        alert('Desculpe-me, mas não permitimos valores menores que 0 para quantidade de itens');
                    }
                    
                });
            }
        })
    });
  
    $('.data').mask('00/00/0000');
    $('.cep').mask('00000-000');
    $('.cpf').mask('000.000.000-00');
    $('.cpf').blur(function(){
        var ok=valCpf($(this).val());
        if(ok==false){
            alert("O CPF precisa ser válido!");
        }
    });
     
    $(".abre-accordion").click(function(){
        $(".accordion").slideToggle('slow');
    });

});

function comprar(host){
    //verifica se está logado
    $.ajax({
        url:host+"site/logado",
        success:function(e){
            if(e==true){
                $.ajax({
                    url:host+"carrinho/pagseguro",
                    success:function(e){
                        $("#pag").append(e);
                    	$("#pag").submit();
                    }
                })
            }else{
                alert('É preciso efetuar login para finalizar a compra');
            }
        }
    });
}
function renovar_senha(){
    var email=$("#login").val();
    if(email==''){
        alert('Por favor, digite um e-mail cadastrado');
    }else{
        $.ajax({
            url:"./acessos/esqueci_senha",
            data:'email='+email,
            type: "POST", 
            success:function(data){
                window.location(data);
            }
        })
    }
}
function valCpf($cpf){
    $cpf = preg_replace('/[^0-9]/','',$cpf);
    $digitoA = 0;
    $digitoB = 0;
    for($i = 0, $x = 10; $i <= 8; $i++, $x--){
        $digitoA += $cpf[$i] * $x;
    }
    for($i = 0, $x = 11; $i <= 9; $i++, $x--){
        if(str_repeat($i, 11) == $cpf){
            return false;
        }
        $digitoB += $cpf[$i] * $x;
    }
    $somaA = (($digitoA%11) < 2 ) ? 0 : 11-($digitoA%11);
    $somaB = (($digitoB%11) < 2 ) ? 0 : 11-($digitoB%11);
    if($somaA != $cpf[9] || $somaB != $cpf[10]){
        return false;	
    }else{
        return true;
    }
}
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-44895504-2', 'olafafa.com');
  ga('send', 'pageview');
