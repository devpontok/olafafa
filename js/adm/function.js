/*                                                                                                                                      * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    //local
 // var host="http://olafafa.com/homologacao/";
    //online
   var host="http://localhost/olafafa/";
    if($('p').attr('class')==='btn-danger'){
        $('.tooltip-inner').addClass('btn-danger');
        $('.tooltip-arrow').attr('style','border-top-color:#e74c3c !important');
    }
    $('.selectpicker').selectpicker("width:30%");    
    //máscara de valor
    $('.valor').blur(function(){
        var txt=$(this).val();
        if(txt.indexOf(',',0)>-1){
            txt=txt.replace(".","");
            txt=txt.replace(",",".");
            $(this).val(txt);
        }
       
    });
    cadastrar_estoque();
    editar_estoque();
    dados_export();
    //SUBMETER A PROMOÇÃO
    $("#promocoes").submit(function(){
        $("#LoadingImage").show();
        $('#resultado').empty();
        $.ajax({
            url:host+"adm/produtos/listagem_promocao",
            type:'POST',
            data:$("#promocoes").serialize(),
            success:function(dado){
                $("#LoadingImage").hide();
                $('#resultado').html(dado);
            }
        })
        return false;
    });
    //Máscara para campos com classe data
  
  //$('.data').mask('00/00/0000');

   // $("#data_final").datepicker();
  //  $('#data_final').datepicker();
    $('.valor').mask('###0.00', {reverse: true});
 //   $("select").selectpicker({
   //     style: 'btn-hg btn-primary', 
     //   menuStyle: 'dropdown-inverse'
    //});
    //Finalizar promoção
    $('#finalizar').change(function(){
        var status=$(this).attr('checked');
        if(status=='checked'){
            $.ajax({
                url:host+'adm/produtos/finaliza_promocao',
                data:'id='+$('#id').attr('value'),
                type:"POST",
                success:function(dado){
                    alert(dado);
                    if(dado.indexOf('não')>=0){
                        window.location.href=host+'adm/produtos/lista_promocao';
                    }
                }
            })
        }
    });
    $('.remove_promocao').click(function(){
        var status=$(this).attr('class').split(' ');
        var id=status[1];
        $("#LoadingImage").show();
        $.ajax({
            url:host+'adm/produtos/finaliza_promocao',
            data:'id='+id,
            type:"POST",
            success:function(dado){
                alert(dado);
                if(dado=='Promoção foi encerrada com sucesso'){
                    location.reload();
                }
                $("#LoadingImage").hide();
            },
            error:function(data){
                $("#LoadingImage").hide();
            }
        });

    });
    //Frete grátis, determinar valor para todo site
    $('.frete').change(function(){
        var funcao='frete_pago_todos';
        var txt='Nenhum produto do site será oferecido com frete gratuito';
        if($(this).hasClass('checked')){
            funcao='frete_gratis_todos';
            txt='Todos os produtos do site serão oferecidos com frete gratuito';
        }
        $.ajax({
            url:host+'adm/produtos/'+funcao,
            success:function(){
                alert(txt);
                location.reload();
            }
        });
    });
    remover_frete(host);
    //frete na página do produto

    $(".frete_gratis").click(function(event){
        event.preventDefault();
        var funcao='remove_frete_gratis';
        var id=$(this).attr('id');
        var txt='Este produto não será mais oferecido com frete gratuito';
        if(!$(this).hasClass('checked')){
            funcao='adiciona_frete_gratis';
            txt='Este produto será oferecido com frete gratuito';
        }
        $.ajax({
            url:host+'adm/produtos/'+funcao,
            data:'id='+id,
            type:'POST',
            success:function(data){
                alert(txt);
              //  console.log(data);
                location.reload();
            }
        });
    });
    //submit do form de pesquisa de usuários
    
    submit_cliente(host);
    submit_produto(host);
    $('#un-login').click(function(){

        $.ajax({
            url:host+"usuarios/deslogar",
            success:function(e){
                window.location.reload();
            }
        });
    });
    $('#destalhes').blur(function(){
        if($('#destalhes').val().length>600){
            alert('O texto de detalhes do produto está maior que 600 caracteres');
        }
    });
    $('.apagar').click(function(){
        var id=$(this).attr('id');
        var confirma=confirm("Deseja remover a imagem "+id+"?");
        //   alert(id);
        if(confirma==true){
            $.ajax({
                url:host+"adm/produtos/apagarSlider",
                data:"id="+id,
                type:'POST',
                success:function(e){
                    window.location.reload();
                }
            });
            return false;
        //window.location.reload(true);
        } 
    });
    $('.remove').click(function(){
        var id=$(this).attr('id');
        var confirme=confirm("Deseja excluir esta imagem? "+id);

        if(confirme==true){
            $.ajax({
                data:'id='+id,
                url:host+"adm/produtos/delete_imagem",
                type:"POST",
                success:function(e){
                    window.location.reload(true);
                }
            })
        }
    });
    $(".imagem>ul>li>.radio").click(function(){
        var imagem=$(this).attr('id');
        if(imagem==null){

        //return false;
        }else{
            $.ajax({
                url:host+"adm/produtos/troca_principal",
                type:"POST",
                data:"imagem="+imagem,
                success:function(e){
                  //  alert(e);
                }
            });
        }
    });
    $(".alt").change(function(){
        var dados=$(this).attr('class').split(' ');
        var conteudo=$(this).val();
         $.ajax({
                url:host+'adm/produtos/troca_alt',
                type:'POST',
                data:'imagem='+dados[1]+"&conteudo="+conteudo,
                success:function(e){
                    console.log(e);
                }
            });
    });
     $(".title").change(function(){
        var dados=$(this).attr('class').split(' ');
        var conteudo=$(this).val();
         $.ajax({
                url:host+'adm/produtos/troca_title',
                type:'POST',
                data:'imagem='+dados[1]+"&conteudo="+conteudo,
                success:function(e){
                    console.log(e);
                }
            });
    });
    $(".apaga-produto").click(function(){
        var identifica=$(this).attr('id');
        console.log(identifica);
        var id=identifica.substr(0,identifica.length-1);
        var confirma=confirm("Deseja remover o produto "+id+"?");
        if(confirma==true){
            $.ajax({
                url:host+'adm/produtos/apaga_produto',
                type:'POST',
                data:'id='+id,
                success:function(e){
                    console.log(e);
                    if(e!=false && e!=1){
                        alert(e);
                        console.log(e);
                        location.reload();
                    }
                }
            });
        }
    });
    //adicionar produtos a uma promoção
    $('#add-produto-promocao').submit(function(){
        var dado=$('#produtos').val();
        var promocao=document.getElementsByName("promocao")[0].value;
         $("#LoadingImage").show();
        $.ajax({
            url:host+'adm/produtos/insere_na_promocao',
            data:'produtos='+dado+'&promocao='+promocao,
            type:'POST',
            success:function(data){
                $("#LoadingImage").hide();
                alert(data);
                window.location.reload(true);
             
            },
            error:function(data){
                $("#LoadingImage").hide();
                alert('Desculpe, mas algo não funcionou');

            }
        });
        return false;
    });
    $(".remove-produto-promocao").click(function(){
        var itens=$(this).attr('class');
        itens=itens.split(' ');
      //  alert(itens[1]+itens[2]);
       $("#LoadingImage").show();
        $.ajax({
            url:host+"adm/produtos/remove_produto_promocao",
            data:'promocao='+itens[2]+'&produto='+itens[1],
            type: "POST", 
            success:function(data){
                 $("#LoadingImage").hide();
                alert('Produto desvinculado da promoção com sucesso');
                 window.location.reload(true);
            },error:function(data){
                $("#LoadingImage").hide();
            }
        })
    });
    $(".estoque-add").click(function(){
        var id=$(this).parents('div').find('.change-estoque').attr('id');
        var val=$(this).parents('div').find('.change-estoque').val();
        var produto=$(this).parents('div').find('.produto').val();
        var tamanho=$(this).parents('div').find('.tamanho').val();
        $.ajax({
            url:host+"adm/estoque/add",
            data:'id='+id+"&valor="+val+'&tamanho='+tamanho+"&produto="+produto,
            type:"POST",
            success:function(e){
                if(e==1){
                    alert("Cadastro realizado com sucesso");
                }else{
                    alert("Algo deu errado no cadastro, atualize a página e tente novamente");
                }
            }/*,error:function(xhr){
	    
	      alert(xhr.status);
	    }*/
        })
    });
});
/*function closeDialog () {
    $('#windowTitleDialog').modal('hide'); 
};
function okClicked () {
    document.title = document.getElementById ("xlInput").value;
    closeDialog ();
};
*/
function dados_export(){
      //localao
 // var host="http://olafafa.com/homologacao/";
    //online
var host="http://localhost/olafafa/";
    $( "#data-inicial-datepicker" ).datepicker({
        onSelect:function(){
           var data_inicial=$(this).datepicker().val().split('/');
            $("#data_inicial").attr("value",data_inicial[1]+"/"+data_inicial[0]+"/"+data_inicial[2]);
            $("#data-inicial-datepicker").hide();
        },
    });
        
    $('#data_inicial').focus(function(){
        $( "#data-inicial-datepicker" ).show();
    });

    $( "#data-final2-datepicker" ).datepicker({
        onSelect:function(){
           var data_final=$(this).datepicker().val().split('/');
            $("#data_final2").attr("value",data_final[1]+"/"+data_final[0]+"/"+data_final[2]);
            $("#data-final2-datepicker").hide();
        },
    });
        
    $('#data_final2').focus(function(){
        $( "#data-final2-datepicker" ).show();
    });
    $( "#data-inicial2-datepicker" ).datepicker({
        onSelect:function(){
           var data_inicial=$(this).datepicker().val().split('/');
            $("#data_inicial2").attr("value",data_inicial[1]+"/"+data_inicial[0]+"/"+data_inicial[2]);
            $("#data-inicial2-datepicker").hide();
        },
    });
        
    $('#data_inicial2').focus(function(){
        $( "#data-inicial2-datepicker" ).show();
    });

    $( "#data-final-datepicker" ).datepicker({
        onSelect:function(){
           var data_final=$(this).datepicker().val().split('/');
            $("#data_final").attr("value",data_final[1]+"/"+data_final[0]+"/"+data_final[2]);
            $("#data-final-datepicker").hide();
        },
    });
        
    $('#data_final').focus(function(){
        $( "#data-final-datepicker" ).show();
    });
   
    $("#pesquisa_compra").submit(function(){
        var dados=$("#pesquisa_compra").serialize();
        $("#LoadingImage").show();
        $.ajax({
            url:host+'adm/compras/pesquisa',
            data:dados,
            type:"POST",
            success:function(data){
                $("#LoadingImage").hide();
                console.log(data);
                $('#resultado').html(data);
                dados_export();
            },
            error:function(data){
                $("#LoadingImage").hide();
                alert('Erro na conexão com o banco de dados');
            }
        });
        return false;
    });
    $('.caracteristica').change(function(){
        var valor=$(this).val();
        var id=$(this).attr('id');
        $.ajax({
            url:host+"adm/caracteristicas/update_tipo",
            data:'id='+id+'&valor='+valor,
            type: "POST", 
            success:function(data){
                console.log(data);
                if(data==1){
                    alert('Valor modificado com sucesso!');
                }
            },error:function(data){
                console.log(data);
            }
        })
    });
    $("#excel_compras").click(function(){
        $("#LoadingImage").show();
        $.ajax({
            url:host+'adm/compras/imprimir_excel',
            success:function(data){
                $("#LoadingImage").hide();
                if(data!=0){
                    window.location.href=host+data;
                }
                console.log(data);
            },
            error:function(data){
                $("#LoadingImage").hide();
                console.log(data);
                alert('Erro na conexão com o banco de dados');
            }
        })
    });
    $("#pdf_compras").click(function(){
        $("#LoadingImage").show();
        $.ajax({
            url:host+'adm/compras/imprimir_pdf',
            success:function(data){
                $("#LoadingImage").hide();
                if(data!=0){
                     window.open(host+data,'_blank');
                }
              
                console.log(data);
            },
            error:function(data){
                $("#LoadingImage").hide();
                console.log(data);
                alert('Erro na conexão com o banco de dados');
            }

        })
    });

    //liberar produto do cliente
    $('#resultado>div>img').click(function(){
        var dado=$(this).attr('class').split(" ");
         $("#LoadingImage").show();
       $.ajax({
            url:host+"adm/compras/liberar_frete",
            type:'POST',
            data:'id='+dado[1],
            success:function(dado){
                if(dado==1){
                     $("#LoadingImage").hide();
                    alert('O produto foi liberado');
                    location.reload();
                }else{
                    alert('Houve uma falha na comunicação com o banco de dados, tente mais tarde');
                }
            },
            error:function(dado){
                console.log(dado);
            }
       });
    });
}
  

function submit_cliente(host){
    $("#pesquisa").submit(function(){
        var nome=$("#nome").val();
        var cpf=$('#cpf').val();
        $.ajax({
            url:host+"usuarios/pesquisar",
            data:'nome='+nome+'&cpf='+cpf,
            type:'POST',
            success:function(e){
                $('#resultado').html(e);
                submit_cliente(host)
                console.log(e);
                return false;
            },
            error:function(e){
                console.log(e);
                return false;
            }
        });
        return false;
        
    });
}

function submit_produto(host){
    $("#buscar_produtos").submit(function(){

        var nome=$("#nome").val();
        var categoria=$('#categoria').find(':selected').val();

        $.ajax({
            
            url:host+"adm/produtos/pesquisar",
            data:'nome='+nome+'&categoria='+categoria,
            type:'POST',
            success:function(e){
                $('#resultado').html(e);
                submit_produto(host);
                remover_frete(host);
                $(".apaga-produto").click(function(){
                    var identifica=$(this).attr('id');
                    var id=identifica.substr(0,identifica.length-1);
                    var confirma=confirm("Deseja remover o produto "+id+"?");
                    if(confirma==true){
                        $.ajax({
                            url:host+'adm/produtos/apaga_produto',
                            type:'POST',
                            data:'id='+id,
                            success:function(e){
                                 console.log(e);
                                    if(e!=false && e!=1){
                                        alert(e);
                                        console.log(e);
                                        location.reload();
                                    }
                            }
                        });
                    }
                });
                return false;
            },
            error:function(e){
                
                return false;
            }
        });
        return false;
        
    });
}
function remover_frete(host){
    //remover o frete de produto
    $('.remove-frete-produto').click(function(){
        var remove=$(this).attr('id');
         $.ajax({
                url:host+'adm/produtos/remove_frete_gratis',
                data:'id='+remove,
                type:'POST',
                success:function(){
                     remover_frete(host);
                    alert('Este produto não será mais oferecido com frete gratuito');
                    location.reload();
                }
            });
    });
}
function add_Novo_Estoque(host){
    //$('.descricao').append('')
    $.ajax({
        url:host,
        success:function(data){
            $(data).insertAfter('.descricao');
            cadastrar_estoque();
            editar_estoque();
            $('.selectpicker').selectpicker("width:121px");  
            console.log(data);
        }
    });
    console.log('adiciona outro item de cadastro de estoque');
}
function cadastrar_estoque(){
    $('.cadastrar_estoque').click(function(){
        var id=$(this).attr('id');
        var tam=$('.tamanhos_novo_'+id).val();
        var cor=$('.cores_novo_'+id).val();
        var modelo=$('.modelos_novo_'+id).val();
        var quantidade=$('#quantidade_'+id).val();
        var produto=$('.produto').val();
        $.ajax({
            url:'../add',
            type: "POST", 
            data:'tamanho='+tam+'&cor='+cor+'&modelo='+modelo+'&produto='+produto+'&qtd='+quantidade,
            success:function(data){
                if(data!=0){
                    var cadastro= $(".cadastrar_estoque");
                    cadastro.removeClass();
                    cadastro.addClass("col-xs-2  form-control btn btn-primary editar_estoque");
                    cadastro.attr('id',data);
                    var tamanho=$('.tamanhos_novo_'+id);
                    tamanho.removeClass('.tamanhos_novo_'+id);
                    tamanho.attr('id','tamanhos_'+data);
                    tamanho.attr('data-style','btn-primary');
                    var cores=$('.cores_novo_'+id);
                    cores.removeClass('.cores_novo_'+id);
                    cores.attr('id','cores_'+data);
                    cores.attr('data-style','btn-primary');
                    var modelos=$('.modelos_novo_'+id);
                    modelos.removeClass('.modelos_novo_'+id);
                    modelos.attr('id','modelos_'+data);
                    modelos.attr('data-style','btn-primary');
                    var quantidade=$('.quantidade_novo_'+id);
                    quantidade.removeClass('.quantidade_novo_'+id);
                    quantidade.attr('id','quantidade_'+data);
                    quantidade.attr('data-style','btn-primary');
                    editar_estoque();
                    location.reload();
                }else{
                    alert('Encontramos um problema na conexão de dados');
                }
                
                console.log(data);
            }
        })
    })
};
function editar_estoque(){
    $('.editar_estoque').click(function(){
        var id=$(this).attr('id');
        var tam=$('#tamanhos_'+id).val();
        var cor=$('#cores_'+id).val();
        var modelo=$('#modelos_'+id).val();
        var quantidade=$('#quantidade_'+id).val();
        var produto=$('.produto').val();
        
        $.ajax({
            url:'../update',
            type: "POST", 
            data:'tamanho='+tam+'&cor='+cor+'&modelo='+modelo+'&produto='+produto+'&qtd='+quantidade+'&id='+id,
            success:function(data){
                if(data!=0){
                    alert('Dados modificados com sucesso');
                }else{
                    alert('Encontramos um problema na conexão de dados');
                }
                
                console.log(data);
            }
        })
    })
};
