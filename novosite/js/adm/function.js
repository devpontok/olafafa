/*                                                                                                                                      * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    //local
    var host="http://olafafa.com/novosite/";
    //online
    //var host="http://www.kcomunicacao.com.br/olafafa/";
    if($('p').attr('class')==='btn-danger'){
        $('.tooltip-inner').addClass('btn-danger');
        $('.tooltip-arrow').attr('style','border-top-color:#e74c3c !important');
    }
    
    $("select").selectpicker({style: 'btn-hg btn-primary', menuStyle: 'dropdown-inverse'});
    //submit do form de pesquisa de usuários
    $("#pesquisa").submit(function(){
        var nome=$("#nome").val();
        var cpf=$('#cpf').val();
        $.ajax({
            url:host+"/usuarios/pesquisar",
            data:'nome='+nome+'&cpf='+cpf,
            type:'POST',
            success:function(e){
                $('#resultado').html(e);
                return false;
            },
            error:function(e){
                
                return false;
            }
        });
        return false;
        
    });
    $("#buscar_produtos").submit(function(){

        var nome=$("#nome").val();
        var categoria=$('#categoria').find(':selected').val();

        $.ajax({
            
            url:host+"/adm/produtos/pesquisar",
            data:'nome='+nome+'&categoria='+categoria,
            type:'POST',
            success:function(e){
                $('#resultado').html(e);
                return false;
            },
            error:function(e){
                
                return false;
            }
        });
        return false;
        
    });
    $('#un-login').click(function(){

        $.ajax({
            url:host+"/usuarios/deslogar",
            success:function(e){
                window.location.reload();
            }
        });
    });
    $('#destalhes').blur(function(){
        if($('#destalhes').val().length>600){
            alert('O texto de detalhes do produto está maior que 600 caracteres');
        }
    });
    $('.apagar').click(function(){
        var id=$(this).attr('id');
        var confirma=confirm("Deseja remover a imagem "+id+"?");
        if(confirma==true){
            $.ajax({
                sync:'sync',
                url:host+'/adm/produtos/apagarSlider',
                data:'id='+id,
                type:'post'
            });
            window.location.reload(true);
        } 
    });
    $('.remove').click(function(){
        var id=$(this).attr('id');
        var confirme=confirm("Deseja excluiir esta imagem? "+id);

        if(confirme==true){
            $.ajax({
                data:'id='+id,
                url:host+"/adm/produtos/delete_imagem",
                type:"POST",
                success:function(e){
                    window.location.reload(true);
                }
            })
        }
    });
    $(".radio").click(function(){
        var imagem=$(this).attr('id');

        if(imagem==null){

        //return false;
        }else{
            $.ajax({
                url:host+"/adm/produtos/troca_principal",
                type:"POST",
                data:"imagem="+imagem/*,
            success:function(e){
                alert(e);
            }*/
            });
        }
    });
    $(".apaga-produto").click(function(){
        var identifica=$(this).attr('id');
        var id=identifica.substr(0,identifica.length-1);
        var confirma=confirm("Deseja remover o produto "+id+"?");
        if(confirma==true){
            $.ajax({
                url:host+'/adm/produtos/apaga_produto',
                type:'POST',
                data:'id='+id,
                success:function(e){
                   
                        alert(e);
                    
                }
            });
        }
    });
});