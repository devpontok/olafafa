$(document).ready(function(){
    var host="http://olafafa.com/novosite/";            
    $('#feature').orbit({
        bullets: true,
        bulletThumbs: true,
        captions: false,
        directionalNav: false, 
        timer: false  
    });
    $('img.lazy-img').lazyload({
	effect : "fadeIn"
    });
    $('#show-login').click(function(){
        $('#cabecalho').slideToggle('slow');
    });
    $('#logar').submit(function(){
        $.ajax({
            data:$("#logar").serialize(),
            url:host+'usuarios/login_ajax',
            type:"POST",
            success:function(e){
                window.location.reload();
            } 
        });
        return false;
    });
    $('#un-login').click(function(){

        $.ajax({
            url:host+"/site/deslogar",
            success:function(e){
                window.location.reload();
            }
        });
    });
  
    $('#destalhes').blur(function(){
        if($('#destalhes').val().length>600){
            alert('O texto de detalhes do produto está maior que 600 caracteres');
        }
    });
    $("#cadastro").submit(function(){
        var email=$("#cadastro #email").val(); 
        $.ajax({
            type:'POST',
            data:'email='+email,
            url:host+'adm/cliente/cadastro_news',
            success:function(){
                alert('Cadastro reaizado com sucesso, a partir de agora poderá receber nossos informativos');
            }
        })
    })
    $('.galeria img').click(function(){
        var img=$(this).attr('src');
        $("#principal").attr('src',img);
    });
    $(".btn-add-cart").click(function(){
        var id=$("#id").val();
        var qtd=$("#qtd").val();
        var valor=$("#valor").val();
        var nome=$("#nome").val();
        $.ajax({
            url:host+"/produtos/adiciona_carrinho",
            type:"POST",
            data:"nome="+nome+"&id="+id+"&qtd=1"+"&valor="+valor,
            success:function(e){
                window.location.reload(true);
            }
        });
    });
    $('.btn-chekout').click(function(){
      
        //verifica se está logado
        comprar(host);
        
       
    });
    $('#sacola img').click(function(){
        var html=$("#resultado").html();
         
        $("#resultado").slideToggle('slow');
         
        $.ajax({
            url:host+"/carrinho",
            success:function(e){
               
                if(html==''){
                    $("#resultado").html(e);
                }
                $('.btn-chekout').click(function(){
                    //verifica se está logado
                    comprar(host);
                });
                $(".um_quinto :input").change(function(){
                    if($(this).val()>=0){
                        var unidade=$(this).parents('.item');

                        //id do elemento modificado
                        var id=unidade.attr('id');
                        var anterior=$("#"+unidade.attr('id')+" .um_terco label").text();
                    
                        var elemento=$("#"+unidade.attr('id')+" .um_terco .unidade").val();
                        var novo=parseFloat(elemento)*$(this).val();
                        $("#"+unidade.attr('id')+" .um_terco label").text(novo);
                        var label=$("#"+unidade.attr('id')+" .um_terco label").text(parseFloat(elemento)*$(this).val());
                        //nova quantidade do elemento
                        var qtd=(novo-anterior)/elemento;
                        //novo valor total do carrinho
                        var total=parseFloat($("#total label").text())+novo-anterior;
                        //nova quantidade de itens na sessão

                        var quantidade=$('.um_quinto :input');
                        var item=0;

                        for(i=0;i<quantidade.length;i++){
                            item+=parseInt(quantidade[i].value);
                        }

                        var itens=parseInt($("#itens").text());
                        $("#sacola >p >label").first().text(itens+qtd);

                        $("#sacola >p >label").last().text(total);    
                        $("#total label").text(total);
                        //nome do produto a ser modificado na sessão
                    
                        var nome=$("#"+unidade.attr('id')+" .um_terco p").text();
                        $.ajax({
                            url:host+"/produtos/adiciona_carrinho",
                            type:"POST",
                            data:"nome="+nome+"&id="+id+"&qtd="+$(this).val()+"&valor="+elemento+'&total='+total+'&itens='+item/*,
                            success:function(e){
                             //   alert(e);
                            //window.location.reload(true);
                            }*/
                        });
                        comprar();
                       
                    }else{
                        alert('Desculpe-me, mas não permitimos valores menores que 0 para quantidade de itens');
                    }
                });
            }
        })
    });
  
    $('.data').mask('00/00/0000');
    $('.cep').mask('00000-000');
    $('.cpf').mask('000.000.000-00');
    $('.cpf').blur(function(){
        var ok=valCpf($(this).val());
        if(ok==false){
            alert("O CPF precisa ser válido!");
        }
    });
});
function comprar(host){
    //verifica se está logado
    $.ajax({
        url:host+"/site/logado",
        success:function(e){
            if(e==true){
                $.ajax({
                    url:host+"/carrinho/pagseguro",
                    success:function(e){
                        $("#pag").html(e);
                        $("#pag").submit();
                    }
                })
            }else{
                alert('É preciso efetuar login para finalizar a compra');
            }
        }
    });
}
function valCpf($cpf){
    $cpf = preg_replace('/[^0-9]/','',$cpf);
    $digitoA = 0;
    $digitoB = 0;
    for($i = 0, $x = 10; $i <= 8; $i++, $x--){
        $digitoA += $cpf[$i] * $x;
    }
    for($i = 0, $x = 11; $i <= 9; $i++, $x--){
        if(str_repeat($i, 11) == $cpf){
            return false;
        }
        $digitoB += $cpf[$i] * $x;
    }
    $somaA = (($digitoA%11) < 2 ) ? 0 : 11-($digitoA%11);
    $somaB = (($digitoB%11) < 2 ) ? 0 : 11-($digitoB%11);
    if($somaA != $cpf[9] || $somaB != $cpf[10]){
        return false;	
    }else{
        return true;
    }
}
