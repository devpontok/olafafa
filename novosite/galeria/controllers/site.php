<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class site extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_site();
    }

    public function index() {

        //Buscar produtos com destaques
        $this->load->model('produtos_model', 'produto');
        $dados['pasta'] = './galeria/destaques';
        echo var_dump($_SERVER);
        //buscar dados de destaques
        $this->db->limit(6);
        $dados['destaques'] = $this->produto->get_productsByDestaques();


        //buscar dados de carrossel

        $dados['carrossel'] = scandir($dados['pasta'] . 'galeria/destaques');
        $conteudo = $this->load->view('home/destaques_view', $dados, true);
        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function deslogar() {
        $this->session->sess_destroy();
    }

    public function logado() {
        echo esta_logado(FALSE);
    }

    public function mostrar_carrinho() {
        $conteudo='';
        if (esta_logado(FALSE)) {
            $this->load->model('compras_model','compras');
            $user=$this->session->userdata('user_id');
            $this->db->where('compras_tb.cliente_tb_id',$user);
            $compras=$this->compras->get_all();
            $compras_itens=array();
             $this->load->model('itens');
            foreach($compras as $compra){
                //array id_compra, valor_total,data_compra,status
                echo var_dump($compra);
                $compras_itens['id_compra']=$compra->compra;
                $compras_itens['valor_total']=$compra->valor;
                $compras_itens['data_compra']=$compra->data_compra;
                $compras_itens['status']=$compra->status;
                $compras_itens['itens']=array();
                //busca itens para adicionar no array['itens']=>array de itens da compra
                $itens_dados=$this->itens->get_all($compra->compra);
                echo var_dump($itens_dado);
               // $dados_compra=$itens->get_all();
            }
            echo var_dump($compras);
           
        }else{
            $conteudo= "<h1>Desculpe, mas você não pode acessar esta área sem realizar login</h1>";
        }
        set_tema('conteudo',$conteudo);
        load_template();
        
    }
    public function cadastro(){
        init_site();
        $this->load->library('session');
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nome', 'Nome de Usuário', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('senha', 'Senha', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('email', 'Email', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('login', 'Login', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('data_nascimento', 'Data de nascimento', "trim|min_length[8]|strtolower");
        $this->form_validation->set_rules('cpf', 'CPF', "trim|required|min_length[11]|strtolower");
        $this->form_validation->set_rules('rua', 'Rua', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('bairro', 'Bairro', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('cidade', 'Cidade', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('estado', 'Estado', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('cep', 'CEP', "trim|required|min_length[4]|strtolower");

        $this->form_validation->set_rules('complemento', 'Complemento', "trim|required|min_length[4]|strtolower");
        $this->form_validation->set_rules('aceite', 'Aceite', "required");

        if ($this->form_validation->run() == TRUE):
            $nome = $this->input->post('nome', TRUE);
            $senha = md5($this->input->post('senha', TRUE));
            $email = $this->input->post('email', TRUE);
            $login = $this->input->post('login', TRUE);
            $data_nascimento = $this->input->post('data_nascimento', TRUE);
            $cpf = $this->input->post('cpf', TRUE);
            $rua = $this->input->post('rua', TRUE);
            $bairro = $this->input->post('bairro', TRUE);
            $cidade = $this->input->post('cidade', TRUE);
            $estado = $this->input->post('estado', TRUE);
            $cep = $this->input->post('cep', TRUE);
            $complemento = $this->input->post('complemento', TRUE);
            $aceite = $this->input->post('aceite', TRUE);

            $this->load->model('usuarios_model', 'usuarios');

            $query = $this->usuarios->do_cadastro($nome, $senha, $email, $login, $data_nascimento, $cpf, $rua, $bairro, $cidade, $estado, $cep, $complemento, $aceite);
            if ($query != FALSE) {
                $dados = array(
                    'user_id' => $query,
                    'user_nome' => $nome,
                    'user_logado' => true
                );

                $this->session->set_userdata($dados);

                redirect('');
            } else {
                echo 'Cadastro falhou';
            }
        endif;
        $dados['tela'] = 'usuario';
        $conteudo = $this->load->view('cadastro', $dados, true);
        set_tema('conteudo', $conteudo);
        load_template();
    }
    public function perfil(){
        init_site();
        $logado=esta_logado(false);
        $conteudo='';
        if($logado==true){
            
        }else{
            $conteudo.="<p>Para acessar esta página é preciso faze login</p>";
        }
        set_tema('conteudo',$conteudo);
        load_template();
    }

}

/*
 * End of file site.php
 * Location: application/controllers/site.php
 */
?>

