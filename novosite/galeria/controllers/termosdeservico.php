<?php
if ( !defined('BASEPATH')) exit('Acesso ao script não é permitido');
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class termosdeservico extends CI_Controller{
    public function __construct(){
           parent::__construct();
           init_site();
    }
    public function index()
    {
         $dados=array();
        $conteudo = '<div id="box-menu2"><h1 id="body-nome">Termos e Condições</h1></div><div class="site termos-page">
			<strong>Entrega</strong><br>
			Os pedidos serão processados em até 2 (dois) dias úteis após a confirmação do pagamento junto as instituições financeiras.<br>
			Os pedidos feitos aos sábados, domingos ou feriados serão despachados no primeiro dia útil subsequente à autorização do pagamento.<br>
			Utilizamos o sistema dos Correios para todas as nossas entregas e o prazo pode variar de acordo com a cidade e o tipo de frete escolhido na hora da compra.<br>
			O cliente pode escolher a forma de entrega no momento de fechamento de sua compra.  As modalidades de entrega que disponibilizamos são PAC e SEDEX.<br>

			As compras são embaladas e enviadas em caixas de papelão, mas você pode optar por embalar para presente qualquer produto do site, basta marcar a opção PRESENTE ao lado de cada produto a ser embalado.<br>

			A entrega podera ser feita no seu endereço ou no endereço do presenteado, mas cada pedido terá um único endereço de entrega.<br>
			Recuse o recebimento caso a embalagem esteja aberta, o produto avariado ou o produto esteja em desacordo com o pedido.<br><br>
			<strong>Trocas de Devoluções</strong><br>
			Nosso compromisso é com a sua satisfação, então se precisar trocar algum produto, por qualquer um dos motivos abaixo,o primeiro passo é entrar em contato conosco informando o motivo da troca através do e-mail trocas@olafafa.comno prazo de até 7 (sete) dias úteis após a data do recebimento do produto.<br><br>
			<strong>Troca de produtos</strong><br>
			Caso você receba o produto e queria trocar o tamanho ou escolher outra mercadoria, você poderá escolher um produto de mesmo valor ou faremos o abatimento proporcional ao valor pago na compra de outra mercadoria.<br><br>

			<strong>Insatisfação, arrependimento ou desistência</strong><br>
			Caso você receba o produto e desista de sua compra, você tem a opção de devolvê-lo e receber a restituição do valor da compra. O código do consumidor assegura o direito de troca do produto no caso de defeito ou desistência desde que ocorra em até 7 (sete) dias úteis após a entrega do mesmo.<br><br>


			<strong>Defeito de fabricação</strong><br>
			Caso você receba um produto que apresente defeito de fabricação, você deverá enviá-lo para análise. Se for constatado o problema, será feita a substituição do produto por outro igual ou você poderá optar pela restituição do valor pago ou pelo abatimento proporcional ao valor na compra de outro produto. O prazo para análise é de 30 dias corridos, mas pode ser definido antes deste período.<br><br>
			</p>
		</div>';
        set_tema('conteudo', $conteudo);
        load_template();
    }
}
/*
 * End of file termosdeservico.php
 * Location: application/controllers/termosdeservico.php
 */
?>

