<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class produtos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_painel();
    }

    public function index() {
        $this->load->view("");
        init_painel();
        $this->load->model('produtos_model', 'produto');
    }

    public function lista() {
        //Categorias para pesquisa
        $this->load->model('categorias_model', 'categoria');
        $categorias = $this->categoria->get_all();
        $dados['options'] = array();
        $dados['options'][0] = '';
        foreach ($categorias as $cat) {
            $dados['options'][$cat->id] = $cat->nome;
        }
        $dados['select'] = array(0);

        $conteudo = $this->load->view('adm/menu_view', $dados, true);
        //Pegar produtos para montar grid de listagem
        $this->load->model('produtos_model', 'produto');
        $this->db->select('categorias_tb.alias as categoria_nome, produtos_tb.id,produtos_tb.nome');
        $this->db->join('categorias_tb', 'categorias_tb.id=produtos_tb.categorias_tb_id');
        $this->db->group_by('produtos_tb.id');
        $dados['tela'] = 'lista';
        $dados['produtos'] = $this->produto->get_all(NULL, 20, 0);

        $conteudo.=$this->load->view('adm/produto', $dados, true);
        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function atualiza($id = NULL) {
        $conteudo = "";

        $this->load->model('produtos_model', 'produto');
        $product = $this->produto->get_productsById($id);
        $this->load->model('categorias_model', 'categoria');
        $categorias = $this->categoria->get_all();
        $dados['options'] = array();
        foreach ($categorias as $cat) {
            $dados['options'][$cat->id] = $cat->nome;
        }
        $dados['tela'] = 'cadastro';
        $dados['select'] = array();
        $dados['action'] = base_url('adm/produtos/atualiza/' . $id);

        if (!is_null($id) && is_array($product)) {
            $this->load->library('upload');

            $this->load->model('categorias_model', 'categoria');

            if (!is_null($product)) {
                $dados['nome'] = $product[0]->nome;
                $dados['valor'] = $product[0]->valor;
                $dados['peso'] = $product[0]->peso;
                $dados['detalhes'] = $product[0]->detalhes;
                $dados['id'] = $product[0]->id;
                $dados['destaques'] = ($product[0]->destaques == 1) ? 'checked' : '';
                $dados['select'] = array($product[0]->categorias_tb_id);
                $cat_nome = $this->categoria->get_byId($product[0]->categorias_tb_id);
                $imagem = get_imagemProduto($cat_nome[0]->alias, $id);
                $dados['imagem'] = $imagem['url'];
            }

            $this->form_validation->set_rules('nome', 'Nome', "trim|required|min_length[4]|strtolower|xss_clean");
            $this->form_validation->set_rules('valor', 'Preço', "trim|required|decimal|xss_clean");
            $this->form_validation->set_rules('peso', 'Peso', "trim|decimal|xss_clean");
            $this->form_validation->set_rules('detalhes', 'Detalhes', "trim|max_length[600]|strtolower|xss_clean");
            $this->form_validation->set_rules('destaque', 'Destaques', "xss_clean");
            $this->form_validation->set_rules('categoria', 'Categoria', "trim|required|numeric|xss_clean");
            $dados['tela'] = 'cadastro';
            $dados['action'] = "adm/produtos/atualiza/{$id}";
            if ($this->form_validation->run() == TRUE):
                $nome = $this->input->post('nome', TRUE);
                $valor = $this->input->post('valor', TRUE);
                $peso = $this->input->post('peso', TRUE);
                $detalhes = $this->input->post('detalhes', TRUE);
                $categoria = $this->input->post('categoria', true);
                $destaque = $this->input->post('destaque') == 'destaque';


                if ($this->produto->do_update($nome, $valor, $peso, $detalhes, $destaque, $categoria, $id)) {
                    //Trocar imagem produto
                    $conteudo.="<div class='col-md-12  form-group has-success'><input type='text' class='form-control' value='O produto foi atualizado com sucesso!'/></div>";
                    if ($_FILES['arquivo']['name'] != '') {
                        if (isset($imagem['path'])) {
                            unlink($imagem['path']);
                        }
                        $cat = $this->categoria->get_byId($categoria);
                        $pasta = get_pathServidor() . "galeria/roupas/";
                        $img = "{$pasta}{$cat[0]->alias}{$id}";
                        $conteudo.=upload($pasta, $img, $_FILES);
                        $this->load->model('galeria_model', 'galeria');
                        $ext = '.' . substr($_FILES['arquivo']['type'], strpos($_FILES['arquivo']['type'], '/') + 1);
                        $img = "{$cat[0]->alias}{$id}{$ext}";
                        $this->galeria->do_update($img, $id, 1);
                    }
                } else {
                    $conteudo.="<div class='col-md-12  form-group has-success'><input type='text' class='form-control' value='O produto não foi atualizado com sucesso!'/></div>";
                }
            endif;
            $conteudo = $this->load->view('adm/menu_view', $dados, TRUE);
            $conteudo.=$this->load->view('adm/produto', '', TRUE);
            set_tema('conteudo', $conteudo);
        } else {
            $conteudo = $this->load->view('adm/menu_view', $dados, TRUE);
            $conteudo.= "<h1>Produto não localizado, por favor verifique se ainda está cadastrado na loja e tente novamente</h1>";
        }
        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function cadastro() {
        //MONTAR O MENU SUPERIOR
        $conteudo = '';
        //Seleção de tela
        $dados['tela'] = 'cadastro';
        //Action do formulário
        $dados['action'] = "adm/produtos/cadastro";
        $this->load->model('categorias_model', 'categoria');
        $categorias = $this->categoria->get_all();
        //montar select de categorias
        $dados['options'] = array();
        foreach ($categorias as $categoria_item) {
            $dados['options'][$categoria_item->id] = $categoria_item->alias;
        }
        $dados['select'] = array();
        $dados['destaques'] = '';
        //carregar menu
        $conteudo.=$this->load->view('adm/menu_view', $dados, TRUE);
        //regras de validação de formulário
        $this->form_validation->set_rules('nome', 'Nome', "trim|required|min_length[4]|strtolower|xss_clean");
        $this->form_validation->set_rules('valor', 'Preço', "trim|required|decimal|xss_clean");
        $this->form_validation->set_rules('peso', 'Peso', "trim|decimal|xss_clean");
        $this->form_validation->set_rules('detalhes', 'Detalhes', "trim|max_length[600]|strtolower|xss_clean");
        $this->form_validation->set_rules('destaque', 'Destaque', "xss_clean");
        $this->form_validation->set_rules('categoria', 'Categoria', "trim|required|numeric|xss_clean");
        //Disparar cadastro  
        if ($this->form_validation->run() == TRUE):
            
            $nome = $this->input->post('nome', TRUE);
            $valor = $this->input->post('valor', TRUE);
            $peso = $this->input->post('peso', TRUE);
            $detalhes = $this->input->post('detalhes', TRUE);
            $destaques = $this->input->post('destaque', TRUE) == 'destaque';
            $categoria = $this->input->post('categoria', true);
            $this->load->model('produtos_model', 'produto');
            $id_produto = $this->produto->do_cadastro($nome, $valor, $peso, $detalhes, $destaques, $categoria);
            if (is_numeric($id_produto)) {
                $cat = $this->categoria->get_byId($categoria);
                $pasta = get_pathServidor() . "galeria/roupas/";
                $img = "{$pasta}{$cat[0]->alias}{$id_produto}";
                $conteudo.="<div class='col-md-12  form-group has-success'><input type='text' class='form-control' value='O cadastro de produto foi bem suscedido!'/></div>";

                $conteudo.=upload($pasta, $img, $_FILES);
                
                $this->load->model('galeria_model', 'galeria');
                $ext = '.' . substr($_FILES['arquivo']['type'], strpos($_FILES['arquivo']['type'], '/') + 1);
                $img = "{$cat[0]->alias}{$id_produto}{$ext}";
                $this->galeria->do_cadastro($img, $id_produto, 1);

            } else {
                $conteudo.="<div class='col-md-12  form-group has-error'><input type='text' class='form-control' value='O cadastro de produtos não foi bem suscedido!'/></div>";
            }
        endif;
        $conteudo.=$this->load->view('adm/produto', $dados, true);
        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function destaques() {
        $this->load->model('categorias_model', 'categoria');
        $categorias = $this->categoria->get_all();
        $dados['options'] = array();
        foreach ($categorias as $cat) {
            $dados['options'][$cat->id] = $cat->nome;
        }
        $dados['select'] = array();

        $conteudo = $this->load->view('adm/menu_view', $dados, true);
        //Pegar produtos para montar grid de listagem
        $this->load->model('produtos_model', 'produto');
        $this->db->select('categorias_tb.alias as categoria_nome, produtos_tb.id,produtos_tb.nome');
        $this->db->join('categorias_tb', 'categorias_tb.id=produtos_tb.categorias_tb_id');
        $this->db->group_by('produtos_tb.id');
        $this->db->where(array('destaques' => 1));
        $dados['tela'] = 'lista';
        $dados['produtos'] = $this->produto->get_all(NULL, 20, 0);
        $conteudo.=$this->load->view('adm/produto', $dados, true);
        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function slider() {
        //Montar tela de menu
        $this->load->model('categorias_model', 'categoria');
        $categorias = $this->categoria->get_all();
        $dados['options'] = array();
        foreach ($categorias as $cat) {
            $dados['options'][$cat->id] = $cat->nome;
        }
        $dados['select'] = array();

        $conteudo = $this->load->view('adm/menu_view', $dados, true);
        //Obter as imagens de destaque do site
        $dados['destaques'] = scandir(get_pathServidor() . "galeria/destaques");
        //Permitir excluir imagem solicitada
        unset($dados['destaques'][0]);
        unset($dados['destaques'][1]);

        $this->form_validation->set_rules('nome', 'arquivo', "trim|xss_clean");
        if ($this->form_validation->run() == true):
            //checar se pode enviar nova imagem

            $fotos = count($dados['destaques']);

            if ($fotos < 3) {
                $fotos+=1;

                //enviar nova imagem
                $pasta = get_pathServidor() . "galeria/destaques/";
                $img = $pasta."destaque{$fotos}";

                $conteudo.=upload($pasta, $img, $_FILES);
                redirect('adm/produtos/slider');
            } else {
                $conteudo.="<div class='col-md-12 form-group has-error'><input type='text' class='form-control' value='Não é permitido ter mais que três imagens como destaque para a home do site'/></div>";
            }
            $_FILES = array();
        endif;

        $conteudo.=$this->load->view('adm/destaques', $dados, true);
        //Permitir adcionar nova imagem, se ainda não houverem 3 selecionadas
        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function apagarSlider() {
        $imagem = $this->input->post('id');
        unlink(get_pathServidor() . "galeria/destaques/{$imagem}");
        redirect('adm/produtos/apagarSlider');
    }

    public function pesquisar() {
        $this->load->model('categorias_model', 'categoria');
        $categorias = $this->categoria->get_all();
        $dados['options'] = array();
        $dados['options'][0] = '';
          $conteudo = $this->load->view('adm/menu_view', $dados, TRUE);
        foreach ($categorias as $cat) {
            $dados['options'][$cat->id] = $cat->nome;
        }
        $dados['select'] = array(0);

        $nome = $this->input->post('nome', true);
        $categoria = $this->input->post('categoria', true);
        if ($nome != '') {
            $this->db->like('produtos_tb.nome', $nome);
        }if ($categoria != 0) {
            $this->db->where('categorias_tb.id', $categoria);
        }
        $this->load->model('produtos_model', 'produto');
        $this->db->select('categorias_tb.alias as categoria_nome, produtos_tb.id,produtos_tb.nome');
        $this->db->join('categorias_tb', 'categorias_tb.id=produtos_tb.categorias_tb_id');
        $this->db->group_by('produtos_tb.id');
        $dados['tela'] = 'lista';
        $dados['produtos'] = $this->produto->get_all();

        $conteudo.= $this->load->view('adm/produto', $dados, true);
        set_tema('conteudo',$conteudo);
        load_template();
    }

    public function galeria($id = NULL) {
        $dados = array();
        $conteudo = $this->load->view('adm/menu_view', $dados, TRUE);
        if (!is_null($id)) {
            $this->load->model('produtos_model', 'produto');
            $produto = $this->produto->get_productsById($id);
            $dados['id'] = $id;
            if (($produto) != false) {
                $this->db->where('produtos_tb_id', $id);
                $this->load->model('galeria_model', 'galeria');
                $dados['galeria'] = $this->galeria->get_all();
                $conteudo.=$this->load->view('galeria_view', $dados, true);
            } else {
                $conteudo.="<div class='col-md-12 form-group has-error'><input type='text' class='form-control' value='O produto não existe'/></div>";
            }
        } else {
            $conteudo.="<div class='col-md-12 form-group has-error'><input type='text' class='form-control' value='O produto não existe'/></div>";
        }
        set_tema('conteudo', $conteudo);
        load_template();
    }
public function delete_imagem(){
        $imagem=$this->input->post('id',true);
        if(!is_null($imagem)){
            unlink(get_pathServidor() . "galeria/roupas/{$imagem}");
            $this->load->model('galeria_model','galeria');
            echo $this->galeria->do_delete($imagem);
        }
    }
    public function add_Imagem($id = null) {
        echo var_dump($_FILES);
        if (!is_null($id)) {
            $array = $this->input->post();
            $principal = (isset($array['prinicipal']) && $array['prinicipal'] == 'principal') ? 1 : 0;
            $this->load->model('produtos_model', 'produto');
            $produtos = $this->produto->get_productsById($id);
            $this->load->model('categorias_model', 'categoria');
            $cat = $this->categoria->get_byId($produtos[0]->categorias_tb_id);
            $categoria_produto = $cat[0]->alias;
            $pasta = get_pathServidor() . "galeria/roupas/";
            $ext = '.' . substr($_FILES['arquivo']['type'], strpos($_FILES['arquivo']['type'], '/') + 1);
            $this->load->model('galeria_model', 'galeria');
            if ($principal == 1) {
                //apagar imagem anterior
                $imagem = get_imagemProduto($categoria_produto, $id);
                if (file_exists($imagem['path'])) {
                    unlink($imagem['path']);
                }
                $this->galeria->delete($id);
                $img = "{$cat[0]->alias}{$id}";
            } else {
                //adicionar sequencial ao noma da imagem
                
                $this->db->select("count(id)+1 as max");
                $this->db->where('produtos_tb_id', $id);
                $imagem = $this->galeria->get_all();
                //fazerupload de imagem
                $img = "{$cat[0]->alias}{$id}_{$imagem[0]->max}";
                //registrar nova imagem no banco
            }
            $conteudo=upload($pasta, $pasta.$img, $_FILES);
            $this->load->model('galeria_model','galeria');
            $this->galeria->do_cadastro($img.$ext, $id, $principal);
            redirect("adm/produtos/galeria/{$id}");
        } else {
            //IMPOSSÍVEL ADD IMAGEM NÃO FOI ID PRODUTO
            echo "<div class='col-md-12 form-group has-error'><input type='text' class='form-control' value='O produto não existe'/></div>";
        }
    }
    public function troca_principal(){
        $id=$this->input->post('imagem',true);
        echo $id;
       if(!is_null($id)){
            $imagem=explode("-",$id);
            $str="update galeria set principal='0' where principal='1' and produtos_tb_id='{$imagem[1]}';";
            $this->db->query($str);
            $str="update galeria set principal='1' where arquivo like '{$imagem[0]}' and produtos_tb_id='{$imagem[1]}';";
            $this->db->query($str);
        }
    }
public function apaga_produto(){
    $id=$this->input->post('id');
    
        if(!is_null($id)){
            $this->load->model('itens');
            $this->db->where(array('produtos_tb_id'=>$id));
            $itens=$this->itens->get_all();
            if(count($itens)>0){
                echo 'O produto já foi comprado e não pode ser removido do banco de dados';
            }else{
                $this->load->model('produtos_model','produtos');
                echo $this->produtos->delete($id);
            }
        }else{
            echo 'O produto não foi identificado';
        }
}
    

}

/*
 * End of file produtos.php
 * Location: application/controllers/produtos.php
 */
?>

