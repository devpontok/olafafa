<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class produtos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_site();
    }

    public function index() {
	set_tema('footerinc', (load_js('http://gs-libs.googlecode.com/files/jquery.gs-innerzoom.min.js', 'js', TRUE)),false);
	set_tema('footerinc',load_js('funcao'),false);
	set_tema('footerinc',load_js('zoom'),false);
        $segs = $this->uri->segment_array();
        if (!is_numeric($segs[count($segs)])) {
            set_tema('conteudo', '<h1>O produto não foi localizado</h1>');
        } else {
            $id = $segs[count($segs)];
            $conteudo = '';
            $dados = array();
            unset($segs[1]);
            //Montar dados de produto
            $this->load->model('produtos_model', 'produto');
            $produto = $this->produto->get_productsById($id);
            if ($produto != false) {
                $dados['imagens'] = array();


                $this->load->model('categorias_model', 'categoria');
                $dados['produto'] = $produto[0];
                if (count($segs) == 1) {
                    //pegar categorias do produto
                    $cat = $this->categoria->get_byId($dados['produto']->categorias_tb_id);
                    $cat2 = '';
                    $url = $cat[0]->alias;
                    if (!is_null($cat[0]->pai_id)) {
                        $cat2 = $this->categoria->get_byId($cat[0]->pai_id);
                        $url.='/' . $cat2[0]->alias;
                    }
                    $url = 'roupas-infantis/' . $url . "/{$id}";
                    redirect(base_url($url));
                } else {

                    $cat = $this->categoria->get_byAlias($segs[2]);
                }
                $dados['categoria'] = $cat[0];
                $dados['filhos'] = $this->categoria->get_categoriasFilhas($dados['categoria']->id);
                if ($dados['filhos']) {
                    $dados['sem_titulo'] = true;
                    $conteudo.= $this->load->view('submenu_view', $dados, TRUE);
                }
                set_tema('headerinc', load_css(array('bebe','zoom')),FALSE);
                $this->load->model('galeria_model', 'galeria');
                $this->db->where('produtos_tb_id', $id);
                $this->db->order_by('principal', 'desc');
                $dados['imagens'] = $this->galeria->get_all();
                $conteudo.=$this->load->view('produtos_view', $dados, TRUE);
            } else {
                $conteudo = 'sem dados para este produto';
            }
        }

        set_tema('conteudo', $conteudo);
        load_template();
    }

    public function adiciona_carrinho() {
        if (!isset($_SESSION))
            session_start();

        $id = $this->input->post('id', TRUE);
        $data = array(
            'qty' => $this->input->post('qtd', TRUE),
            'price' => $this->input->post('valor', TRUE),
            'name' => $this->input->post('nome', TRUE)
        );

        echo var_dump($data);
        if (isset($_SESSION['carrinho'])) {
            $dados = $_SESSION['carrinho'];
            $carrinho = array_keys($_SESSION['carrinho']); 
            if (!in_array($id, $carrinho)) {
                $dados[$id] = $data;
            }
            // $carrinho[count($carrinho)]=$data;
            if (isset($_POST['total'])) {
                $_SESSION['total'] = $_POST['total'];
                if (in_array($id, $carrinho)) {
                    $dados[$id]['qty'] = $data['qty'];
                }
            } else {
                $_SESSION['total']+= (float) $data['price'];
               if (in_array($id, $carrinho)) {
                     $dados[$id]['qty']+= 1;
                }
            }
            if (isset($_POST['itens'])) {
                $_SESSION['itens'] = $_POST['itens'];
            } else {
                $_SESSION['itens']+=1;
            }
        } else {
            $dados[$id] = $data;
            $_SESSION['total'] = (float) $data['price'];
            $_SESSION['itens'] = 1;
        }
        $_SESSION['carrinho'] = $dados;

        // echo var_dump($_SESSION['carrinho']);
    }

}

/*
 * End of file produtos.php
 * Location: application/controllers/produtos.php
 */
?>

