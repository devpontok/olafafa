<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class carrinho extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_site();
    }

    public function index() {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (isset($_SESSION['carrinho']) && !is_null($_SESSION['carrinho'])) {
            $dados = array();
            $dados['carrinho'] = $_SESSION['carrinho'];
            $dados['total'] = $_SESSION['total'];
            $dados['itens'] = $_SESSION['itens'];
            $conteudo = $this->load->view("cart_view", $dados, true);
        } else {
            $conteudo = "Não temos itens no carrinho";
        }
        echo $conteudo;
       /* set_tema('conteudo', $conteudo);
        load_template();*/
    }
    public function pagseguro(){
        $this->load->model('compras_model','compras');
        $dados['idcompra']=$this->compras->do_cadastro();
        $dados['carrinho']=$_SESSION['carrinho'];
        $this->load->model('itens');
        $this->itens->do_cadastro($dados['idcompra']);
        echo $this->load->view('pagseguro',$dados,true);
        unset($_SESSION['carrinho']);
    }

}

/*
 * End of file carrinho.php
 * Location: application/controllers/carrinho.php
 */
?>

