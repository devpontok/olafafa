<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class privacidade extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_site();
    }

    public function index() {
        $dados = array();
        $conteudo = '<div id="box-menu2"><h1 id="body-nome">Integridade de dados</h1></div><div class="site termos-page">
<p>Os dados armazenados em nossa base de dados não será utilizado para outros fins, além das solicitações feitas pelo sistema.
O dados são alocados em um banco de dados relacional hospedado na UOL Host em ambiente compartilhado de hospedagem.</p></div>';
        set_tema('conteudo', $conteudo);
        load_template();
    }

}

/*
 * End of file privacidade.php
 * Location: application/controllers/privacidade.php
 */
?>

