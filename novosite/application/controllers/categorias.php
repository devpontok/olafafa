<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class categorias extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_site();
    }

    public function index($alias = NULL,$cat2=NULL) {
        $dados =array();
        $url=$alias;
        if(!is_null($cat2)){
            $alias=$cat2;
            $url.='/'.$cat2.'/';
        }
        //$dados['url']=base_url('roupas-infantis/'.$url);
        if ($alias != NULL) {
            
             //Monta a página
           //set_tema('headerinc',load_css('bebe'));
            $this->load->model('categorias_model', 'categoria');
            $cat = $this->categoria->get_byAlias($alias);

            if ($cat) {
                //Buscar categorias filhas
                $cat=$cat[0];
                $dados['filhos'] = $this->categoria->get_categoriasFilhas($cat->id);
                //Montar menu de filhos
                $dados['categoria']=$cat;
                $conteudo = '';

                if(!is_null($cat->pai_id)){
                    $dados['filhos'] = $this->categoria->get_categoriasFilhas($cat->pai_id);
                    $pai=$this->categoria->get_byId($cat->pai_id);

                //Montar menu de filhos
                    $dados['categoria']=$pai[0];
                    $dados['categoria']->nome=$cat->nome;
                   $conteudo.= $this->load->view('submenu_view', $dados, TRUE);
                    
                    $dados['categoria']=$cat;
                }elseif ($dados['filhos']) {
                    $conteudo.= $this->load->view('submenu_view', $dados, TRUE);
                }
		$conteudo.='<div id="box-menu">';
		$conteudo.='<div class="site">';
		$conteudo.='<div id="body-nome">';
		$conteudo.='<h1>'.$cat->nome.'</h1>';
		$conteudo.='</div>';
		$conteudo.='</div>';
		$conteudo.='</div>';
                //montar produtos
                $dados['pasta']=get_pathServidor();
                
                $this->load->model('produtos_model','produtos');

                $dados['produtos']=$this->produtos->get_productsByCategoria($cat->id);
                
                $conteudo.=$this->load->view("categorias_view",$dados,TRUE);
             
               set_tema('conteudo', $conteudo);
                load_template();
            } else {
                set_tema('conteudo', '<h2>Categoria não existe</h2>');
                load_template();
            }
        } else {
            //Notifica erro para montar página
            set_tema('conteudo', '<h2>Categoria sem produtos listados</h2>');
            load_template();
        }
    }

}

/*
 * End of file categorias.php
 * Location: application/controllers/categorias.php
 */
?>

