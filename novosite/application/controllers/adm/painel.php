<?php
if ( !defined('BASEPATH')) exit('Acesso ao script não é permitido');
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Painel extends CI_Controller{
    public function __construct(){
           parent::__construct();
           init_painel();
    }
    public function index()
    {
        IF(esta_logado(TRUE)){
                set_tema('titulo','painel');
                $conteudo='';
                $conteudo=$this->load->view('adm/menu_view','',TRUE);
                set_tema('conteudo',$conteudo);
                load_template();
        }else{
              $this->inicio();
        }
        //$this->load->view("painel_view");
    }
    public function inicio(){
        //carregar o módulo de usuários e mostrar tela de login
      redirect('usuarios/login');
    }
}
/*
 * End of file sistema.php 
 * Location: Application/controllers/adm/painel.php
  */

