<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class compras extends CI_Controller {

    private $conteudo;

    public function __construct() {
        parent::__construct();
        init_painel();
        $this->conteudo = $this->load->view('adm/menu_view', '', TRUE);
    }

    public function index() {
        $this->load->view("");
    }

    public function lista() {
        $this->load->model('compras_model', 'compras');
        $dados['compras']=$this->compras->get_all();
        $this->conteudo.= $this->load->view('adm/compras_view', $dados, true);
        set_tema('conteudo', $this->conteudo);
        load_template();
    }

     public function itens($id) {
        $this->load->model('itens', 'compras');
        $this->db->where('compras_tb_id',$id);
        $dados['itens']=$this->compras->get_all();
        $this->conteudo.= $this->load->view('adm/itens_view', $dados, true);
        set_tema('conteudo', $this->conteudo);
        load_template();
    }
}

/*
 * End of file compras.php
 * Location: application/controllers/compras.php
 */
?>

