<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class pagamento extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_site();
    }

    public function index() {
        $dados = array();
        $conteudo = '<div id="box-menu2"><div id="body-nome"><div class="site"><h1>Formas de pagamento</h1></div></div></div>
                <div class="site termos-page">            
                <p>
			<strong>Pagamentos</strong><br>
			A nossa forma de pagamento pode ser por:<br><br><br>
			<strong>-Depósito bancário</strong><br>
			O pagamento poderá ser realizado por meio do Internet banking, nas agências bancárias ou caixas eletrônicos para a conta da Olá fafá!no Banco Itaú, nesse caso entre em contato conosco através do e-mail pagamento@olafafa.com
			Após a compensação e confirmação do depósito, sua encomenda será entregue no endereço do seu cadastro.<br><br>
			<strong>-Pagseguro</strong><br>
			Através do Pagseguro aceitamos todos os cartões de débito e crédito e dividimos em até 3x (parcela mínima R$ 40,00)
			Poder ser também por boleto bancário e nesse caso a data de vencimentoé de 2 (dois) dias úteis após a realização de sua compra, após esta data, o documento perde a validade e a compra será automaticamente cancelada. A confirmação de pagamento é feita pelo Pagseguro em até 3 (três) dias úteis, sendo assim, não é preciso enviar-nos qualquer notificação. Caso necessite de um novo boleto envie um e-mail para pagamento@olafafa.com. <br><br>
			</div>';
        set_tema('conteudo', $conteudo);
        load_template();
    }

}

/*
 * End of file privacidade.php
 * Location: application/controllers/privacidade.php
 */
?>

