<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');

//carrega módulo do sistema devolvendo a tela solicitada
function load_modulo($modulo = NULL, $tela = NULL, $diretorio = 'adm') {
    $CI = & get_instance();
    if ($modulo != NULL) {
        return $CI->load->view("$diretorio/$modulo", array('tela' => $tela), TRUE);
    } else {
        return FALSE;
    }
}

//seta valores ao array tema da classe sistema
function set_tema($prop, $valor, $replace = TRUE) {
    $CI = & get_instance();
    $CI->load->library('sistema');
    if ($replace) {
        $CI->sistema->tema[$prop] = $valor;
    } else {
        if (!isset($CI->sistema->tema[$prop])) {
            $CI->sistema->tema[$prop] = '';
        }
        $CI->sistema->tema[$prop].=$valor;
    }
}

//retorna os valores do  arrayy tema da classe array
function get_tema() {
    $CI = & get_instance();
    $CI->load->library('sistema');
    return $CI->sistema->tema;
}

//Inicializar painel adm carregando recursos necessáros
function init_painel() {
    $CI = & get_instance();
    $CI->load->library(array('sistema', 'session', 'form_validation'));
    $CI->load->helper(array('form', 'url', 'array', 'text'));
    $CI->load->model('usuarios_model', 'usuarios');
    //carregamento dos models
    set_tema('titulo_padrao', 'painel administrativo');
    // set_tema('rodape','<p>copy'.date('Y')." | todos os direitos reservados para ");
    set_tema('template', 'painel_view');
    set_tema('rodape', '<p>&copy; ' . date('Y') . " | todos os direitos reservados para Olá Fafá");
    set_tema('headerinc', load_css(array('bootstrap', 'flat-ui', 'demo', 'estilo'), 'css/adm'), FALSE);
    set_tema('footerinc', load_js(array('jquery-1.8.3.min', 'jquery-ui-1.10.3.custom.min',
                'jquery.ui.touch-punch.min', 'bootstrap.min', 'bootstrap-select', 'bootstrap-switch',
                'flatui-checkbox', 'flatui-radio', 'jquery.tagsinput', 'jquery.placeholder', 'jquery.stacktable', 'application','function'), 'js/adm', FALSE));
    if(strpos($_SERVER['REQUEST_URI'],'login')<=0 && strpos($_SERVER['REQUEST_URI'],'usuarios/cadastro')<=0){
        if(administrador()==FALSE){
            redirect();
        }

    }
}

//Inicializar o frontendsite carregando recursos necessáros
function init_site() {
    $CI = & get_instance();
    $CI->load->library('sistema', 'session', 'form_validation','cart');
    $CI->load->helper(array('form', 'url', 'array', 'text'));
    //carregamento dos models
    //recuperar id de usuário logado
    if (esta_logado(FALSE)) {
        $dados['user'] = $CI->session->userdata;
        
    } else {
        $dados = NULL;
    }
    
    if(!isset($_SESSION)){
        session_start();
    }if(isset($_SESSION['carrinho'])){
          $dados['total']=$_SESSION['total'];
        $dados['itens']=$_SESSION['itens'];
    }else{
        $dados['total']="0,0";
        $dados['itens']="0";
    }
      
    //Determinar se é para logar ou saudar usuário
    set_tema('login', $CI->load->view("login_view", $dados, true));
    set_tema('titulo_padrao', 'Olá Fafá');
    set_tema('headerinc', load_css('main'));
    set_tema('footerinc', load_js(array('jquery', 'jquery.lazyload.min' ,'jquery.orbit-1.2.3.min',  'mask','functions')),FALSE);
    set_tema('rodape', $CI->load->view("rodape_view", '', true));
    set_tema('template', 'site_view');
}

//carrega template passando array tema como parâmetro
function load_template() {
    $CI = & get_instance();
    $CI->load->library('sistema');
    $CI->parser->parse($CI->sistema->tema['template'], get_tema());
}

//carrega arquivos de css 
function load_css($arquivo = NULL, $pasta = 'css', $media = 'all') {
    if ($arquivo != NULL) {
        $CI = & get_instance();
        $CI->load->helper('url');
        $retorno = '';
        if (is_array($arquivo)) {
            foreach ($arquivo as $css) {
                $retorno.='<link rel="stylesheet" type="text/css" href="' . base_url("$pasta/$css.css") . '" media="' . $media . '"/>';
            }
        } else {
            $retorno = '<link rel="stylesheet" type="text/css" href="' . base_url("$pasta/$arquivo.css") . '" media="' . $media . '"/>';
        }
    }
    return $retorno;
}

//carrega arquivos .js de pasta ou servidor remoto
function load_js($arquivo = NULL, $pasta = 'js', $remoto = FALSE) {
    if ($arquivo != NULL) {
        $CI = & get_instance();
        $CI->load->helper('url');
        $retorno = '';
        if (is_array($arquivo)) {
            foreach ($arquivo as $js) {
                if ($remoto) {
                    $retorno.='<script type="text/javascript" src="' . $js . '"></script>';
                } else {
                    $retorno.='<script type="text/javascript" src="' . base_url("$pasta/$js.js") . '"></script>';
                }
            }
        } else {
            if ($remoto) {
                $retorno = '<script type="text/javascript" src="' . $arquivo . '"></script>';
            } else {
                $retorno = '<script type="text/javascript" src="' . base_url("$pasta/$arquivo.js") . '"></script>';
            }
        }
    }
    return $retorno;
}

//mostrar erros de validação em formulários
function errors_validacao($local=null) {
    if (validation_errors()) {
        if($local==null){
        $erro = implode('!!!   ', explode('.', strip_tags(validation_errors())));
        echo '<br/><br/><br/><p data-toggle="tooltip" class="btn-danger" title="' . $erro . '"></p>';
        }else{
            echo  validation_errors('<p class="item-valid">','</p>');
        }
    }
}

//verifica se o usuário está logado
function esta_logado($redir = TRUE, $local = 'usuarios/login') {
    $CI = & get_instance();
    $CI->load->library('session');
    $user_status = $CI->session->userdata('user_logado');
    if (!isset($user_status) || $user_status != TRUE) {
        $CI->session->sess_destroy();
        if ($redir) {
            redirect($local);
        } else {
            return FALSE;
        }
    } else {
        return TRUE;
    }
}


//devolve caminho do servidor
function get_pathServidor() {

    if($_SERVER['HTTP_HOST']    ==  'localhost'){
         $server= $_SERVER['DOCUMENT_ROOT'].'/olafafa/';
    }else{
        $server= '/var/www/html/olafafa.com/web/novosite/';
    }
    return $server;
}

function get_imagemProduto($categoria, $id) {

    $img = NULL;
    $jpg = get_pathServidor() . "galeria/roupas/$categoria$id.jpg";
    $png = get_pathServidor() . "galeria/roupas/$categoria$id.png";
    $jpeg = get_pathServidor() . "galeria/roupas/$categoria$id.jpeg";
    if (file_exists($jpg)) {
        $img['url'] = base_url("galeria/roupas/$categoria$id.jpg");
        $img['path'] = get_pathServidor() . "galeria/roupas/$categoria$id.jpg";
    } else {
        if (file_exists($jpeg)) {
            $img['url'] = base_url("galeria/roupas/$categoria$id.jpeg");
            $img['path'] = get_pathServidor() . "galeria/roupas/$categoria$id.jpeg";
        } else {
            if (file_exists($png)) {
                $img['url'] = base_url("galeria/roupas/$categoria$id.png");
                $img['path'] = get_pathServidor() . "galeria/roupas/$categoria$id.png";
            }
        }

        return $img;
    }
}

function upload($pasta, $img, $array) {

    $CI = & get_instance();
    $field_name = 'arquivo';
    $config['upload_path'] = $pasta;

    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size'] = '100';
    $config['max_width'] = '1024';
    $config['max_height'] = '768';

    if (isset($array[$field_name]) && is_uploaded_file($array[$field_name]['tmp_name'])) {
        $CI->load->library('upload', $config);
        $ext = '.' . substr($array[$field_name]['type'], strpos($array[$field_name]['type'], '/') + 1);
        chmod($pasta,0777);
        $subiu=move_uploaded_file($array[$field_name]['tmp_name'], $img . $ext);
        chmod($img . $ext,0777);
          if ($subiu==true) {
            return "<div class='col-md-12  form-group has-success'><input type='text' class='form-control' value='O upload da imagem foi bem suscedido!'/></div>";
        } else {
            //upload passed
            return "<div class='col-md-12  form-group has-error'><input type='text' class='form-control' value='O upload da imagem não foi bem suscedido!'/></div>";
        }
    }
}

function administrador(){
    $CI = & get_instance();
    $CI->load->library('session');
    $user_status = $CI->session->userdata;
    return(isset($user_status['user_adm']) && $user_status['user_adm']== TRUE);
}
function cria_url($string){
    //remover todos os acentos e caracteres especiais
    $string=strtolower($string);
    $string=  trim($string);
    $string=str_replace(array('à','á','ã','â',"ä"),'a',$string);
    $string=str_replace(array('è','é','ẽ','ê',"ë"),'e',$string);
    $string=str_replace(array('ì','ì','ĩ','î',"ï"),'i',$string);
    $string=str_replace(array('ò','ó','õ','ô',"ö"),'o',$string);
    $string=str_replace(array('ù','ú','ũ','û',"ü"),'u',$string);
    $string=str_replace("ç",'c',$string);
    $string=str_replace("ñ",'n',$string);
    $string=str_replace(' ','-',$string);
    return $string;
}


/*
 * End of file sistema.php 
 * Location: Application/helpers/funcoes_helper.php
 */
?>
