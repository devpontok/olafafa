<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');

class itens extends CI_Model {

    public function get_all($id = NULL) {
        $this->db->select('cliente_tb.nome as cliente,produtos_tb.nome as produto, produtos_tb.valor as unidade,itens.quantidade,data_compra,status');
        $this->db->join('compras_tb', 'compras_tb.id=itens.compras_tb_id');
  
        $this->db->join('cliente_tb', 'compras_tb.cliente_tb_id=cliente_tb.id');
        $this->db->join('produtos_tb', 'produtos_tb.id=itens.produtos_tb_id');
        return $this->db->get('itens')->result();
    }

    public function do_cadastro($idcompra = NULL) {
        if (!is_null($idcompra)) {
            if (!isset($_SESSION)) {
                session_start();
            }

            $dados['carrinho'] = $_SESSION['carrinho'];
            foreach ($dados['carrinho'] as $produto => $item) {
                $data = array('produtos_tb_id' => $produto,
                    'quantidade' => $item['qty'],
                    'compras_tb_id' => $idcompra
                );
                $str = $this->db->insert_string('itens', $data);
                $this->db->query($str);
            }
        } else {
            return false;
        }
    }

}

/*
 * End of file itens.php
 * Location: application/models/itens.php
 */
?>
