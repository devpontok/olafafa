<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');

class news_model extends CI_Model {
    public function get_all(){
        return $this->db->get('news')->result();
    }
    public function do_cadastro($email=NULL){
        if($email!=NULL){
             $data = array('email' => $email);
             $str=$this->db->insert_string('news',$data);
             return $this->db->query($str);
        }else{
            return FALSE;
        }
    }
}

/*
 * End of file news_model.php
 * Location: application/models/news_model.php
 */
?>
