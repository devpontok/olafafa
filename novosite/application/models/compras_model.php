<?php

if (!defined('BASEPATH'))
    exit('Acesso ao script não é permitido');

class compras_model extends CI_Model {

    public function get_all() {
        $this->db->select('compras_tb.id as compra,login_tb.email,data_compra, status,valor');
        $this->db->join('cliente_tb', 'cliente_tb.id=compras_tb.cliente_tb_id');
        $this->db->join('login_tb', 'cliente_tb.id=login_tb.cliente_tb_id');
        return $this->db->get('compras_tb')->result();
    }

    public function do_cadastro() {
        $this->load->library('session', 'database');
        $usuario = $this->session->userdata('user_id');

        if(!isset($_SESSION)){
            session_start();
        }
        $dados['carrinho'] = $_SESSION['carrinho'];
        $data = array('valor' => $_SESSION['total'], 'status' => 'a','cliente_tb_id'=>$usuario);
        $data['data_compra'] =date ("Y-m-d H:i:s");
        $str = $this->db->insert_string('compras_tb', $data);
        $this->db->query($str);
        return $this->db->insert_id();
    }

}

/*
 * End of file compras.php
 * Location: application/models/compras.php
 */
?>
