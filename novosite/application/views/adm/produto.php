<?php
switch ($tela):
    case 'cadastro':
        echo '<div class="col-md-12">';
        if (isset($id)) {
            echo '<a href="' . base_url('adm/produtos/galeria/' . $id) . '">Adicionar Galeria</a>';
        }
        echo form_open_multipart($action, array('class' => 'costum cadastroform'));
        echo form_fieldset("Cadastro de produtos");
        echo errors_validacao();
        $this->session->flashdata('erros');
        echo "<div>";

        echo form_input(array('name' => 'nome', "placeholder" => "Nome", 'class' => 'form-control col-sm-1 mg-right-3', "value" => isset($nome) ? $nome : ""), 'autofocus');

        echo form_dropdown('categoria', $options, $select, 'class="btn dropdown-toggle clearfix btn-primary  mg-right-3"');

        echo ' <label class="checkbox rigth col-md-2 ' . $destaques . ' ">
            <input type="checkbox" name="destaque" id="destaque" value="destaque" data-toggle="checkbox" >
            Destaque
          </label>';
     
        foreach($tamanho as $tam){
            $check=in_array($tam->id, $selected_tam)?"checked":"";
           echo  ' <label class="checkbox rigth col-md-2 '.$check.'">
            <input type="checkbox" name="'.$tam->id.'"   value="'.$tam->id.'" data-toggle="checkbox" ';
           echo in_array($tam->id, $selected_tam)?"checked=checked":"";
           echo '>'.$tam->nome.'
          </label>';
        }
        
        echo "</div><br/>";
        echo form_input(array('name' => 'valor', "placeholder" => "Valor", 'class' => 'form-control', "value" => isset($valor) ? $valor : ""));
        echo '<br/>';
        echo form_input(array('name' => 'peso em gramas', "placeholder" => "Peso", 'class' => 'form-control', "value" => isset($peso) ? $peso : ""));
        echo '<br/>';
        echo form_textarea(array('name' => 'detalhes', 'id' => 'detalhes', "placeholder" => "Detalhes", 'class' => 'form-control', "value" => isset($detalhes) ? $detalhes : ""));
        echo '<br/>';

        if (isset($imagem) && !is_null($imagem)) {
            echo "<img style='width:500px !important;' src='{$imagem}'/>";
        }
        echo form_upload(array('name' => 'arquivo', 'placeholder' => 'Arquivo', 'class' => "form-control btn btn-block btn-lg btn-default"));
        echo '<br/>';
        echo form_submit(array('name' => 'cadastro', 'class' => 'btn btn-defalut btn-info', 'style' => 'float:right'), 'Cadastrar');
        echo form_fieldset_close();
        echo form_close();
        echo '</div>';
        break;
    case 'lista':
        ?>
        <div id="resultado">
            <div class="col-sm-12">
                <?php
                echo form_open('', array('id' => 'buscar_produtos', 'class' => 'costum loginform'));
                echo form_fieldset("Produtos");
                echo errors_validacao();
                echo '<div class="row"><div class="col-sm-6">';
                echo form_input(array('name' => 'nome', 'id' => 'nome', "placeholder" => "nome", 'class' => 'form-control'), '', 'autofocus');
                echo '</div><div class="col-sm-6">';
                echo form_dropdown('categoria', $options, $select, 'class="btn dropdown-toggle clearfix btn-primary" id="categoria"');
                echo '</div></div><br/>';
                echo form_submit(array('name' => 'Pesquisar', 'class' => 'btn btn-defalut btn-info', 'style' => 'float:right'), 'Pesquisar');
                echo form_fieldset_close();
                echo form_close();
                ?>
            </div>
            <div class='col-lg-20'>
                <div class="row">
                    <div class='col-sm-4'></div>
                    <div class='col-sm-4'>Produto</div>
                    <div class='col-sm-4'>Categoria</div>
                </div>
                <div  class="row">
                    <?php
                    foreach ($produtos as $produto) {
                        $imagem = get_imagemProduto($produto->categoria_nome, $produto->id);
                        ?>
                        <div class='col-sm-4  heigth-40'><a href="<?php echo base_url('adm/produtos/atualiza/' . $produto->id); ?>"><img src="<?php echo (isset($imagem['url'])) ? $imagem['url'] : base_url("galeria/default-roupa.png"); ?>" class="width-50" /></a></div>
                        <div class='col-sm-4  heigth-40'><?php echo $produto->nome; ?><br><a href="<?php echo base_url('adm/produtos/galeria/' . $produto->id); ?>">Adicionar Galeria</a>
                            <br><a href="#" class="apaga-produto" id="<?php echo $produto->id;?>_">Excluir produto</a>
                        </div>
                        <div class='col-sm-4  heigth-40'><?php echo $produto->categoria_nome; ?></div>
                        <br/>
                    <?php } ?>
                </div>

            </div>
        </div>
        <?php
        break;
    default:
        break;
endswitch;
?>
