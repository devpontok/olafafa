<div id="box-menu2">
    <div id="body-nome">
	<div class="site">
		<h1>Quem Somos</h1>
	</div>
    </div>
</div>
<div class="site termos-page">
    <div class="site">
        <p id="title-dados-pessoais">Olá fafá!</p>
        <p>
            É um e-commerce multimarcas  para os pequenos cheios de estilo.
            Trazemos facilidade e comodidade para o dia-a-dia dos pais que buscam produtos diferenciados para os pequenos de 0 a 6 anos.<br>
            Com uma proposta divertida e um olhar apurado para tudo que é bacana no mundo infantil, montamos um mix de produtos descolados de marcas do Brasil e de fora.<br>
            Fazemos uma seleção criteriosa de todos os nossos parceiros, prezando sempre pelo conforto, qualidade e originalidade de cada produto.<br>
            Queremos que as nossas crianças nunca percam a espontaneidade e a alegria de viver!<br>

        </p>

    </div>
</div>
