	<footer id="rodape">
		<div id="logos">
			<div class="site">
				<img src="<?php echo base_url('imagens/baner-rodape.png');?>">
			</div>
		</div>
		<div id="rodape-topo"></div>
		<div id="rodape-baixo">
			<div class="site">	
				<div class="creditos">
					<ul>
						<h2>Sobre nós</h2>
						<li><a href="<?php echo base_url('quemsomos');?>">Quem Somos</a></li>
						<li><a href="<?php echo base_url('privacidade');?>">Política de Privacidade</a></li>
					</ul>
				</div>
				<div class="creditos">
					<ul>
						<h2>Política</h2>
						<li><a href="<?php echo base_url('termosdeservico');?>">Termos e Condições</a></li>
						<li><a href="<?php echo base_url('pagamento');?>">Pagamento</a></li>
					</ul>
				</div>
				<div class="creditos">
					<ul>
						<h2>Contato</h2>
						<li><a href="">contato@olafafa.com</a></li>
					</ul>
				</div>
			
				<div class="creditos" id="redes_sociais">
					<a href="https://www.facebook.com/olafafa" target="blank"><img src="<?php echo base_url('imagens/icon-facebook.png');?>"></a>
					<a href="http://instagram.com/olafafa#" target="blank"><img src="<?php echo base_url('imagens/icon-instagram.png');?>"></a>			
				</div>
				<form action="#" id="cadastro">
					<p>Cadastre-se e receba nossas novidades!</p>
					<input id="email" type="email" placeholder=" e-mail" required>
					<input id="botao" type="submit" value="Ok">			
				</form>
				<div id="linha"></div>
				<div id="direito-de-copia"><p>Copyright &copy 2013 Olá Fafá. Todos os direitos reservados. </p></div>
				<div id="logo-k">
					<img src="<?php echo base_url('imagens/logo-k.png');?>">
				</div>
			</div>		
		</div>
	</footer>
